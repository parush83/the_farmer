package Utils;

import com.inqube.thefarmer.base.BaseActivity;
import com.inqube.thefarmer.model.MSG;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by sanjib on 17/11/17.
 */

public interface APIService {
    public String USER = "";

    @FormUrlEncoded
    @POST("login")
    Call<MSG> userLogIn(@Field("frm_primary_number") String frm_primary_number,
                        @Field("password") String password, @Field("frm_latitude") String frm_latitude
            , @Field("frm_longitude") String frm_longitude);

    @FormUrlEncoded
    @POST("logout")
    Call<MSG> userLogOut(@Field("token") String token, @Field("frm_language_id") String frm_language_id);

    @FormUrlEncoded
    @POST("register")
    Call<MSG> shortRegister(@Field("frm_farmer_type") String frm_farmer_type,
                            @Field("name") String name,
                            @Field("email") String email,
                            @Field("frm_state") String frm_state,
                            @Field("frm_district") String frm_district,
                            @Field("frm_subdivision") String frm_subdivision,
                            @Field("frm_block") String frm_block,
                            @Field("frm_mouza") String frm_mouza,
                            @Field("frm_primary_number") String frm_primary_number,
                            @Field("frm_language_id") String frm_language_id,
                            @Field("frm_farmer_profile_image") String Frm_farmer_profile_image,
                            @Field("password") String password);

    @FormUrlEncoded
    @POST("get_static_data")
    Call<MSG> getStaticData(@Field("frm_language_id") String frm_language_id);

    @FormUrlEncoded
    @POST("get_mouza_by_block")
    Call<MSG> getMouzaByBlock(@Field("frm_language_id") String frm_language_id, @Field("frm_block") String frm_block);


    @FormUrlEncoded
    @POST("temp_image_upload")
    Call<MSG> uploadImage(@Field("image_source") String image_source,
                          @Field("token") String token, @Field("frm_language_id") String frm_language_id);


    ///////////////////////// TO SEND SANJIB
    @FormUrlEncoded
    @POST("send_otp")
    Call<MSG> sendOTP(@Field("mobile_no") String mobile_no);

    @FormUrlEncoded
    @POST("get_reset_password_token")
    Call<MSG> forgotPassword(@Field("frm_primary_number") String mobile_no);

    @FormUrlEncoded
    @PUT("forget_change_password/{id}")
    Call<MSG> forgotPasswordSecond(@Path("id") String id, @Field("otp") String otp, @Field("new_password") String new_password);

    @FormUrlEncoded
    @PUT("change_password/{id}")
    Call<MSG> changePassword(@Path("id") String id, @Field("token") String token,
                             @Field("old_password") String old_password,
                             @Field("new_password") String new_password, @Field("frm_language_id") String frm_language_id);

    @FormUrlEncoded
    @POST("get_static_details_registration_data")
    Call<MSG> getStaticDetailsRegistrationData(@Field("token") String token,
                                               @Field("frm_language_id") String frm_language_id);

    @FormUrlEncoded
    @POST("submit_question_answers_agri")
    Call<MSG> agriProblemSubmit(@Field("question_submit_for") String question_submit_for,
                                @Field("question_type_id") String question_type_id,
                                @Field("crop_type_id") String crop_type_id,
                                @Field("crop_id") String crop_id,
                                @Field("growth_stage_id") String growth_stage_id,
                                @Field("problem_type_id") String problem_type_id,
                                // @Field("problem_id") String problem_id,
                                @Field("user_id") String user_id,
                                @Field("farmer_name") String farmer_name,
                                @Field("farmer_mobile_no") String farmer_mobile_no,
                                @Field("state_id") String state_id,
                                @Field("district_id") String district_id,
                                @Field("subdivision_id") String subdivision_id,
                                @Field("block_id") String block_id,
                                @Field("mouza_id") String mouza_id,
                                @Field("latitude") String latitude,
                                @Field("lognitude") String lognitude,
                                @Field("question_title") String question_title,
                                @Field("long_description") String long_description,
                                @Field("image_save_data") String image_save_data,
                                @Field("token") String token,
                                @Field("frm_language_id") String frm_language_id);

    @FormUrlEncoded
    @POST("submit_question_answers_natural")
    Call<MSG> naturalDisasterSubmit(@Field("question_submit_for") String question_submit_for,
                                    @Field("question_type_id") String question_type_id,
                                    // @Field("problem_id") String problem_id,
                                    @Field("user_id") String user_id,
                                    @Field("farmer_name") String farmer_name,
                                    @Field("farmer_mobile_no") String farmer_mobile_no,
                                    @Field("state_id") String state_id,
                                    @Field("district_id") String district_id,
                                    @Field("subdivision_id") String subdivision_id,
                                    @Field("block_id") String block_id,
                                    @Field("mouza_id") String mouza_id,
                                    @Field("effective_area") String effective_area,
                                    @Field("latitude") String latitude,
                                    @Field("lognitude") String lognitude,
                                    @Field("question_title") String question_title,
                                    @Field("long_description") String long_description,
                                    @Field("image_save_data") String image_save_data,
                                    @Field("token") String token,
                                    @Field("frm_language_id") String frm_language_id);

    @FormUrlEncoded
    @POST("details_register/{id}")
    Call<MSG> detailRegistration(@Path("id") String id,
                                 @Field("name") String name,
                                 @Field("frm_primary_number") String frm_primary_number,
                                 @Field("frm_gender") String frm_gender,
                                 @Field("frm_secondary_number") String frm_secondary_number,
                                 @Field("frm_pincode") String frm_pincode,
                                 @Field("frm_father_name") String frm_father_name,
                                 @Field("frm_category") String frm_category,
                                 @Field("frm_farmer_type") String frm_farmer_type,
                                 @Field("frm_farmer_land_type") String frm_farmer_land_type,
                                 @Field("frm_id_proof_number") String frm_id_proof_number,
                                 @Field("frm_id_proof") String frm_id_proof,
                                 @Field("frm_kcc_card") String frm_kcc_card,
                                 @Field("frm_kcc_card_number") String frm_kcc_card_number,
                                 @Field("frm_bank_account") String frm_bank_account,
                                 @Field("frm_bank_name") String frm_bank_name,
                                 @Field("frm_bank_branch") String frm_bank_branch,
                                 @Field("frm_bank_ifsc") String frm_bank_ifsc,
                                 @Field("frm_state") String frm_state,
                                 @Field("frm_district") String frm_district,
                                 @Field("frm_subdivision") String frm_subdivision,
                                 @Field("frm_block") String frm_block,
                                 @Field("frm_mouza") String frm_mouza,
                                 @Field("frm_total_cultivabe_land") String frm_total_cultivabe_land,
                                 @Field("frm_farm_machinery") String frm_farm_machinery,
                                 @Field("frm_animal_number") String frm_animal_number,
                                 @Field("frm_cultivate_fish") String frm_cultivate_fish,
                                 @Field("frm_cultivate_pond_number") String frm_cultivate_pond_number,
                                 @Field("frm_language_id") String frm_language_id,
                                 @Field("frm_season_rabi_list") String frm_season_rabi_list,
                                 @Field("frm_season_kharif_list") String frm_season_kharif_list,
                                 @Field("frm_season_pre_kharif_list") String frm_season_pre_kharif_list,
                                 @Field("frm_animal_list") String frm_animal_list,
                                 @Field("frm_farm_machinery_list") String frm_farm_machinery_list,
                                 @Field("email") String email,
                                 @Field("frm_farmer_profile_image") String Frm_farmer_profile_image,
                                 @Field("user_language_id") String lang_id,
                                 @Field("token") String token);

    @FormUrlEncoded
    @PUT("get_my_question_list")
    Call<MSG> getList(@Field("user_id") String user_id,
                      @Field("token") String token,
                      @Field("question_type_id") String question_type_id,
                      @Field("frm_language_id") String frm_language_id);

    @FormUrlEncoded
    @PUT("get_other_question_list")
    Call<MSG> getOtherList(@Field("user_id") String user_id,
                           @Field("token") String token,
                           @Field("question_type_id") String question_type_id,
                           @Field("frm_language_id") String frm_language_id);

    @FormUrlEncoded
    @POST("get_user_details")
    Call<MSG>
    getUserDetails(@Field("id") String id, @Field("frm_language_id") String frm_language_id);

    @FormUrlEncoded
    @POST("user_depends_user_list")
    Call<MSG> getFramerList(@Field("user_id") String user_id, @Field("token") String token, @Field("user_language_id") String user_language_id);

    @FormUrlEncoded
    @POST("user_depends_register")
    Call<MSG> newDetailRegistration(
            @Field("name") String name,
            @Field("frm_primary_number") String frm_primary_number,
            @Field("frm_gender") String frm_gender,
            @Field("frm_secondary_number") String frm_secondary_number,
            @Field("frm_pincode") String frm_pincode,
            @Field("frm_father_name") String frm_father_name,
            @Field("frm_category") String frm_category,
            @Field("frm_farmer_type") String frm_farmer_type,
            @Field("frm_farmer_land_type") String frm_farmer_land_type,
            @Field("frm_id_proof_number") String frm_id_proof_number,
            @Field("frm_id_proof") String frm_id_proof,
            @Field("frm_kcc_card") String frm_kcc_card,
            @Field("frm_kcc_card_number") String frm_kcc_card_number,
            @Field("frm_bank_account") String frm_bank_account,
            @Field("frm_bank_name") String frm_bank_name,
            @Field("frm_bank_branch") String frm_bank_branch,
            @Field("frm_bank_ifsc") String frm_bank_ifsc,
            @Field("frm_state") String frm_state,
            @Field("frm_district") String frm_district,
            @Field("frm_subdivision") String frm_subdivision,
            @Field("frm_block") String frm_block,
            @Field("frm_mouza") String frm_mouza,
            @Field("frm_total_cultivabe_land") String frm_total_cultivabe_land,
            @Field("frm_farm_machinery") String frm_farm_machinery,
            @Field("frm_animal_number") String frm_animal_number,
            @Field("frm_cultivate_fish") String frm_cultivate_fish,
            @Field("frm_cultivate_pond_number") String frm_cultivate_pond_number,
            @Field("frm_language_id") String frm_language_id,
            @Field("frm_season_rabi_list") String frm_season_rabi_list,
            @Field("frm_season_kharif_list") String frm_season_kharif_list,
            @Field("frm_season_pre_kharif_list") String frm_season_pre_kharif_list,
            @Field("frm_animal_list") String frm_animal_list,
            @Field("frm_farm_machinery_list") String frm_farm_machinery_list,
            @Field("email") String email,
            @Field("token") String token,
            @Field("frm_farmer_profile_image") String Frm_farmer_profile_image,
            @Field("password") String password,
            @Field("user_language_id") String lang_id,
            @Field("approved_by_user_id") String approved_by_user_id);

    @FormUrlEncoded
    @POST("get_answer")
    Call<MSG> getAnswerList(@Field("question_id") String question_id, @Field("token") String token, @Field("user_language_id") String user_language_id, @Field("question_type_id") String question_type_id);

}



