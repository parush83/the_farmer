package Utils;

import android.view.View;

import com.inqube.thefarmer.model.MSG;

import retrofit2.Response;

/**
 * Created by Tubu on 11/16/2017.
 */

public class AllInterfaces {
    public interface ForgotPassword {
        public void onForgotPasswqordProperDataSubmitted(String otp, String newPassword, String user_id);
    }

    public interface SendOTP {
        public void sendOTPResponse(String phoneNumber);
    }

    public interface RetrofitResponseToActivityOrFrament {
        public void onSuccess(Response<MSG> response, String which_method);

        public void onFailure(Response<MSG> response);

        public void onResponseFailure();
    }

    public interface IfGPSNotAvailable {
        public void ifGPSNotAvailable();
    }

    public interface KeyboardDialogCallback {
        public void onOkClick();

        public void onCancleClick();
    }

    public interface CustomItemClickListener {
        public void onItemClick(View v, int position, String question_id);
    }

}
