package Utils;

import java.net.URI;

/**
 * Created by Tubu on 11/18/2017.
 */

public class Constant {
    public static final int AADHAR = 1;
    public static final int VOTER_ID = 2;
    //public static final String BASE_URL = "http://188.166.211.154/api/auth/";
    //public static final String BASE_URL = "http://192.168.1.22/krishishakti/public/api/auth/";

   // public static final int ENGLISH = 1;
    //public static final int HINDI = 2;
    //public static final int MARATHI = 3;
    //public static final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;
    public static final String AGRI = "2";
    public static final String NATURAL_DISASTER = "3";
    public static final String WHICH_PROBLEM = "WHICH_PROBLEM";
    public static final String PLACE_LIST = "PLACE_LIST";
    //public static final String REMEMBER_ME = "REMEMBER_ME";
    public static final String SELECTED_LANGUAGE = "SELECTED_LANGUAGE";
    public static final String TOKEN_ID = "TOKEN_ID";
    public static final int MALE = 1;
    public static final int FEMALE = 2;
    public static final int YES = 1;
    public static final int NO = 2;
    //public static final String STATE_ID = "1";

    public static final String CROP_LIST = "CROP_LIST";
    public static final String USER_ID = "USER_ID";
    public static final String USER_IMAGE = "USER_IMAGE";

    public static String USER_NAME = "USER_NAME";
    public static String LAST_LOGIN = "LAST_LOGIN";

    public static String ANIMAL_LIST = "ks_animals_list";
    public static String FARMER_TYPE_LIST = "ks_farmer_types_list";
    public static String FARM_MACHINARY_LIST = "ks_farm_machinerys_list";
    public static String SEASON_LIST = "ks_seasons_list";
    public static String FARMER_NAME = "name";
    public static String FARMER_PRIMARY_NUMBER = "frm_primary_number";
    public static String EMAIL = "email";
    public static String KEYBOARD = "com.google.android.inputmethod.latin";
    public static String KEYBOARD1 = "market://details?id=" + "com.google.android.inputmethod.latin";
    //    public static String HINDI_KEYBOARD="com.fancy.fontforu.hindikeyboard";
//    public static String HINDI_KEYBOARD1="market://details?id=" + "com.fancy.fontforu.hindikeyboard";
//    public static String MARATHI_KEYBOARD="com.sparsh.inputmethod.marathi";
//    public static String MARATHI_KEYBOARD1="market://details?id=" + "com.sparsh.inputmethod.marathi";
    public static int CODE_422 = 422;
    public static int CODE_500 = 401;
    public static int CODE_200 = 200;
    public static String REGISTRATION_STATUS = "registration_complete";
    public static String TYPE = "TYPE";
    public static String FARMER_ID = "FARMER_ID";
    public static String FRM_NAME = "FRM_NAME";
    public static String FRM_MOB = "FRM_MOB";
    public static String CLICK = "CLICK";
}
