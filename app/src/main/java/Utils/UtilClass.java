package Utils;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.inqube.thefarmer.BuildConfig;
import com.inqube.thefarmer.FarmerApplication;
import com.inqube.thefarmer.R;
import com.inqube.thefarmer.SplashActivity;
import com.inqube.thefarmer.Widget.MyEditText;
import com.inqube.thefarmer.base.BaseActivity;
import com.inqube.thefarmer.base.BaseActivityWithoutMenu;
import com.inqube.thefarmer.model.BlockList;
import com.inqube.thefarmer.model.CropGrowthStageList;
import com.inqube.thefarmer.model.CropsList;
import com.inqube.thefarmer.model.Data;
import com.inqube.thefarmer.model.DropDownModelClass;
import com.inqube.thefarmer.model.GetMouzaList;
import com.inqube.thefarmer.model.KsAnimalsList;
import com.inqube.thefarmer.model.KsFarmMachinerysList;
import com.inqube.thefarmer.model.KsFarmerTypesList;
import com.inqube.thefarmer.model.KsSeasonsList;
import com.inqube.thefarmer.model.MSG;
import com.inqube.thefarmer.model.PlaceList;
import com.inqube.thefarmer.model.SubdivisionList;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Field;

/**
 * Created by Tu`
 * bu on 11/16/2017.
 */

public class UtilClass {
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    private static UtilClass utils;
    /////Add By Sanjib/////////////////
    private AlertDialog dialog;
    private AlertDialog.Builder dialogBuilder;

    public static UtilClass getInstance() {
        if (utils == null) {
            utils = new UtilClass();
        }
        return utils;
    }

    /////////////////////////////Image Capture Permission////////////////////
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static boolean checkPermission(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("External storage permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();

                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    //////////////////////// IMAGE RESIZE//////////////////////////
    public Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                            boolean filter) {
        float ratio = Math.min(
                (float) maxImageSize / realImage.getWidth(),
                (float) maxImageSize / realImage.getHeight());
        int width = Math.round((float) ratio * realImage.getWidth());
        int height = Math.round((float) ratio * realImage.getHeight());
        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        return newBitmap;
    }

    public Bitmap scaleBitmap(Bitmap bitmap, int wantedWidth, int wantedHeight) {
        Bitmap output = Bitmap.createBitmap(wantedWidth, wantedHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        Matrix m = new Matrix();
        m.setScale((float) wantedWidth / bitmap.getWidth(), (float) wantedHeight / bitmap.getHeight());
        canvas.drawBitmap(bitmap, m, new Paint());

        return output;
    }

    public String convertBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
        return encoded;
    }

    public Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }

    public Bitmap modifyOrientation(Bitmap bitmap, String image_absolute_path) throws IOException {
        ExifInterface ei = new ExifInterface(image_absolute_path);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotate(bitmap, 90);

            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotate(bitmap, 180);

            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotate(bitmap, 270);

            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                return flip(bitmap, true, false);

            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                return flip(bitmap, false, true);

            default:
                return bitmap;
        }
    }

    public Bitmap rotate(Bitmap bitmap, float degrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    ////////////////// SEND OTP PASSWORD/////////////////////////////////////////////

    public Bitmap flip(Bitmap bitmap, boolean horizontal, boolean vertical) {
        Matrix matrix = new Matrix();
        matrix.preScale(horizontal ? -1 : 1, vertical ? -1 : 1);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public void sendOTPDialog(BaseActivity baseActivity, final AllInterfaces.SendOTP sendOTPResponse, final String sendOTP) {
        final Dialog dialog = new Dialog(baseActivity);
        dialog.setContentView(R.layout.send_otp);
        dialog.setCancelable(false);
        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final MyEditText edt_phone_number = (MyEditText) dialog.findViewById(R.id.edt_phone_number);
        System.out.println("  OTP " + sendOTP);
        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
        Button btn_cancle = (Button) dialog.findViewById(R.id.btn_cancle);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edt_phone_number.getText().toString())) {
                    return;
                }
                if (!edt_phone_number.getText().toString().trim().equalsIgnoreCase(sendOTP)) {
                    return;
                }
                dialog.dismiss();
                sendOTPResponse.sendOTPResponse(edt_phone_number.getText().toString().trim());
            }
        });
        btn_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////Add By Sanjib////////////////////////////
    ////////////////LogIn From Server//////////////////////////////
    public void loginToServer(final BaseActivityWithoutMenu activity, final AllInterfaces.RetrofitResponseToActivityOrFrament retRes, String frm_primary_number, String password, String lat, String lon) {
        showProgressDialog(activity);
        APIService service = ApiClient.getClient().create(APIService.class);
        System.out.println("Send Param:" + frm_primary_number + "," + password + "," + lat + "," + lon);
        Call<MSG> userCall = service.userLogIn(frm_primary_number, password, lat, lon);
        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hideProgressDialog();
                System.out.println("Response Code::" + response.code());
                //onSignupSuccess();
                if (response.code() == Constant.CODE_422) {
                    Toast.makeText(activity, response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                } else if (response.code() == Constant.CODE_500) {
                    Toast.makeText(activity, response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                } else if (response.code() == Constant.CODE_200) {
                    if (response.body().getSuccess()) {
                        /////Success////////////////
                        retRes.onSuccess(response, "loginToServer");
                    } else {
                        /////Error Msg/////////////////////////
                        retRes.onFailure(response);
                    }
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hideProgressDialog();
                retRes.onResponseFailure();
                Log.d("onFailure", t.toString());
            }
        });
    }

    ////////////////////////////////////////////////////////////////////////////////////////////

    public void logoutFromServer(final BaseActivity activity, final AllInterfaces.RetrofitResponseToActivityOrFrament retRes, String lang_id) {
        showProgressDialog(activity);
        APIService service = ApiClient.getClient().create(APIService.class);
        Call<MSG> userCall = service.userLogOut(activity.getUserPreference(Constant.TOKEN_ID, ""), lang_id);
        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hideProgressDialog();
                //onSignupSuccess();
                //Log.d("onResponse", "" + response.body().getMessage());
                System.out.println("Response::" + response.code());
                if (response.code() == Constant.CODE_422) {
                    Toast.makeText(activity, response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                } else if (response.code() == Constant.CODE_500) {
                    System.out.println("inside 500");
                    //Toast.makeText(activity, R.string.token_expired, Toast.LENGTH_SHORT).show();
                    retRes.onSuccess(response, "TokenHasExpired");
                } else if (response.code() == Constant.CODE_200) {
                    if (response.body().getSuccess()) {
                        /////Success////////////////
                        retRes.onSuccess(response, "logoutFromServer");
                    } else {
                        /////Error Msg/////////////////////////
                        retRes.onFailure(response);
                    }
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hideProgressDialog();
                retRes.onResponseFailure();
                Log.d("onFailure", t.toString());
            }
        });
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    public void shortRegistrationToServer(final BaseActivity activity, final AllInterfaces.RetrofitResponseToActivityOrFrament retRes,
                                          String frm_type_id,
                                          String name,
                                          String email,
                                          String password,
                                          String frm_state,
                                          String frm_district,
                                          String frm_subdivision,
                                          String frm_block,
                                          String frm_mouza,
                                          String frm_primary_number,
                                          String frm_language_id,
                                          String img) {
        showProgressDialog(activity);
        System.out.println("PARAM " + name + "," + email + "," + password + "," + frm_state + "," + frm_district + "," + frm_subdivision + "," + frm_block + "," + frm_mouza + "," + frm_primary_number + "," + frm_language_id + "," + "--" + img);
        APIService service = ApiClient.getClient().create(APIService.class);

        Call<MSG> userCall = service.shortRegister(frm_type_id, name, email, frm_state, frm_district, frm_subdivision, frm_block, frm_mouza, frm_primary_number, frm_language_id, img, password);
        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hideProgressDialog();
                //onSignupSuccess();
                System.out.println("Code::" + response.code());
                // System.out.println("succes::"+response.body().getSuccess());
                System.out.println("Msg1::" + response.message());
                //System.out.println("Msg::"+response.body().getMessage());
                if (response.code() == Constant.CODE_422) {
                    try {
                        Toast.makeText(activity, response.errorBody().string(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == Constant.CODE_500) {
                    System.out.println("inside 500");
                    try {
                        Toast.makeText(activity, response.errorBody().string(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    retRes.onSuccess(response, "TokenHasExpired");
                } else if (response.code() == Constant.CODE_200) {
                    if (response.body().getSuccess()) {
                        /////Success////////////////
                        retRes.onSuccess(response, "shortRegistrationToServer");
                    } else {
                        /////Error Msg/////////////////////////retRes
                        retRes.onFailure(response);
                    }
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hideProgressDialog();
                System.out.println("Reassss:" + call.request().body());
                retRes.onResponseFailure();
                Log.d("onFailure", t.toString());
            }
        });
    }
    /////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    public void getStaticDataFromServer(final BaseActivity activity, final AllInterfaces.RetrofitResponseToActivityOrFrament retRes) {
        Response<MSG> response;
        showProgressDialog(activity);
        APIService service = ApiClient.getClient().create(APIService.class);
        Call<MSG> userCall = service.getStaticData(activity.getUserPreference(Constant.SELECTED_LANGUAGE, "" + BuildConfig.MARATHI));
        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hideProgressDialog();
                //onSignupSuccess();
                //Log.d("onResponse", "" + response.body().getData().getPlaceList().size());
                if (response.body().getSuccess()) {
                    /////Success////////////////
                    retRes.onSuccess(response, "getStaticDataFromServer");
                } else {
                    /////Error Msg/////////////////////////
                    retRes.onFailure(response);
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hideProgressDialog();
                retRes.onResponseFailure();
                Log.d("onFailure", t.toString());
            }
        });
    }
    /////////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    public void getMouzaByBlock(final BaseActivity activity, final AllInterfaces.RetrofitResponseToActivityOrFrament retRes, String block_id) {
        Response<MSG> response;
        showProgressDialog(activity);
        APIService service = ApiClient.getClient().create(APIService.class);
        System.out.println(" BLOCK ID " + block_id);
        Call<MSG> userCall = service.getMouzaByBlock(activity.getUserPreference(Constant.SELECTED_LANGUAGE, "" + BuildConfig.MARATHI), block_id);
        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hideProgressDialog();
                //onSignupSuccess();
                Log.d("onResponse", "" + response.body().getGetMouzaList().size());
                if (response.body().getSuccess()) {
                    /////Success////////////////
                    retRes.onSuccess(response, "getMouzaByBlock");
                } else {
                    /////Error Msg/////////////////////////
                    retRes.onFailure(response);
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hideProgressDialog();
                retRes.onResponseFailure();
                Log.d("onFailure", t.toString());
            }
        });
    }

    public int parseDistrictListJson(BaseActivity activity) {//String result) {
        FarmerApplication fm = (FarmerApplication) activity.getApplicationContext();
        List<PlaceList> hi = new Gson().fromJson(activity.getDataPreference(Constant.PLACE_LIST, null), new TypeToken<List<PlaceList>>() {
        }.getType());
        fm.setAl_place(hi);
        return 1;
    }

    public ArrayList<DropDownModelClass> getDistrictList(Activity activity) {
        FarmerApplication fm = (FarmerApplication) activity.getApplicationContext();

        ArrayList<DropDownModelClass> al_dist = new ArrayList<DropDownModelClass>();
        for (int i = 0; i < fm.getAl_place().get(0).getDistrictsList().size(); i++) {
            DropDownModelClass dc = new DropDownModelClass();
            dc.setId(fm.getAl_place().get(0).getDistrictsList().get(i).getDistrictId());
            dc.setName(fm.getAl_place().get(0).getDistrictsList().get(i).getDistrictName());
            al_dist.add(dc);
        }
        return al_dist;
    }

    public ArrayList<DropDownModelClass> getSubDivisionList(Activity activity, int dist_id) {
        FarmerApplication fm = (FarmerApplication) activity.getApplicationContext();
        List<SubdivisionList> al_sub_div = new ArrayList<SubdivisionList>();
        for (int i = 0; i < fm.getAl_place().get(0).getDistrictsList().size(); i++) {
            for (int j = 0; j < fm.getAl_place().get(0).getDistrictsList().get(i).getSubdivisionList().size(); j++) {
                if (fm.getAl_place().get(0).getDistrictsList().get(i).getSubdivisionList().get(j).getDistrictId() == dist_id) {
                    SubdivisionList sl = new SubdivisionList();
                    sl = fm.getAl_place().get(0).getDistrictsList().get(i).getSubdivisionList().get(j);
                    al_sub_div.add(sl);
                }
            }
            System.out.println("Size::" + al_sub_div.size());
        }
        ArrayList<DropDownModelClass> al_demo = new ArrayList<DropDownModelClass>();
        for (int i = 0; i < al_sub_div.size(); i++) {
            if (al_sub_div.get(i).getDistrictId() == dist_id) {
                SubdivisionList sd = al_sub_div.get(i);
                DropDownModelClass dc = new DropDownModelClass();
                dc.setName(sd.getSubDivisionName());
                dc.setId(sd.getSubDivisionId());
                al_demo.add(dc);
            }
        }
        return al_demo;
    }

    public ArrayList<DropDownModelClass> getBolckList(Activity activity, int sub_div_id) {
        FarmerApplication fm = (FarmerApplication) activity.getApplicationContext();
        List<BlockList> al_block = new ArrayList<BlockList>();
        for (int i = 0; i < fm.getAl_place().get(0).getDistrictsList().size(); i++) {
            for (int j = 0; j < fm.getAl_place().get(0).getDistrictsList().get(i).getSubdivisionList().size(); j++) {
                for (int k = 0; k < fm.getAl_place().get(0).getDistrictsList().get(i).getSubdivisionList().get(j).getBlockList().size(); k++) {
                    if (fm.getAl_place().get(0).getDistrictsList().get(i).getSubdivisionList().get(j).getBlockList().get(k).getSubdivisionId() == sub_div_id) {
                        BlockList sl = new BlockList();
                        sl = fm.getAl_place().get(0).getDistrictsList().get(i).getSubdivisionList().get(j).getBlockList().get(k);
                        al_block.add(sl);
                    }
                }
            }
        }
        ArrayList<DropDownModelClass> al_demo = new ArrayList<DropDownModelClass>();
        for (int i = 0; i < al_block.size(); i++) {
            if (al_block.get(i).getSubdivisionId() == sub_div_id) {
                BlockList sd = al_block.get(i);
                DropDownModelClass dc = new DropDownModelClass();
                dc.setName(sd.getBlockName());
                dc.setId(sd.getBlockId());
                al_demo.add(dc);
            }
        }
        return al_demo;
    }

    public ArrayList<DropDownModelClass> geMouza(Activity activity, List<GetMouzaList> al_mouza_list, int block_id) {
        System.out.println(" BLOCK ID  " + block_id);
        ArrayList<DropDownModelClass> al_demo = new ArrayList<DropDownModelClass>();
        for (int i = 0; i < al_mouza_list.size(); i++) {
            if (al_mouza_list.get(i).getBlockId() == block_id) {
                GetMouzaList sd = al_mouza_list.get(i);
                DropDownModelClass dc = new DropDownModelClass();
                dc.setName(sd.getMouzaName());
                dc.setId(sd.getMouzaId());
                al_demo.add(dc);
            }
        }
        return al_demo;
    }
    /////////Forgot password Dialog////////////////////////

    //////////////// TO SEND SANJIB
    public void sendOTP(final BaseActivity activity, final AllInterfaces.RetrofitResponseToActivityOrFrament retRes, String mobile_no) {
        Response<MSG> response;
        showProgressDialog(activity);
        APIService service = ApiClient.getClient().create(APIService.class);
        Call<MSG> userCall = service.sendOTP(mobile_no);
        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hideProgressDialog();
                //onSignupSuccess();
                if (response.body().getSuccess()) {
                    /////Success////////////////
                    retRes.onSuccess(response, "SendOTP");
                } else {
                    /////Error Msg/////////////////////////
                    retRes.onFailure(response);
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hideProgressDialog();
                retRes.onResponseFailure();
                Log.d("onFailure", t.toString());
            }
        });
    }

    public void forgotPassDialog(final BaseActivityWithoutMenu baseActivity, final AllInterfaces.RetrofitResponseToActivityOrFrament retRes) {
        final Dialog dialog = new Dialog(baseActivity);
        dialog.setContentView(R.layout.forgot_pass_dialog);
        dialog.setCancelable(true);
        // dialog.setTitle(R.string.forgot_pass_title);
        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final MyEditText edt_phone_number = (MyEditText) dialog.findViewById(R.id.edt_phone_number);
        TextView tv_title = (TextView) dialog.findViewById(R.id.tv_title);
        tv_title.setText(R.string.forgot_pass_title);
        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
        Button btn_cancle = (Button) dialog.findViewById(R.id.btn_cancle);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edt_phone_number.getText().toString())) {
                    return;
                }
                if (edt_phone_number.getText().toString().trim().length() < 10) {
                    return;
                }
                dialog.dismiss();
                forgotPassword(baseActivity, retRes, edt_phone_number.getText().toString().trim());
            }
        });
        btn_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void forgotPassword(final BaseActivityWithoutMenu activity, final AllInterfaces.RetrofitResponseToActivityOrFrament retRes, String mobile_no) {
        Response<MSG> response;
        showProgressDialog(activity);
        APIService service = ApiClient.getClient().create(APIService.class);
        Call<MSG> userCall = service.forgotPassword(mobile_no);
        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hideProgressDialog();
                //onSignupSuccess();
                if (response.body().getSuccess()) {
                    /////Success////////////////
                    retRes.onSuccess(response, "forgotPassword");
                } else {
                    /////Error Msg/////////////////////////
                    retRes.onFailure(response);
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hideProgressDialog();
                retRes.onResponseFailure();
                Log.d("onFailure", t.toString());
            }
        });
    }

    public void newPasswordDialog(BaseActivityWithoutMenu baseActivity, final AllInterfaces.ForgotPassword forgotPassword, final String user_id) {
        final Dialog dialog = new Dialog(baseActivity);
        dialog.setContentView(R.layout.new_password);
        dialog.setCancelable(false);
        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final MyEditText edt_otp = (MyEditText) dialog.findViewById(R.id.edt_otp);
        final MyEditText edt_new_password = (MyEditText) dialog.findViewById(R.id.edt_new_password);

        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
        Button btn_cancle = (Button) dialog.findViewById(R.id.btn_cancle);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edt_otp.getText().toString())) {
                    return;
                }
                if (TextUtils.isEmpty(edt_new_password.getText().toString())) {
                    return;
                }
                forgotPassword.onForgotPasswqordProperDataSubmitted(edt_otp.getText().toString().trim(), edt_new_password.getText().toString().trim(), user_id);
                dialog.dismiss();
            }
        });
        btn_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void forgotPasswordSecond(final BaseActivityWithoutMenu activity, final AllInterfaces.RetrofitResponseToActivityOrFrament retRes, String user_id, String otp, String mobile_no) {
        Response<MSG> response;
        showProgressDialog(activity);
        APIService service = ApiClient.getClient().create(APIService.class);
        Call<MSG> userCall = service.forgotPasswordSecond(user_id, otp, mobile_no);
        System.out.println("PARAMS:" + user_id + "," + otp + "," + mobile_no);
        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hideProgressDialog();
                System.out.println("RESPONSE CODE " + response.code());
                //onSignupSuccess();
                if (response.body().getSuccess()) {
                    /////Success////////////////
                    retRes.onSuccess(response, "forgotPasswordSecond");
                } else {
                    /////Error Msg/////////////////////////
                    retRes.onFailure(response);
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hideProgressDialog();
                retRes.onResponseFailure();
                Log.d("onFailure", t.toString());
            }
        });
    }

    public void changPassword(final BaseActivity activity, final AllInterfaces.RetrofitResponseToActivityOrFrament retRes, String user_id, String token, String old_password, String new_password, String lang_id) {
        Response<MSG> response;
        showProgressDialog(activity);
        APIService service = ApiClient.getClient().create(APIService.class);
        Call<MSG> userCall = service.changePassword(user_id, token, old_password, new_password, lang_id);
        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hideProgressDialog();
                //onSignupSuccess();
                if (response.code() == Constant.CODE_422) {
                    Toast.makeText(activity, response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                } else if (response.code() == Constant.CODE_500) {
                    Toast.makeText(activity, response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                } else if (response.code() == Constant.CODE_200) {
                    if (response.body().getSuccess()) {
                        /////Success////////////////
                        retRes.onSuccess(response, "changPassword");
                    } else {
                        /////Error Msg/////////////////////////
                        retRes.onFailure(response);
                    }
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hideProgressDialog();
                retRes.onResponseFailure();
                Log.d("onFailure", t.toString());
            }
        });
    }
    ///////////////////////////////////

    public void changePasswordDialog(final BaseActivity baseActivity, final AllInterfaces.RetrofitResponseToActivityOrFrament retRes) {
        final Dialog dialog = new Dialog(baseActivity);
        dialog.setContentView(R.layout.change_password);
        dialog.setCancelable(false);
        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final MyEditText edt_old_password = (MyEditText) dialog.findViewById(R.id.edt_old_password);
        final MyEditText edt_new_password = (MyEditText) dialog.findViewById(R.id.edt_new_password);

        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
        Button btn_cancle = (Button) dialog.findViewById(R.id.btn_cancle);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edt_old_password.getText().toString())) {
                    return;
                }
                if (TextUtils.isEmpty(edt_new_password.getText().toString())) {
                    return;
                }
                changPassword(baseActivity,
                        retRes,
                        baseActivity.getUserPreference(Constant.USER_ID, ""),
                        baseActivity.getUserPreference(Constant.TOKEN_ID, ""),
                        edt_old_password.getText().toString().trim(),
                        edt_new_password.getText().toString().trim(),
                        baseActivity.getUserPreference(Constant.SELECTED_LANGUAGE, BuildConfig.MARATHI));
                dialog.dismiss();
            }
        });
        btn_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public int parseCroptListJson(BaseActivity activity) {//String result) {

        //List<Data> hi=new Gson().fromJson(activity.getDataPreference(Constant.CROP_LIST,null),new TypeToken<List<Data>>(){}.getType());
        Data data = new Gson().fromJson(activity.getDataPreference(Constant.CROP_LIST, null), Data.class);
        FarmerApplication fm = (FarmerApplication) activity.getApplicationContext();
        fm.setAl_type_crop(data.getCropTypeList());
        fm.setAl_problems(data.getKsProblemTypesList());
        //fm.setAl_type_user(data.getGetRoleList());
        return 1;
    }

    public int parseUserListJson(BaseActivity activity) {//String result) {

        //List<Data> hi=new Gson().fromJson(activity.getDataPreference(Constant.CROP_LIST,null),new TypeToken<List<Data>>(){}.getType());
        Data data = new Gson().fromJson(activity.getDataPreference(Constant.CROP_LIST, null), Data.class);
        FarmerApplication fm = (FarmerApplication) activity.getApplicationContext();
        fm.setAl_type_user(data.getGetRoleList());
        return 1;
    }

    public ArrayList<DropDownModelClass> getCropTypeList(Activity activity) {
        FarmerApplication fm = (FarmerApplication) activity.getApplicationContext();
        ArrayList<DropDownModelClass> al_type_crop = new ArrayList<DropDownModelClass>();
        for (int i = 0; i < fm.getAl_type_crop().size(); i++) {
            DropDownModelClass dc = new DropDownModelClass();
            dc.setId(fm.getAl_type_crop().get(i).getCropsTypesId());
            dc.setName(fm.getAl_type_crop().get(i).getCropsTypeName());
            al_type_crop.add(dc);
        }
        return al_type_crop;
    }

    public ArrayList<DropDownModelClass> getCropList(Activity activity, int id) {
        FarmerApplication fm = (FarmerApplication) activity.getApplicationContext();
        List<CropsList> al_crops = new ArrayList<>();

        for (int i = 0; i < fm.getAl_type_crop().size(); i++) {
            for (int j = 0; j < fm.getAl_type_crop().get(i).getCropsList().size(); j++) {
                if (fm.getAl_type_crop().get(i).getCropsList().get(j).getCropsTypeId() == id) {
                    CropsList cp = new CropsList();
                    cp = fm.getAl_type_crop().get(i).getCropsList().get(j);
                    al_crops.add(cp);
                }
            }

        }
        System.out.println("Sizeeee" + al_crops.size());
        ArrayList<DropDownModelClass> al_demo = new ArrayList<DropDownModelClass>();
        for (int i = 0; i < al_crops.size(); i++) {
            if (al_crops.get(i).getCropsTypeId() == id) {
                CropsList cp = al_crops.get(i);
                DropDownModelClass dc = new DropDownModelClass();
                dc.setName(cp.getCropsName());
                dc.setId(cp.getCropsId());
                al_demo.add(dc);
            }
        }
        return al_demo;
    }

    /////////////////////////Upload Image///////////////////////////

    public ArrayList<DropDownModelClass> getCropGrowthList(Activity activity, int id) {
        FarmerApplication fm = (FarmerApplication) activity.getApplicationContext();
        List<CropGrowthStageList> al_crop_growth = new ArrayList<CropGrowthStageList>();
        for (int i = 0; i < fm.getAl_type_crop().size(); i++) {
            for (int j = 0; j < fm.getAl_type_crop().get(i).getCropsList().size(); j++) {
                for (int k = 0; k < fm.getAl_type_crop().get(i).getCropsList().get(j).getCropGrowthStageList().size(); k++) {
                    if (fm.getAl_type_crop().get(i).getCropsList().get(j).getCropGrowthStageList().get(k).getCrop_id() == id) {
                        CropGrowthStageList cg = new CropGrowthStageList();
                        cg = fm.getAl_type_crop().get(i).getCropsList().get(j).getCropGrowthStageList().get(k);
                        al_crop_growth.add(cg);
                    }
                    System.out.println("SIZE:" + fm.getAl_type_crop().get(i).getCropsList().get(j).getCropGrowthStageList().get(k).getCrop_id());
                }
            }
        }

        ArrayList<DropDownModelClass> al_demo = new ArrayList<DropDownModelClass>();
        for (int i = 0; i < al_crop_growth.size(); i++) {
            if (al_crop_growth.get(i).getCrop_id() == id) {
                CropGrowthStageList cg = al_crop_growth.get(i);
                DropDownModelClass dc = new DropDownModelClass();
                dc.setName(cg.getGrowthStageName());
                dc.setId(cg.getGrowthStageId());
                al_demo.add(dc);
            }
        }
        return al_demo;
    }

    public void getImageResponse(final BaseActivity activity, final AllInterfaces.RetrofitResponseToActivityOrFrament retRes, String img, String token, String lang_id) {
        showProgressDialog(activity);
        APIService service = ApiClient.getClient().create(APIService.class);
        Call<MSG> userCall = service.uploadImage(img, token, lang_id);
        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hideProgressDialog();
                System.out.println("Message:" + response.body().getMessage());
                //onSignupSuccess();
                try {
                    if (response.code() == Constant.CODE_422) {
                        Toast.makeText(activity, response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                    } else if (response.code() == Constant.CODE_500) {
                        Toast.makeText(activity, response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                    } else if (response.code() == Constant.CODE_200) {
                        if (response.body().getSuccess()) {
                            /////Success////////////////
                            retRes.onSuccess(response, "imageUploadToServer");
                        } else {
                            /////Error Msg/////////////////////////
                            retRes.onFailure(response);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hideProgressDialog();
                retRes.onResponseFailure();
                Log.d("onFailure", t.toString());
            }
        });
    }

    public void showProgressDialog(BaseActivity baseActivity) {
        dialogBuilder = new AlertDialog.Builder(baseActivity);
        LayoutInflater inflater = (LayoutInflater) baseActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.progress_bar_layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);
        dialog = dialogBuilder.create();
        dialog.show();
    }

    public void showProgressDialog(BaseActivityWithoutMenu baseActivity) {
        dialogBuilder = new AlertDialog.Builder(baseActivity);
        LayoutInflater inflater = (LayoutInflater) baseActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.progress_bar_layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);
        dialog = dialogBuilder.create();
        dialog.show();
    }

    public void hideProgressDialog() {
        dialog.dismiss();
    }

    public void setLocale(Activity activity, String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = activity.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    public void getStaticDetailsRegistrationDataFromServer(final BaseActivity activity, final AllInterfaces.RetrofitResponseToActivityOrFrament retRes, String token, String language_id) {
        Response<MSG> response;
        showProgressDialog(activity);
        APIService service = ApiClient.getClient().create(APIService.class);
        Call<MSG> userCall = service.getStaticDetailsRegistrationData(token, language_id);
        System.out.println("Params:" + language_id + "...." + token);
        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hideProgressDialog();
                //onSignupSuccess();
                //Log.d("onResponse", "" + response.body().getData().getPlaceList().size());
                if (response.code() == Constant.CODE_422) {
                    Toast.makeText(activity, response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                } else if (response.code() == Constant.CODE_500) {
                    Toast.makeText(activity, response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                } else if (response.code() == Constant.CODE_200) {
                    if (response.body().getSuccess()) {
                        /////Success////////////////
                        retRes.onSuccess(response, "getStaticDetailsRegistrationDataFromServer");
                    } else {
                        /////Error Msg/////////////////////////
                        retRes.onFailure(response);
                    }
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hideProgressDialog();
                retRes.onResponseFailure();
                Log.d("onFailure", t.toString());
            }
        });
    }

    public ArrayList<DropDownModelClass> geAnimals(Activity activity, List<KsAnimalsList> al_animals_list) {
        ArrayList<DropDownModelClass> al_demo = new ArrayList<DropDownModelClass>();
        for (int i = 0; i < al_animals_list.size(); i++) {
            KsAnimalsList sd = al_animals_list.get(i);
            DropDownModelClass dc = new DropDownModelClass();
            dc.setName(sd.getAnimalName());
            dc.setId(sd.getId());
            al_demo.add(dc);
        }
        return al_demo;
    }

    public ArrayList<DropDownModelClass> geFarmers(Activity activity, List<KsFarmerTypesList> al_farmers) {
        ArrayList<DropDownModelClass> al_demo = new ArrayList<DropDownModelClass>();
        for (int i = 0; i < al_farmers.size(); i++) {
            KsFarmerTypesList sd = al_farmers.get(i);
            DropDownModelClass dc = new DropDownModelClass();
            dc.setName(sd.getFarmerTypeName());
            dc.setId(sd.getId());
            al_demo.add(dc);
        }
        return al_demo;
    }

    public ArrayList<DropDownModelClass> geFarmMachinary(Activity activity, List<KsFarmMachinerysList> al_farm_machinary) {
        ArrayList<DropDownModelClass> al_demo = new ArrayList<DropDownModelClass>();
        for (int i = 0; i < al_farm_machinary.size(); i++) {
            KsFarmMachinerysList sd = al_farm_machinary.get(i);
            DropDownModelClass dc = new DropDownModelClass();
            dc.setName(sd.getFarmMachineryName());
            dc.setId(sd.getId());
            al_demo.add(dc);
        }
        return al_demo;
    }

    public ArrayList<DropDownModelClass> geSeasonList(Activity activity, List<KsSeasonsList> al_season) {
        ArrayList<DropDownModelClass> al_demo = new ArrayList<DropDownModelClass>();
        for (int i = 0; i < al_season.size(); i++) {
            KsSeasonsList sd = al_season.get(i);
            DropDownModelClass dc = new DropDownModelClass();
            dc.setName(sd.getSeasonName());
            dc.setId(sd.getId());
            al_demo.add(dc);
        }
        return al_demo;
    }

    public ArrayList<DropDownModelClass> getProblemsList(Activity activity) {
        FarmerApplication fm = (FarmerApplication) activity.getApplicationContext();
        ArrayList<DropDownModelClass> al_problems = new ArrayList<DropDownModelClass>();
        for (int i = 0; i < fm.getAl_problems().size(); i++) {
            DropDownModelClass dc = new DropDownModelClass();
            dc.setId(fm.getAl_problems().get(i).getId());
            dc.setName(fm.getAl_problems().get(i).getProblemType());
            al_problems.add(dc);
        }
        return al_problems;
    }

    public ArrayList<DropDownModelClass> getUserTypeList(Activity activity) {
        FarmerApplication fm = (FarmerApplication) activity.getApplicationContext();
        ArrayList<DropDownModelClass> al_user_type = new ArrayList<DropDownModelClass>();
        for (int i = 0; i < fm.getAl_type_user().size(); i++) {
            DropDownModelClass dc = new DropDownModelClass();
            dc.setId(fm.getAl_type_user().get(i).getId());
            dc.setName(fm.getAl_type_user().get(i).getName());
            al_user_type.add(dc);
        }
        return al_user_type;
    }

    public void agriSubmit(final BaseActivity activity, final AllInterfaces.RetrofitResponseToActivityOrFrament retRes,
                           String question_submit_for,
                           String question_type_id,
                           String crop_type_id,
                           String crop_id,
                           String growth_stage_id,
                           String problem_type_id,
                           //String problem_id,
                           String user_id,
                           String farmer_name,
                           String farmer_mobile_no,
                           String state_id,
                           String district_id,
                           String subdivision_id,
                           String block_id,
                           String mouza_id,
                           String latitude,
                           String lognitude,
                           String question_title,
                           String long_description,
                           String image_save_data,
                           String token,
                           String lang_id) {
        showProgressDialog(activity);
        System.out.println("PARAM: " + "question_submit_for:" + question_submit_for + " question_type_id:" + question_type_id + " crop_type_id:" + crop_type_id + " crop_id:" + crop_id + " growth_stage_id:" + growth_stage_id + " problem_type_id:" + problem_type_id + " user_id:" +/*problem_id+*/user_id + " farmer_name:" + farmer_name + " farmer_mobile_no:" + farmer_mobile_no + " state_id:" +
                state_id + " district_id:" + district_id + " subdivision_id:" + subdivision_id + " block_id:" + block_id + " mouza_id:" + mouza_id + " latitude:" + latitude + " lognitude:" + lognitude + " question_title:" + question_title + " long_description:" +
                long_description + " image_save_data:" + image_save_data + " token:" + token + " frm_language_id:" + lang_id);
        APIService service = ApiClient.getClient().create(APIService.class);

        Call<MSG> userCall = service.agriProblemSubmit(question_submit_for, question_type_id, crop_type_id, crop_id, growth_stage_id, problem_type_id,/*problem_id,*/user_id, farmer_name, farmer_mobile_no,
                state_id, district_id, subdivision_id, block_id, mouza_id, latitude, lognitude, question_title,
                long_description, image_save_data, token, lang_id);
        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hideProgressDialog();
                //onSignupSuccess();
                System.out.println("ResponseCode:" + response.code());
                /*System.out.println("Msg:" + response.body().getMessage());*/
                if (response.code() == Constant.CODE_422) {
                    Toast.makeText(activity, response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                } else if (response.code() == Constant.CODE_500) {
                    Toast.makeText(activity, response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                } else if (response.code() == Constant.CODE_200) {
                    if (response.body().getSuccess()) {
                        System.out.println("Data:" + response.body().getQuestionAnswersData());
                        /////Success////////////////
                        retRes.onSuccess(response, "agriProblemSubmitted");
                    } else {
                        /////Error Msg/////////////////////////retRes
                        retRes.onFailure(response);
                    }
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hideProgressDialog();
                retRes.onResponseFailure();
                Log.d("onFailure", t.toString());
            }
        });
    }

    public void naturalDisasterSubmit(final BaseActivity activity, final AllInterfaces.RetrofitResponseToActivityOrFrament retRes,
                                      String question_submit_for,
                                      String question_type_id,
                                      String user_id,
                                      String farmer_name,
                                      String farmer_mobile_no,
                                      String state_id,
                                      String district_id,
                                      String subdivision_id,
                                      String block_id,
                                      String mouza_id,
                                      String effective_area,
                                      String latitude,
                                      String lognitude,
                                      String question_title,
                                      String long_description,
                                      String image_save_data,
                                      String token,
                                      String lang_id) {
        showProgressDialog(activity);
        System.out.println("PARAM: " + question_submit_for + "," + question_type_id + "," + user_id + "," + farmer_name + "," + farmer_mobile_no + "," +
                state_id + "," + district_id + "," + subdivision_id + "," + block_id + "," + mouza_id + " ," + effective_area + " ," + latitude + "," + lognitude + " ," + question_title + "," +
                long_description + "," + image_save_data + "," + token + "," + lang_id);
        APIService service = ApiClient.getClient().create(APIService.class);

        Call<MSG> userCall = service.naturalDisasterSubmit(question_submit_for, question_type_id, user_id, farmer_name, farmer_mobile_no,
                state_id, district_id, subdivision_id, block_id, mouza_id, effective_area, latitude, lognitude, question_title,
                long_description, image_save_data, token, lang_id);
        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hideProgressDialog();
                //onSignupSuccess();
                System.out.println("Response:" + response.code());
                System.out.println("Msg:" + response.body().getMessage());
                if (response.code() == Constant.CODE_422) {
                    Toast.makeText(activity, response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                } else if (response.code() == Constant.CODE_500) {
                    Toast.makeText(activity, response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                } else if (response.code() == Constant.CODE_200) {
                    if (response.body().getSuccess()) {
                        System.out.println("Data:" + response.body().getQuestionAnswersData());
                        /////Success////////////////
                        retRes.onSuccess(response, "naturalDisasterSubmitted");
                    } else {
                        /////Error Msg/////////////////////////retRes
                        retRes.onFailure(response);
                    }
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hideProgressDialog();
                retRes.onResponseFailure();
                Log.d("onFailure", t.toString());
            }
        });
    }

    public void setProfilePicassoImage(Activity activity, ImageView imageView, String imageUri) {
        if (imageUri.trim().length() <= 2) {
            Picasso.with(activity).load(R.drawable.image)
                    .resize(80, 80)
                    .transform(new CircleTransform())
                    .placeholder(R.drawable.image)
                    .error(R.drawable.image)
                    .into(imageView);
        } else {
            Picasso.with(activity).load(imageUri)
                    .resize(80, 80)
                    .transform(new CircleTransform())
                    .placeholder(R.drawable.image)
                    .error(R.drawable.image)
                    .into(imageView);
        }
    }

    public void setProfilePicassoImage(Activity activity, ImageView imageView, String imageUri, int width, int height) {
        if (imageUri.trim().length() <= 2) {
            Picasso.with(activity).load(R.drawable.image)
                    .resize(width, height)
                    //.transform(new CircleTransform())
                    .placeholder(R.drawable.image)
                    .error(R.drawable.image)
                    .into(imageView);
        } else {
            Picasso.with(activity).load(imageUri)
                    .resize(width, height)
                    //.transform(new CircleTransform())
                    .placeholder(R.drawable.image)
                    .error(R.drawable.image)
                    .into(imageView);
        }
    }

    public String getSomeDate(final String str)
            throws ParseException {
        SimpleDateFormat readFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat writeFormat = new SimpleDateFormat("MMM dd,yyyy");
        String startDate = null;
        try {
            startDate = readFormat.parse(str).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return startDate;
    }

    ////////////////////// FOR TEMPORARY PURPOSE /////////////////////
    public ArrayList<DropDownModelClass> getAllCropList(Activity activity) {
        FarmerApplication fm = (FarmerApplication) activity.getApplicationContext();
        List<CropsList> al_crops = new ArrayList<>();

        for (int i = 0; i < fm.getAl_type_crop().size(); i++) {
            for (int j = 0; j < fm.getAl_type_crop().get(i).getCropsList().size(); j++) {
                CropsList cp = new CropsList();
                cp = fm.getAl_type_crop().get(i).getCropsList().get(j);
                al_crops.add(cp);
            }
        }
        System.out.println("Sizeeee" + al_crops.size());
        ArrayList<DropDownModelClass> al_demo = new ArrayList<DropDownModelClass>();
        for (int i = 0; i < al_crops.size(); i++) {
            CropsList cp = al_crops.get(i);
            DropDownModelClass dc = new DropDownModelClass();
            dc.setName(cp.getCropsName());
            dc.setId(cp.getCropsId());
            al_demo.add(dc);
        }
        return al_demo;
    }

    public void detailRegistration(final BaseActivity activity, final AllInterfaces.RetrofitResponseToActivityOrFrament retRes,
                                   String user_id,
                                   String name,
                                   String frm_primary_number,
                                   String frm_gender,
                                   String frm_secondary_number,
                                   String frm_pincode,
                                   String frm_father_name,
                                   String frm_category,
                                   String frm_farmer_type,
                                   String frm_farmer_land_type,
                                   String frm_id_proof_number,
                                   String frm_id_proof,
                                   String frm_kcc_card,
                                   String frm_kcc_card_number,
                                   String frm_bank_account,
                                   String frm_bank_name,
                                   String frm_bank_branch,
                                   String frm_bank_ifsc,
                                   String frm_state,
                                   String frm_district,
                                   String frm_subdivision,
                                   String frm_block,
                                   String frm_mouza,
                                   String frm_total_cultivabe_land,
                                   String frm_farm_machinery,
                                   String frm_animal_number,
                                   String frm_cultivate_fish,
                                   String frm_cultivate_pond_number,
                                   String frm_language_id,
                                   String frm_season_rabi_list,
                                   String frm_season_kharif_list,
                                   String frm_season_pre_kharif_list,
                                   String frm_animal_list,
                                   String frm_farm_machinery_list,
                                   String email, String img, String usr_lang_id,
                                   String token) {
        showProgressDialog(activity);
        APIService service = ApiClient.getClient().create(APIService.class);

        System.out.println(" USER _ID " + user_id);

        Call<MSG> userCall = service.detailRegistration(user_id, name, frm_primary_number, frm_gender, frm_secondary_number, frm_pincode, frm_father_name, frm_category, frm_farmer_type,
                frm_farmer_land_type, frm_id_proof_number, frm_id_proof, frm_kcc_card, frm_kcc_card_number, frm_bank_account, frm_bank_name, frm_bank_branch, frm_bank_ifsc, frm_state,
                frm_district, frm_subdivision, frm_block, frm_mouza, frm_total_cultivabe_land, frm_farm_machinery, frm_animal_number, frm_cultivate_fish, frm_cultivate_pond_number, frm_language_id, frm_season_rabi_list, frm_season_kharif_list, frm_season_pre_kharif_list, frm_animal_list, frm_farm_machinery_list, email, img, usr_lang_id, token);

        System.out.println(" PARAM " + " name:" + name + " frm_primary_number:" + frm_primary_number + " frm_gender:" + frm_gender + " frm_secondary_number:" + frm_secondary_number + " frm_pincode:" + frm_pincode + " frm_father_name:" + frm_father_name + " frm_category:" + frm_category + " frm_farmer_type:" + frm_farmer_type + " frm_farmer_land_type:" +
                frm_farmer_land_type + " frm_id_proof_number:" + frm_id_proof_number + " frm_id_proof:" + frm_id_proof + " frm_kcc_card:" + frm_kcc_card + " frm_kcc_card_number:" + frm_kcc_card_number + " frm_bank_account:" + frm_bank_account + " frm_bank_name:" + frm_bank_name + " frm_bank_branch:" + frm_bank_branch + " frm_bank_ifsc:" + frm_bank_ifsc + " frm_state:" + frm_state + " frm_district:" +
                frm_district + " frm_subdivision:" + frm_subdivision + " frm_block:" + frm_block + " frm_mouza:" + frm_mouza + " frm_total_cultivabe_land:" + frm_total_cultivabe_land + " frm_farm_machinery:" + frm_farm_machinery + " frm_animal_number:" + frm_animal_number + " frm_cultivate_fish:" + frm_cultivate_fish + " frm_cultivate_pond_number:" + frm_cultivate_pond_number + " frm_language_id:" + frm_language_id + " frm_season_rabi_list:" + frm_season_rabi_list + " frm_season_kharif_list:" + frm_season_kharif_list + " frm_season_pre_kharif_list:" + frm_season_pre_kharif_list + " frm_animal_list:" + frm_animal_list + " frm_farm_machinery_list:"
                + frm_farm_machinery_list + " email:" + email + " user_language_id:" + usr_lang_id + " token:" + token);

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hideProgressDialog();
                System.out.println("code:" + response.code());
                //onSignupSuccess();
                if (response.code() == Constant.CODE_422) {
                    Toast.makeText(activity, response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                } else if (response.code() == Constant.CODE_500) {
                    Toast.makeText(activity, response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                } else if (response.code() == Constant.CODE_200) {
                    if (response.body().getSuccess()) {
                        System.out.println("Inside Succes");
                        /////Success////////////////
                        retRes.onSuccess(response, "detailRegistration");
                    } else {
                        /////Error Msg/////////////////////////retRes
                        System.out.println("Inside Failure");
                        retRes.onFailure(response);
                    }
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                System.out.println("Inside onFailure");
                hideProgressDialog();
                retRes.onResponseFailure();
                Log.d("onFailure", t.toString());
            }
        });
    }

    public void newDetailRegistration(final BaseActivity activity, final AllInterfaces.RetrofitResponseToActivityOrFrament retRes,
                                      String name,
                                      String frm_primary_number,
                                      String frm_gender,
                                      String frm_secondary_number,
                                      String frm_pincode,
                                      String frm_father_name,
                                      String frm_category,
                                      String frm_farmer_type,
                                      String frm_farmer_land_type,
                                      String frm_id_proof_number,
                                      String frm_id_proof,
                                      String frm_kcc_card,
                                      String frm_kcc_card_number,
                                      String frm_bank_account,
                                      String frm_bank_name,
                                      String frm_bank_branch,
                                      String frm_bank_ifsc,
                                      String frm_state,
                                      String frm_district,
                                      String frm_subdivision,
                                      String frm_block,
                                      String frm_mouza,
                                      String frm_total_cultivabe_land,
                                      String frm_farm_machinery,
                                      String frm_animal_number,
                                      String frm_cultivate_fish,
                                      String frm_cultivate_pond_number,
                                      String frm_language_id,
                                      String frm_season_rabi_list,
                                      String frm_season_kharif_list,
                                      String frm_season_pre_kharif_list,
                                      String frm_animal_list,
                                      String frm_farm_machinery_list,
                                      String email,
                                      String token,
                                      String img,
                                      String pass,
                                      String lang_id, String user_id) {
        showProgressDialog(activity);
        APIService service = ApiClient.getClient().create(APIService.class);

        System.out.println(" USER _ID " + user_id);

        Call<MSG> userCall = service.newDetailRegistration(name, frm_primary_number, frm_gender, frm_secondary_number, frm_pincode, frm_father_name, frm_category, frm_farmer_type,
                frm_farmer_land_type, frm_id_proof_number, frm_id_proof, frm_kcc_card, frm_kcc_card_number, frm_bank_account, frm_bank_name, frm_bank_branch, frm_bank_ifsc, frm_state,
                frm_district, frm_subdivision, frm_block, frm_mouza, frm_total_cultivabe_land, frm_farm_machinery, frm_animal_number, frm_cultivate_fish, frm_cultivate_pond_number, frm_language_id, frm_season_rabi_list, frm_season_kharif_list, frm_season_pre_kharif_list, frm_animal_list, frm_farm_machinery_list, email, token, img, pass, lang_id, user_id);

        System.out.println(" PARAM " + " name:" + name + " frm_primary_number:" + frm_primary_number + " frm_gender:" + frm_gender + " frm_secondary_number:" + frm_secondary_number + " frm_pincode:" + frm_pincode + " frm_father_name:" + frm_father_name + " frm_category:" + frm_category + " frm_farmer_type:" + frm_farmer_type + " frm_farmer_land_type:" +
                frm_farmer_land_type + " frm_id_proof_number:" + frm_id_proof_number + " frm_id_proof:" + frm_id_proof + " frm_kcc_card:" + frm_kcc_card + " frm_kcc_card_number:" + frm_kcc_card_number + " frm_bank_account:" + frm_bank_account + " frm_bank_name:" + frm_bank_name + "frm_bank_branch:" + frm_bank_branch + " frm_bank_ifsc:" + frm_bank_ifsc + " frm_state:" + frm_state + " frm_district:" +
                frm_district + " frm_subdivision:" + frm_subdivision + " frm_block:" + frm_block + " frm_mouza:" + frm_mouza + " frm_total_cultivabe_land:" + frm_total_cultivabe_land + " frm_farm_machinery:" + frm_farm_machinery + " frm_animal_number:" + frm_animal_number + " frm_cultivate_fish:" + frm_cultivate_fish + " frm_cultivate_pond_number:" + frm_cultivate_pond_number + " frm_language_id:" + frm_language_id + " frm_season_rabi_list:" + frm_season_rabi_list + " frm_season_kharif_list:" + frm_season_kharif_list + " frm_season_pre_kharif_list:" + frm_season_pre_kharif_list + " frm_animal_list:" + frm_animal_list + " frm_farm_machinery_list:"
                + frm_farm_machinery_list + " email:" + email + " token:" + token + " frm_farmer_profile_image:" + img + " password:" + pass + " user_language_id:" + lang_id + " approved_by_user_id:" + user_id + " ;");

        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                hideProgressDialog();
                //onSignupSuccess();
                System.out.println("Code:" + response.code());
                if (response.code() == Constant.CODE_422) {
                    Toast.makeText(activity, response.toString(), Toast.LENGTH_SHORT).show();
                } else if (response.code() == Constant.CODE_500) {
                    Toast.makeText(activity, response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                } else if (response.code() == Constant.CODE_200) {
                    if (response.body().getSuccess()) {
                        System.out.println("Inside Succes");
                        /////Success////////////////
                        retRes.onSuccess(response, "newdetailRegistration");
                    } else {
                        /////Error Msg/////////////////////////retRes
                        System.out.println("Inside Failure");
                        retRes.onFailure(response);
                    }
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                System.out.println("Inside onFailure");
                hideProgressDialog();
                retRes.onResponseFailure();
                Log.d("onFailure", t.toString());
            }
        });
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    public void getQuestionsList(final BaseActivity activity, final AllInterfaces.RetrofitResponseToActivityOrFrament retRes, String user_id, String token, String question_type_id, String lang_id) {
        System.out.println("Inside getQuestionsList");
        Response<MSG> response;
        //showProgressDialog(activity);
        APIService service = ApiClient.getClient().create(APIService.class);
        System.out.println(" question_type_id:: " + question_type_id + "," + user_id + "," + token + "," + lang_id);
        Call<MSG> userCall = service.getList(user_id, token, question_type_id, lang_id);
        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                //hideProgressDialog();
                //onSignupSuccess();
                // Log.d("onResponse", "" + response.body().getGetList().size());
                if (response.code() == Constant.CODE_422) {
                    Toast.makeText(activity, response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                } else if (response.code() == Constant.CODE_500) {
                    Toast.makeText(activity, response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                } else if (response.code() == Constant.CODE_200) {
                    if (response.body().getSuccess()) {
                        /////Success////////////////
                        retRes.onSuccess(response, "getQuestionsList");
                    } else {
                        /////Error Msg/////////////////////////
                        retRes.onFailure(response);
                    }
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hideProgressDialog();
                retRes.onResponseFailure();
                Log.d("onFailure", t.toString());
            }
        });
    }

    public void getOtherQuestionsList(final BaseActivity activity, final AllInterfaces.RetrofitResponseToActivityOrFrament retRes, String user_id, String token, String question_type_id, String lang_id) {
        System.out.println("Inside getOtherQuestionsList");
        Response<MSG> response;
        //showProgressDialog(activity);
        APIService service = ApiClient.getClient().create(APIService.class);
        //System.out.println(" BLOCK ID "+block_id);
        System.out.println("Send Params_ques:" + user_id + "," + question_type_id + "," + lang_id + "," + token);
        Call<MSG> userCall = service.getOtherList(user_id, token, question_type_id, lang_id);
        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                //hideProgressDialog();
                //onSignupSuccess();
                //Log.d("onResponse", "" + response.body().getGetList().size());
                if (response.code() == Constant.CODE_422) {
                    Toast.makeText(activity, response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                } else if (response.code() == Constant.CODE_500) {
                    Toast.makeText(activity, response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                } else if (response.code() == Constant.CODE_200) {
                    if (response.body().getSuccess()) {
                        /////Success////////////////
                        System.out.println("response code" + response.code());
                        retRes.onSuccess(response, "getOtherQuestionsList");
                    } else {
                        /////Error Msg/////////////////////////
                        retRes.onFailure(response);
                    }
                }
            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hideProgressDialog();
                retRes.onResponseFailure();
                Log.d("onFailure", t.toString());
            }
        });
    }

    public void getUserDetails(final BaseActivity activity, final AllInterfaces.RetrofitResponseToActivityOrFrament retRes, String id, String lang_id) {
        //showProgressDialog(activity);
        APIService service = ApiClient.getClient().create(APIService.class);
        System.out.println("Send Params:" + id + "," + lang_id);
        Call<MSG> userCall = service.getUserDetails(id, lang_id);
        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                //hideProgressDialog();
                System.out.println("Inside GetUserDetails");
                System.out.println("Message:" + response.body().getGetUserDetails().getName());
                System.out.println("Response Code:" + response.code());
                //onSignupSuccess();
                try {
                    if (response.body().getSuccess()) {
                        /////Success////////////////
                        System.out.println("Inside try");
                        retRes.onSuccess(response, "getUserDetails");
                    } else {
                        /////Error Msg/////////////////////////
                        retRes.onFailure(response);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hideProgressDialog();
                retRes.onResponseFailure();
                Log.d("onFailure", t.toString());
            }
        });
    }

    public void getFarmerList(final BaseActivity activity, final AllInterfaces.RetrofitResponseToActivityOrFrament retRes, String id, String token, String lang_id) {
        // showProgressDialog(activity);
        APIService service = ApiClient.getClient().create(APIService.class);
        System.out.println("Send Params:" + id + "," + lang_id);
        Call<MSG> userCall = service.getFramerList(id, token, lang_id);
        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                //hideProgressDialog();
                System.out.println("Inside getFarmerList");
                //onSignupSuccess();
                try {
                    if (response.code() == Constant.CODE_422) {
                        Toast.makeText(activity, response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                    } else if (response.code() == Constant.CODE_500) {
                        Toast.makeText(activity, response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                    } else if (response.code() == Constant.CODE_200) {
                        if (response.body().getSuccess()) {
                            /////Success////////////////
                            retRes.onSuccess(response, "getFarmerList");
                        } else {
                            /////Error Msg/////////////////////////
                            retRes.onFailure(response);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hideProgressDialog();
                retRes.onResponseFailure();
                Log.d("onFailure", t.toString());
            }
        });
    }

    public void getFarmerListinActivity(final BaseActivityWithoutMenu activity, final AllInterfaces.RetrofitResponseToActivityOrFrament retRes, String id, String token, String lang_id) {
        // showProgressDialog(activity);
        APIService service = ApiClient.getClient().create(APIService.class);
        System.out.println("Send Params:" + id + "," + lang_id);
        Call<MSG> userCall = service.getFramerList(id, token, lang_id);
        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                //hideProgressDialog();
                System.out.println("Inside getFarmerList");
                //onSignupSuccess();
                try {
                    if (response.code() == Constant.CODE_422) {
                        Toast.makeText(activity, response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                    } else if (response.code() == Constant.CODE_500) {
                        Toast.makeText(activity, response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                    } else if (response.code() == Constant.CODE_200) {
                        if (response.body().getSuccess()) {
                            /////Success////////////////
                            retRes.onSuccess(response, "getFarmerList");
                        } else {
                            /////Error Msg/////////////////////////
                            retRes.onFailure(response);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hideProgressDialog();
                retRes.onResponseFailure();
                Log.d("onFailure", t.toString());
            }
        });
    }

    public void getAnswerList(final BaseActivity activity, final AllInterfaces.RetrofitResponseToActivityOrFrament retRes, String qus_id, String token, String lang_id, String question_type_id) {
        // showProgressDialog(activity);
        APIService service = ApiClient.getClient().create(APIService.class);
        System.out.println("Send Params:" + qus_id + "," + lang_id + "," + token + "," + question_type_id);
        Call<MSG> userCall = service.getAnswerList(qus_id, token, lang_id, question_type_id);
        userCall.enqueue(new Callback<MSG>() {
            @Override
            public void onResponse(Call<MSG> call, Response<MSG> response) {
                //hideProgressDialog();
                System.out.println("Inside getAnswerList");
                //onSignupSuccess();
                try {
                    if (response.code() == Constant.CODE_422) {
                        Toast.makeText(activity, response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                    } else if (response.code() == Constant.CODE_500) {
                        Toast.makeText(activity, response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                    } else if (response.code() == Constant.CODE_200) {
                        if (response.body().getSuccess()) {
                            /////Success////////////////
                            retRes.onSuccess(response, "getAnswerList");
                        } else {
                            /////Error Msg/////////////////////////
                            retRes.onFailure(response);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<MSG> call, Throwable t) {
                hideProgressDialog();
                retRes.onResponseFailure();
                Log.d("onFailure", t.toString());
            }
        });
    }

    public void downloadKeyboard(final Activity activity, final AllInterfaces.KeyboardDialogCallback callback) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(activity);
        builder1.setMessage(R.string.dwnld_keybrd);
        builder1.setCancelable(false);

        builder1.setPositiveButton(activity.getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        callback.onOkClick();
                        dialog.cancel();
                    }

                });

        builder1.setNegativeButton(activity.getString(R.string.cancle),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        callback.onCancleClick();
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
}

