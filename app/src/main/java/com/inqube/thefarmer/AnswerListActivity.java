package com.inqube.thefarmer;

import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.inqube.thefarmer.Widget.HTMLTextView;
import com.inqube.thefarmer.base.BaseActivity;
import com.inqube.thefarmer.model.MSG;

import java.util.ArrayList;

import Utils.Constant;
import Utils.UtilClass;
import retrofit2.Response;

public class AnswerListActivity extends BaseActivity {

    /////////////////// UI VARIABLES ///////////////////
    private HTMLTextView tv_question, tv_answer, tv_short_description;
    private ImageButton imb_prev, imb_next;
    private ImageView imv_prob_ic;
    //////////////////////////////////////////
    ///////////////NORMAL VARIABLES///////////
    private String id, question_type_id, question;
    private int img_pos = 0, img_size = 0;
    private Response<MSG> response;

    ///////////////////////////////////////////
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        onCreate(savedInstanceState, R.layout.activity_answer_list);
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        id = intent.getStringExtra("ques_id");
        question_type_id = intent.getStringExtra("question_type_id");
        question = intent.getStringExtra("question");
        System.out.println("qus_id:" + id);
    }

    @Override
    protected void setUI() {
        super.setUI();

        col_holder = (CoordinatorLayout) findViewById(R.id.col_holder);

        setToolbarTitle(R.string.q_a);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tv_question = (HTMLTextView) findViewById(R.id.tv_question);
        tv_question.setText(question);

        tv_answer = (HTMLTextView) findViewById(R.id.tv_answer);

        tv_short_description = (HTMLTextView) findViewById(R.id.tv_short_description);

        imv_prob_ic = (ImageView) findViewById(R.id.imv_prob_ic);

        imb_prev = (ImageButton) findViewById(R.id.imb_prev);
        imb_prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (img_size > 0) {
                    if (img_pos > 0) {
                        img_pos--;
                        UtilClass.getInstance().setProfilePicassoImage(AnswerListActivity.this, imv_prob_ic, response.body().getAnswerData().get(0).getImageList().get(img_pos).getImageSource(), 150, 150);
                    }
                }
            }
        });
        imb_next = (ImageButton) findViewById(R.id.imb_next);
        imb_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (img_size > 0) {
                    if (img_pos < (img_size - 1)) {
                        img_pos++;
                        UtilClass.getInstance().setProfilePicassoImage(AnswerListActivity.this, imv_prob_ic, response.body().getAnswerData().get(0).getImageList().get(img_pos).getImageSource(), 150, 150);
                    }
                }
            }
        });

        UtilClass.getInstance().getAnswerList(this, this, id,
                getUserPreference(Constant.TOKEN_ID, ""), getUserPreference(Constant.SELECTED_LANGUAGE, BuildConfig.MARATHI), question_type_id);
    }

    @Override
    public void onSuccess(Response<MSG> response, String which_method) {
        super.onSuccess(response, which_method);
        this.response = response;
        if (which_method.equalsIgnoreCase("getAnswerList")) {
            tv_question.setText("Q. " + response.body().getAnswerData().get(0).getQuestion_title());
            System.out.println("Answer1" + response.body().getAnswerData().get(0).getAnswerComment());
            System.out.println();
            for (int i = 0; i < response.body().getAnswerData().size(); i++) {
                System.out.println("Answer2" + response.body().getAnswerData().get(i).getAnswerComment());
                tv_answer.append(response.body().getAnswerData().get(i).getAnswerComment());
            }
            tv_short_description.setText(response.body().getAnswerData().get(0).getLong_description());
            if (response.body().getAnswerData().get(0).getImageList().size() > 0) {
                img_size = response.body().getAnswerData().get(0).getImageList().size();
                System.out.println(" IMAGE SIZE " + img_size);
                UtilClass.getInstance().setProfilePicassoImage(AnswerListActivity.this, imv_prob_ic, response.body().getAnswerData().get(0).getImageList().get(img_pos).getImageSource(), 150, 150);
            }
        }
    }

    @Override
    public void onFailure(Response<MSG> response) {
        super.onFailure(response);
        createSnackBar(col_holder, response.body().getMessage());
    }

    @Override
    public void onResponseFailure() {
        super.onResponseFailure();
        createSnackBar(col_holder, getString(R.string.sorry_server_error));
    }
}
