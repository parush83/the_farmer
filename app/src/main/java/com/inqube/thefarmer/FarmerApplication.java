package com.inqube.thefarmer;

import android.app.Application;
import android.support.multidex.MultiDexApplication;

import com.inqube.thefarmer.base.BaseActivity;
import com.inqube.thefarmer.model.BlockList;
import com.inqube.thefarmer.model.CropTypeList;
import com.inqube.thefarmer.model.DistrictsList;
import com.inqube.thefarmer.model.GetMouzaList;
import com.inqube.thefarmer.model.GetRoleList;
import com.inqube.thefarmer.model.KsProblemTypesList;
import com.inqube.thefarmer.model.PlaceList;
import com.inqube.thefarmer.model.SubdivisionList;

import java.util.ArrayList;
import java.util.List;

import Utils.ApplicationLifecycleManager;

/**
 * Created by sanjib on 19/11/17.
 */

public class FarmerApplication extends MultiDexApplication {

    public BaseActivity farmer_base_activity;
    private List<KsProblemTypesList> al_problems;
    private List<PlaceList> al_place;
    private List<CropTypeList> al_type_crop;
    private List<GetRoleList> al_type_user;

    @Override
    public void onCreate() {
        super.onCreate();
        registerActivityLifecycleCallbacks(new ApplicationLifecycleManager());
    }

    public List<KsProblemTypesList> getAl_problems() {
        return al_problems;
    }

    public void setAl_problems(List<KsProblemTypesList> al_problems) {
        this.al_problems = al_problems;
    }

    public List<PlaceList> getAl_place() {
        return al_place;
    }

    public void setAl_place(List<PlaceList> al_place) {
        this.al_place = al_place;
    }

    public List<CropTypeList> getAl_type_crop() {
        return al_type_crop;
    }

    public void setAl_type_crop(List<CropTypeList> al_type_crop) {
        this.al_type_crop = al_type_crop;
    }

    public List<GetRoleList> getAl_type_user() {
        return al_type_user;
    }

    public void setAl_type_user(List<GetRoleList> al_type_user) {
        this.al_type_user = al_type_user;
    }
}
