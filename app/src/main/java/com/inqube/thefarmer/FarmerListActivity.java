package com.inqube.thefarmer;

import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.gson.Gson;
import com.inqube.thefarmer.adapter.FarmerListAdapter;
import com.inqube.thefarmer.base.BaseActivity;
import com.inqube.thefarmer.base.BaseActivityWithoutMenu;
import com.inqube.thefarmer.fragment.BankDetailsFragment;
import com.inqube.thefarmer.fragment.PersonalDetailsFragment;
import com.inqube.thefarmer.model.MSG;

import Utils.AllInterfaces;
import Utils.Constant;
import Utils.UtilClass;
import retrofit2.Response;

public class FarmerListActivity extends BaseActivityWithoutMenu {
    FarmerListAdapter adapter;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        onCreate(savedInstanceState, R.layout.activity_farmer_list);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!resume) {
            setUI();
            //UtilClass.getInstance().getQuestionsList((BaseActivity) getActivity(), this, ((BaseActivity) getActivity()).getUserPreference(Constant.USER_ID, ""), ((BaseActivity) getActivity()).getUserPreference(Constant.TOKEN_ID, ""), ((BaseActivity) getActivity()).getDataPreference(Constant.WHICH_PROBLEM, ""));
        }
        //UtilClass.getInstance().getOtherQuestionsList((BaseActivity)getActivity(),this,((BaseActivity )  getActivity()).getUserPreference(Constant.USER_ID,""),((BaseActivity )  getActivity()).getUserPreference(Constant.TOKEN_ID,""),"1");
        UtilClass.getInstance().getFarmerListinActivity(this, this, getUserPreference(Constant.USER_ID, ""), getUserPreference(Constant.TOKEN_ID, ""), getUserPreference(Constant.SELECTED_LANGUAGE, BuildConfig.MARATHI +
                "" +
                ""));
        resume = false;
    }


    @Override
    protected void setUI() {
        super.setUI();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(R.string.app_name);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        GridLayoutManager llm = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(llm);


    }

    @Override
    public void onSuccess(Response<MSG> response, String which_method) {
        super.onSuccess(response, which_method);
        if (which_method.equalsIgnoreCase("getFarmerList")) {
            System.out.println("inside getFramerList Success");
            //question_loaded=true;

            final MSG msg = new Gson().fromJson(new Gson().toJson(response.body()), MSG.class);
            System.out.println("Size:" + msg.getGetuserList().size());
            //list.setAdapter( adapter );
            /*adapter=new FarmerListAdapter(recyclerView, this, msg.getGetuserList(), col_holder, new AllInterfaces.CustomItemClickListener() {
                @Override
                public void onItemClick(View v, int position) {

                }
            });*/
            recyclerView.setAdapter(new FarmerListAdapter(recyclerView, this, msg.getGetuserList(), col_holder, new AllInterfaces.CustomItemClickListener() {
                @Override
                public void onItemClick(View v, int position, String name) {
                    //saveDataPreference(Constant.TYPE,"farmer_edit");
                    saveDataPreference(Constant.FARMER_ID, "" + msg.getGetuserList().get(position).getId());
                    saveDataPreference(Constant.FRM_NAME, "" + msg.getGetuserList().get(position).getName());
                    saveDataPreference(Constant.FRM_MOB, "" + msg.getGetuserList().get(position).getFrmPrimaryNumber());
                    //Intent intent=new Intent(FarmerListActivity.this, BankDetailsFragment.class)
                    System.out.println("Farmer Id::" + getDataPreference(Constant.FRM_NAME, ""));
                    finish();

                }
            }));
        }
    }

    @Override
    public void onFailure(Response<MSG> response) {
        createSnackBar(col_holder, response.body().getMessage());
    }

    @Override
    public void onResponseFailure() {
        createSnackBar(col_holder, getString(R.string.sorry_server_error));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (TextUtils.isEmpty(newText)) {
                    /*adapter.filter("");
                    listView.clearTextFilter();*/
                } else {
                    //adapter.filter(newText);
                }
                return true;
            }
        });

        return true;
    }

}
