package com.inqube.thefarmer;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.inqube.thefarmer.Widget.MyEditText;
import com.inqube.thefarmer.base.BaseActivity;
import com.inqube.thefarmer.base.BaseActivityWithoutMenu;
import com.inqube.thefarmer.model.MSG;
import com.inqube.thefarmer.service.GPSTracker;

import Utils.AllInterfaces;
import Utils.Constant;
import Utils.UtilClass;
import retrofit2.Response;

/**
 * Created by Tubu on 11/16/2017.
 */

public class LoginActivity extends BaseActivityWithoutMenu implements View.OnClickListener, CompoundButton.OnCheckedChangeListener, AllInterfaces.ForgotPassword {
    ////////////////////// UI VARIABLES//////////////
    private LinearLayout ll_agri_plan, ll_animal_breeding, ll_fishery_breeding;
    private MyEditText edt_user_id, edt_password;
    private CheckBox chk_remember_me;
    private ImageButton imb_forgot_password, imb_register, imb_login, imb_toll_free;
    //////////////////////////////////////////////////
    ////////////////////NORMAL VARIABLES//////////////
    private Double latitude, longitude;

    //////////////////////////////////////////////////
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        onCreate(savedInstanceState, R.layout.activity_login);
        super.onCreate(savedInstanceState);
    }

    /*@Override
    protected void onResume() {
        super.onResume();
            LocalBroadcastManager.getInstance(LoginActivity.this).registerReceiver(
                    new BroadcastReceiver() {
                        @Override
                        public void onReceive(Context context, Intent intent) {
                            latitude = Double.parseDouble(intent.getStringExtra("EXTRA_LATITUDE"));
                            longitude = Double.parseDouble(intent.getStringExtra("EXTRA_LONGITUDE"));
                            System.out.println("latitude: " + latitude + "longitude: " + longitude);
                            try {
                                Toast.makeText(LoginActivity.this, "Lat"+String.valueOf(latitude), Toast.LENGTH_SHORT).show();
                                Toast.makeText(LoginActivity.this, "Lon"+String.valueOf(longitude), Toast.LENGTH_SHORT).show();
                           *//* tv_latitude.setText(String.valueOf(latitude));
                            tv_longitude.setText(String.valueOf(longitude));*//*
                            } catch (Exception e) {
                                System.out.println("O Maa go Jhar Kheyechhe");
                            }
                        }
                    }, new IntentFilter(GPSTracker.ACTION_LOCATION_BROADCAST)
            );
            if (ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

            } else {
                GPSTracker gps = new GPSTracker(LoginActivity.this, LoginActivity.this);
                if (gps.canGetLocation()) {

                    latitude = gps.getLatitude();
                    longitude = gps.getLongitude();

                    try {

                    } catch (Exception e) {
                        System.out.println("O Maa go Jhar Kheyechhe");
                    }
                    Toast.makeText(LoginActivity.this, "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                } else {
                    gps.showSettingsAlert();
                }
            }
            //setUI();

    }*/

    @Override
    protected void setUI() {
        super.setUI();

        col_holder = (CoordinatorLayout) findViewById(R.id.col_holder);

        ll_agri_plan = (LinearLayout) findViewById(R.id.ll_agri_plan);
        ll_agri_plan.setOnClickListener(this);

        ll_animal_breeding = (LinearLayout) findViewById(R.id.ll_animal_breeding);
        ll_animal_breeding.setOnClickListener(this);

        ll_fishery_breeding = (LinearLayout) findViewById(R.id.ll_fishery_breeding);
        ll_fishery_breeding.setOnClickListener(this);

        edt_user_id = (MyEditText) findViewById(R.id.edt_user_id);
        edt_password = (MyEditText) findViewById(R.id.edt_password);

//        chk_remember_me = (CheckBox) findViewById(R.id.chk_remember_me);
//        chk_remember_me.setOnCheckedChangeListener(this);

        imb_forgot_password = (ImageButton) findViewById(R.id.imb_forgot_password);
        imb_forgot_password.setOnClickListener(this);

        imb_register = (ImageButton) findViewById(R.id.imb_register);
        imb_register.setOnClickListener(this);

        imb_login = (ImageButton) findViewById(R.id.imb_login);
        imb_login.setOnClickListener(this);

        imb_toll_free = (ImageButton) findViewById(R.id.imb_toll_free);
        imb_toll_free.setOnClickListener(this);

        LocalBroadcastManager.getInstance(LoginActivity.this).registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        latitude = Double.parseDouble(intent.getStringExtra("EXTRA_LATITUDE"));
                        longitude = Double.parseDouble(intent.getStringExtra("EXTRA_LONGITUDE"));
                        System.out.println("latitude: " + latitude + "longitude: " + longitude);
                        try {
                            /*Toast.makeText(LoginActivity.this, "Lat"+String.valueOf(latitude), Toast.LENGTH_SHORT).show();
                            Toast.makeText(LoginActivity.this, "Lon"+String.valueOf(longitude), Toast.LENGTH_SHORT).show();*/
                           /* tv_latitude.setText(String.valueOf(latitude));
                            tv_longitude.setText(String.valueOf(longitude));*/
                        } catch (Exception e) {
                            System.out.println("O Maa go Jhar Kheyechhe");
                        }
                    }
                }, new IntentFilter(GPSTracker.ACTION_LOCATION_BROADCAST)
        );
        if (ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        } else {
            GPSTracker gps = new GPSTracker(LoginActivity.this, LoginActivity.this);
            if (gps.canGetLocation()) {

                latitude = gps.getLatitude();
                longitude = gps.getLongitude();

                try {

                } catch (Exception e) {
                    System.out.println("O Maa go Jhar Kheyechhe");
                }
                //Toast.makeText(LoginActivity.this, "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
            } else {
                gps.showSettingsAlert();
            }
        }
    }

    @Override
    public void onClick(View view) {
        hideKeyboard(this);
        switch (view.getId()) {
            case R.id.ll_agri_plan:
                break;
            case R.id.ll_animal_breeding:
                break;
            case R.id.ll_fishery_breeding:
                break;
            case R.id.imb_forgot_password:
                UtilClass.getInstance().forgotPassDialog(this, this);
                break;
            case R.id.imb_register:
                if (isDeviceOnline()) {
                    startActivity(new Intent(LoginActivity.this, ShortRegistrationActivity.class));
                }
                break;
            case R.id.imb_login:
                if (checkLoginValidity() == 0) {
                    // call login webservice
                    if (isDeviceOnline()) {
                        UtilClass.getInstance().loginToServer(LoginActivity.this, LoginActivity.this,
                                edt_user_id.getText().toString().trim(),
                                edt_password.getText().toString().trim(), String.valueOf(latitude), String.valueOf(longitude));
                    }
                }
                break;
            case R.id.imb_toll_free:
                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        switch (compoundButton.getId()) {
//            case R.id.chk_remember_me:
//                if(b){
//                    saveUserPreference(Constant.REMEMBER_ME,"yes");
//                }else{
//                    saveUserPreference(Constant.REMEMBER_ME,"no");
//                }
//                break;
        }
    }

    private int checkLoginValidity() {
        if (TextUtils.isEmpty(edt_user_id.getText().toString())) {
            createSnackBar(edt_user_id, getString(R.string.user_id_cannot_be_blank));
            return 1;
        }
        if (TextUtils.isEmpty(edt_password.getText().toString())) {
            createSnackBar(edt_user_id, getString(R.string.please_enter_password));
            return 1;
        }
        if (edt_user_id.getText().toString().trim().length() < 10) {
            createSnackBar(edt_user_id, getString(R.string.please_enter_ten_digits_password)
            );
            return 1;
        }
        return 0;
    }

    @Override
    public void onForgotPasswqordProperDataSubmitted(String otp, String newPassword, String user_id) {
        UtilClass.getInstance().forgotPasswordSecond(LoginActivity.this, LoginActivity.this, user_id, otp, newPassword);
    }

    @Override
    public void onSuccess(Response<MSG> response, String which_method) {
        super.onSuccess(response, which_method);
        if (which_method.equalsIgnoreCase("loginToServer")) {
            //createSnackBar(col_holder,response.body().getMessage());
            saveUserPreference(Constant.TOKEN_ID, response.body().getTokenId().getToken());
            StringBuffer s = new StringBuffer();

            saveUserPreference(Constant.USER_ID, response.body().getUserDetails().get(0).getId().toString());
            saveUserPreference(Constant.USER_IMAGE, response.body().getUserDetails().get(0).getProfileImageSource().toString());
            saveUserPreference(Constant.USER_NAME, response.body().getUserDetails().get(0).getName().toString());
            saveUserPreference(Constant.LAST_LOGIN, response.body().getUserDetails().get(0).getFrmLastLogin().toString());
            saveUserPreference(Constant.SELECTED_LANGUAGE, "" + response.body().getUserDetails().get(0).getFrmLanguageId());
            saveUserPreference(Constant.EMAIL, "" + response.body().getUserDetails().get(0).getEmail());
            saveUserPreference(Constant.FARMER_NAME, "" + response.body().getUserDetails().get(0).getName());
            saveUserPreference(Constant.FARMER_PRIMARY_NUMBER, "" + response.body().getUserDetails().get(0).getFrmPrimaryNumber());

            startActivity(new Intent(LoginActivity.this, MainHomeActivity.class));
            finish();
        } else if (which_method.equalsIgnoreCase("forgotPassword")) {
            createSnackBar(col_holder, response.body().getMessage());
            UtilClass.getInstance().newPasswordDialog(LoginActivity.this, LoginActivity.this, response.body().getUserdetails_data());
        } else if (which_method.equalsIgnoreCase("forgotPasswordSecond")) {
            createSnackBar(col_holder, response.body().getMessage());
        }
    }

    @Override
    public void onFailure(Response<MSG> response) {
        createSnackBar(col_holder, response.body().getMessage());
    }

    @Override
    public void onResponseFailure() {
        createSnackBar(col_holder, getString(R.string.sorry_server_error));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    GPSTracker gps = new GPSTracker(LoginActivity.this, LoginActivity.this);
                    if (gps.canGetLocation()) {
                        latitude = gps.getLatitude();
                        longitude = gps.getLongitude();
                        try {
                            /*Toast.makeText(gps, "Lat"+String.valueOf(latitude), Toast.LENGTH_SHORT).show();
                            Toast.makeText(gps, "Lon"+String.valueOf(longitude), Toast.LENGTH_SHORT).show();*/
                        } catch (Exception e) {
                            System.out.println("O Maa go Jhar Kheyechhe");
                        }
                    } else {
                        gps.showSettingsAlert();
                    }
                } else {
                    //  Toast.makeText(SoilSampleCollectionActivity.this, "You need to grant permission", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }
}
