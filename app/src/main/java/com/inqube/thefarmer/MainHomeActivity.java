package com.inqube.thefarmer;

import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.inqube.thefarmer.Widget.HTMLTextView;
import com.inqube.thefarmer.base.BaseActivity;
import com.inqube.thefarmer.fragment.AgricultureProblemFragment;
import com.inqube.thefarmer.fragment.BankDetailsFragment;
import com.inqube.thefarmer.fragment.CattleDetailFragment;
import com.inqube.thefarmer.fragment.CreateAgriculturalProblemMyQuestionFragment;
import com.inqube.thefarmer.fragment.CreateNaturalDisasterMyQuestionFragment;
import com.inqube.thefarmer.fragment.HomeMainFragment;
import com.inqube.thefarmer.fragment.LandCropDetailFragment;
import com.inqube.thefarmer.fragment.LeftPanelFragment;
import com.inqube.thefarmer.fragment.MyFarmerFragment;
import com.inqube.thefarmer.fragment.MyFarmerListFragment;
import com.inqube.thefarmer.fragment.MyQuestionFragment;
import com.inqube.thefarmer.fragment.OthersQuestionFragment;
import com.inqube.thefarmer.fragment.PersonalDetailsFragment;
import com.inqube.thefarmer.fragment.QuestionFragment;
import com.inqube.thefarmer.model.MSG;

import Utils.Constant;
import Utils.UtilClass;
import retrofit2.Response;

/**
 * Created by Tubu on 11/11/2017.
 */

public class MainHomeActivity extends BaseActivity implements MyQuestionFragment.OnMyQuestionFragmentInteractionListener,
        OthersQuestionFragment.OnOthersQuestionFragmentInteractionListener,
        QuestionFragment.OnQuestionFragmenAddtInteractionListener,
        CreateNaturalDisasterMyQuestionFragment.OnCreateNaturalDisasterMyQuestionFragmentInteractionListener,
        CreateAgriculturalProblemMyQuestionFragment.OnCreateAgriculturalProblemMyQuestionFragmentInteractionListener,
        AgricultureProblemFragment.OnAgricultureProblemFragmentInteractionListener, LeftPanelFragment.OnLeftPanelFragmentInteractionListener,
        HomeMainFragment.OnHomeMainFragmentInteractionListener,
        PersonalDetailsFragment.OnPersonalDetailsFragmentInteractionListener,
        BankDetailsFragment.OnBankDetailsFragmentInteractionListener,
        LandCropDetailFragment.OnLandCropDetailFragmentInteractionListener,
        CattleDetailFragment.OnCattleDetailFragmentInteractionListener, MyFarmerFragment.OnMyFarmerFragmentInteractionListener
        , MyFarmerListFragment.OnFragmentInteractionListener {

    private Toolbar toolbar;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout drawerLayout;
    private FrameLayout fm_left_slide_menu, fm_container;
    private HTMLTextView toolbarTitle;
    private ImageView imv_home_logo;

    private int drawer_flag = 0;
    private boolean resume;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        onCreate(savedInstanceState, R.layout.activity_main_home);
        super.onCreate(savedInstanceState);
        if (isDeviceOnline()) {
            UtilClass.getInstance().getStaticDataFromServer(MainHomeActivity.this, this);
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbarTitle = (HTMLTextView) toolbar.findViewById(R.id.toolbar_title);
        imv_home_logo = (ImageView) toolbar.findViewById(R.id.imv_home_logo);
        imv_home_logo.setImageResource(R.drawable.logo_home_top);
        toolbarTitle.setText("");

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        col_holder = (CoordinatorLayout) findViewById(R.id.col_holder);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        setupDrawer();
        drawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    public void onBackPressed() {
        System.out.println(" BACK PRESSED " + getSupportFragmentManager().getBackStackEntryCount());
        if (getSupportFragmentManager().getBackStackEntryCount() > 2) {
            getSupportFragmentManager().popBackStackImmediate();
            getSupportFragmentManager().beginTransaction().commit();
        } else {
            System.out.println(" R NEI ");
            appCloseDialog(getString(R.string.close_app), this);
        }
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                mDrawerToggle.syncState();
            }

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                mDrawerToggle.syncState();
            }
        };

        mDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawer_flag == 0) {
                    drawer_flag = 1;
                    drawerLayout.openDrawer(Gravity.LEFT);
                } else {
                    drawer_flag = 0;
                    drawerLayout.closeDrawer(Gravity.RIGHT);
                }
            }
        });
        drawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void setUI() {
        super.setUI();
        fm_left_slide_menu = (FrameLayout) findViewById(R.id.fm_left_slide_menu);
        fm_container = (FrameLayout) findViewById(R.id.fm_container);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fm_left_slide_menu, new LeftPanelFragment(), "LeftPanelFragment")
                .addToBackStack("LeftPanelFragment")
                .commit();

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fm_container, new HomeMainFragment(), "HomeMainFragment")
                .addToBackStack("HomeMainFragment")
                .commit();
    }


    public boolean onPrepareOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.top_button_toll_free_call:

                return true;
        }
        return mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    /////////////////////FRAGMENT CALLBACKS//////////////////////////////////////////////
    ///////////////////// LEFT PANEL FRAGMENT///////////////////////////////////////////
    @Override
    public void onLeftPanelFragmentHomeButtonInteractionListener() {
        while (getSupportFragmentManager().getBackStackEntryCount() > 2) {
            getSupportFragmentManager().popBackStackImmediate();
        }
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fm_container, new HomeMainFragment(), "HomeMainFragment")
                .addToBackStack("HomeMainFragment")
                .commit();
    }

    @Override
    public void onLeftPanelFragmentChangePasswordButtonInteractionListener() {
        if (isDeviceOnline()) {
            UtilClass.getInstance().changePasswordDialog(MainHomeActivity.this, MainHomeActivity.this);
        } else {
            createSnackBar(col_holder, getString(R.string.please_make_sure_your_device_is_online));
        }
    }

    @Override
    public void onLeftPanelFragmentAboutButtonInteractionListener() {

    }

    @Override
    public void onLeftPanelFragmentLogoutButtonInteractionListener() {
        if (isDeviceOnline()) {
            UtilClass.getInstance().logoutFromServer(MainHomeActivity.this, MainHomeActivity.this, getUserPreference(Constant.SELECTED_LANGUAGE, BuildConfig.MARATHI));
        } else {
            createSnackBar(col_holder, getString(R.string.please_make_sure_your_device_is_online));
        }
    }

    @Override
    public void onLeftPanelFragmentInteractionListener(Uri uri) {

    }
    ///////////////////// LEFT PANEL FRAGMENT///////////////////////////////////////////

    //////////////////////////HOME FRAGMENT///////////////////////////////////////////
    @Override
    public void onHomeMainFragmentInteractionListener(Uri uri) {

    }

    @Override
    public void onHomeMainFragmentAgriProblemInteractionListener() {
        saveDataPreference(Constant.WHICH_PROBLEM, Constant.AGRI);
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fm_container, new QuestionFragment(), "QuestionFragment")
                .addToBackStack("QuestionFragment")
                .commit();
    }

    @Override
    public void onHomeMainFragmentNaturalDisasterInteractionListener() {
        saveDataPreference(Constant.WHICH_PROBLEM, Constant.NATURAL_DISASTER);
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fm_container, new QuestionFragment(), "QuestionFragment")
                .addToBackStack("QuestionFragment")
                .commit();
    }

    @Override
    public void onHomeMainFragmentDetailRegistrationInteractionListener() {
        if (isDeviceOnline()) {
            saveDataPreference(Constant.TYPE, "self_edit");

            if (getDataPreference(Constant.SEASON_LIST, "").length() > 3) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.fm_container, new MyFarmerFragment(), "MyFarmerFragment")
                        .addToBackStack("MyFarmerFragment")
                        .commit();
            } else {
                UtilClass.getInstance().getStaticDetailsRegistrationDataFromServer(MainHomeActivity.this, MainHomeActivity.this,
                        getUserPreference(Constant.TOKEN_ID, ""),
                        getUserPreference(Constant.SELECTED_LANGUAGE, BuildConfig.MARATHI));
            }
        } else {
            createSnackBar(col_holder, getString(R.string.please_make_sure_your_device_is_online));
        }
    }

    @Override
    public void onHomeMainFragmentGovtContactsInteractionListener() {

    }

    @Override
    public void onHomeMainFragmentGovtSchemeInteractionListener() {

    }

    @Override
    public void onHomeMainFragmentExpertOpinionInteractionListener() {

    }

    @Override
    public void onHomeMainFragmentAgriInstrumentInteractionListener() {

    }

    @Override
    public void onHomeMainFragmentTrackFarmerInteractionListener() {

    }
    //////////////////////////HOME FRAGMENT///////////////////////////////////////////

    @Override
    public void onAgricultureProblemFragmentInteractionListener(Uri uri) {

    }

    ///////////////////////////////////////////////////////////////////////////////////////
    ////////////CreateAgriculturalProblemMyQuestionFragment/////////
    @Override
    public void onCreateAgriculturalProblemMyQuestionFragmentNextInteractionListener() {

    }

    /////////////////////////////////////////////////////////////////////////////////////
    ////////////CreateNaturalDisasterMyQuestionFragment/////////
    @Override
    public void onCreateNaturalDisasterMyQuestionFragmentNextInteractionListener() {

    }

    /////////////////////////////////////////////////////////////////////////////////////
    ////////////QUESTION FRAGMENT/////////
    @Override
    public void onQuestionFragmenAddtInteractionListener(int position) {
        System.out.println(" POSITION " + position);
        if (getDataPreference(Constant.WHICH_PROBLEM, "").equalsIgnoreCase(Constant.AGRI)) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fm_container, new CreateAgriculturalProblemMyQuestionFragment(), "CreateAgriculturalProblemMyQuestionFragment")
                    .addToBackStack("CreateAgriculturalProblemMyQuestionFragment")
                    .commit();
        } else {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fm_container, new CreateNaturalDisasterMyQuestionFragment(), "CreateNaturalDisasterMyQuestionFragment")
                    .addToBackStack("CreateNaturalDisasterMyQuestionFragment")
                    .commit();
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////
    ////////////MyQuestionFragment FRAGMENT/////////
    @Override
    public void onMyQuestionFragmentInteraction(Uri uri) {

    }

    /////////////////////////////////////////////////////////////////////////////////////
    ////////////OthersQuestionFragment FRAGMENT/////////
    @Override
    public void onOthersQuestionFragmentInteraction(Uri uri) {

    }


    /////////////// My Farmer//////////////////////
    @Override
    public void onFramerFragmenAddtInteractionListener(int position) {
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fm_container, new PersonalDetailsFragment(), "PersonalDetailsFragment")
                .addToBackStack("PersonalDetailsFragment")
                .commit();
    }

    //////////////////////////////////////////////////
    ////////////PERSONAL DETAILS FRAGMENT/////////
    @Override
    public void onPersonalDetailsFragmentNextButtonClick() {
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fm_container, new BankDetailsFragment(), "BankDetailsFragment")
                .addToBackStack("BankDetailsFragment")
                .commit();
    }

    @Override
    public void onPersonalDetailsFragmentNextButtonClickForSelfEdit() {
        /*android.support.v4.app.Fragment page=getSupportFragmentManager().findFragmentByTag("MyFarmerFragment");
        ((MyFarmerFragment)page).changeBankDetails();*/
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fm_container, new BankDetailsFragment(), "BankDetailsFragment")
                .addToBackStack("BankDetailsFragment")
                .commit();
    }

    ////////////BANK DETAILS FRAGMENT/////////
    @Override
    public void onBankDetailsFragmentNextButtonClick() {
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fm_container, new LandCropDetailFragment(), "LandCropDetailFragment")
                .addToBackStack("LandCropDetailFragment")
                .commit();
    }

    ////////////LAND CROP DETAILS FRAGMENT/////////
    @Override
    public void onLandCropDetailFragmentNextButtonClick() {
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fm_container, new CattleDetailFragment(), "CattleDetailFragment")
                .addToBackStack("CattleDetailFragment")
                .commit();
    }

    ////////////CATTLE DETAILS FRAGMENT/////////
    @Override
    public void onCattleDetailFragmentNextButtonClick() {
        System.out.println("Image:" + getDataPreference("frm_img", ""));
        System.out.println("Inside onCattleDetailFragmentNextButtonClick::"
                + getDataPreference("frm_name", "") + ";" +
                getDataPreference("frm_primary_number", "") + "," +
                getDataPreference("frm_gender", "") + "," +
                getDataPreference("frm_secondary_number", "") + "," +
                getDataPreference("frm_pincode", "") + "," +
                getDataPreference("frm_father_name", "") + "," +
                getDataPreference("frm_category", "") + "," +
                getDataPreference("frm_farmer_type", "") + "," +
                getDataPreference("frm_farmer_land_type", "") + "," +
                getDataPreference("frm_id_proof_number", "") + "," +
                getDataPreference("frm_id_proof", "") + "," +
                getDataPreference("frm_kcc_card", "") + "," +
                getDataPreference("frm_kcc_card_number", "") + "," +
                getDataPreference("frm_bank_account", "") + "," +
                getDataPreference("frm_bank_name", "") + "," +
                getDataPreference("frm_bank_branch", "") + "," +
                getDataPreference("frm_bank_ifsc", "") + "," +
                getDataPreference("frm_state", "") + "," +
                getDataPreference("frm_district", "") + "," +
                getDataPreference("frm_subdivision", "") + "," +
                getDataPreference("frm_block", "") + "," +
                getDataPreference("frm_mouza", "") + "," +
                getDataPreference("frm_total_cultivabe_land", "") + "," +
                getDataPreference("frm_farm_machinery", "") + ";" +
                getDataPreference("frm_animal_number", "") + ";" +
                getDataPreference("frm_cultivate_fish", "") + ";" +
                getDataPreference("frm_cultivate_pond_number", "") + ";" +
                getUserPreference(Constant.SELECTED_LANGUAGE, BuildConfig.MARATHI) + ";" +
                getDataPreference("frm_season_rabi_list", "") + ";" +
                getDataPreference("frm_season_kharif_list", "") + ";" +
                getDataPreference("frm_season_pre_kharif_list", "") + ";" +
                getDataPreference("frm_animal_list", "") + ";" +
                getDataPreference("frm_farm_machinery_list", "") + ";" +
                getUserPreference(Constant.EMAIL, "") + "," +
                getUserPreference(Constant.TOKEN_ID, "") + "," +
                getDataPreference("frm_img", "") + "," +
                getDataPreference("frm_password", "") + "," +
                getDataPreference("frm_lang_id", "") + "," +
                getUserPreference(Constant.USER_ID, ""));
        if (getDataPreference(Constant.TYPE, "").equalsIgnoreCase("new")) {
            System.out.println("Inside add Farmer");
            UtilClass.getInstance().newDetailRegistration(MainHomeActivity.this,
                    MainHomeActivity.this,
                    getDataPreference("frm_name", ""),
                    getDataPreference("frm_primary_number", ""),
                    getDataPreference("frm_gender", ""),
                    getDataPreference("frm_secondary_number", ""),
                    getDataPreference("frm_pincode", ""),
                    getDataPreference("frm_father_name", ""),
                    getDataPreference("frm_category", ""),
                    getDataPreference("frm_farmer_type", ""),
                    getDataPreference("frm_farmer_land_type", ""),
                    getDataPreference("frm_id_proof_number", ""),
                    getDataPreference("frm_id_proof", ""),
                    getDataPreference("frm_kcc_card", ""),
                    getDataPreference("frm_kcc_card_number", ""),
                    getDataPreference("frm_bank_account", ""),
                    getDataPreference("frm_bank_name", ""),
                    getDataPreference("frm_bank_branch", ""),
                    getDataPreference("frm_bank_ifsc", ""),
                    getDataPreference("frm_state", ""),
                    getDataPreference("frm_district", ""),
                    getDataPreference("frm_subdivision", ""),
                    getDataPreference("frm_block", ""),
                    getDataPreference("frm_mouza", ""),
                    getDataPreference("frm_total_cultivabe_land", ""),
                    getDataPreference("frm_farm_machinery", ""),
                    getDataPreference("frm_animal_number", ""),
                    getDataPreference("frm_cultivate_fish", ""),
                    getDataPreference("frm_cultivate_pond_number", ""),
                    getUserPreference(Constant.SELECTED_LANGUAGE, BuildConfig.MARATHI),
                    getDataPreference("frm_season_rabi_list", ""),
                    getDataPreference("frm_season_kharif_list", ""),
                    getDataPreference("frm_season_pre_kharif_list", ""),
                    getDataPreference("frm_animal_list", ""),
                    getDataPreference("frm_farm_machinery_list", ""),
                    getUserPreference(Constant.EMAIL, ""),
                    getUserPreference(Constant.TOKEN_ID, ""),
                    getDataPreference("frm_img", ""),
                    getDataPreference("frm_password", ""),
                    getDataPreference("frm_lang_id", ""),
                    getUserPreference(Constant.USER_ID, ""));
        } else {
            String id = "";
            if (getDataPreference(Constant.TYPE, "").equalsIgnoreCase("farmer_edit")) {
                id = getDataPreference(Constant.FARMER_ID, "");
            } else {
                id = getUserPreference(Constant.USER_ID, "");
            }
            UtilClass.getInstance().detailRegistration(MainHomeActivity.this,
                    MainHomeActivity.this,
                    id,
                    getDataPreference("frm_name", ""),
                    getDataPreference("frm_primary_number", ""),
                    getDataPreference("frm_gender", ""),
                    getDataPreference("frm_secondary_number", ""),
                    getDataPreference("frm_pincode", ""),
                    getDataPreference("frm_father_name", ""),
                    getDataPreference("frm_category", ""),
                    getDataPreference("frm_farmer_type", ""),
                    getDataPreference("frm_farmer_land_type", ""),
                    getDataPreference("frm_id_proof_number", ""),
                    getDataPreference("frm_id_proof", ""),
                    getDataPreference("frm_kcc_card", ""),
                    getDataPreference("frm_kcc_card_number", ""),
                    getDataPreference("frm_bank_account", ""),
                    getDataPreference("frm_bank_name", ""),
                    getDataPreference("frm_bank_branch", ""),
                    getDataPreference("frm_bank_ifsc", ""),
                    getDataPreference("frm_state", ""),
                    getDataPreference("frm_district", ""),
                    getDataPreference("frm_subdivision", ""),
                    getDataPreference("frm_block", ""),
                    getDataPreference("frm_mouza", ""),
                    getDataPreference("frm_total_cultivabe_land", ""),
                    getDataPreference("frm_farm_machinery", ""),
                    getDataPreference("frm_animal_number", ""),
                    getDataPreference("frm_cultivate_fish", ""),
                    getDataPreference("frm_cultivate_pond_number", ""),
                    getUserPreference(Constant.SELECTED_LANGUAGE, BuildConfig.MARATHI),
                    getDataPreference("frm_season_rabi_list", ""),
                    getDataPreference("frm_season_kharif_list", ""),
                    getDataPreference("frm_season_pre_kharif_list", ""),
                    getDataPreference("frm_animal_list", ""),
                    getDataPreference("frm_farm_machinery_list", ""),
                    getUserPreference(Constant.EMAIL, ""),
                    getDataPreference("frm_img", ""),
                    getDataPreference("frm_lang_id", ""),
                    getUserPreference(Constant.TOKEN_ID, ""));
        }

    }
    /////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////// RETROFIT RESPONSE ///////////////////////////

    @Override
    public void onSuccess(Response<MSG> response, String which_method) {
        super.onSuccess(response, which_method);
        if (which_method.equalsIgnoreCase("getStaticDataFromServer")) {
            Gson gson = new Gson();
            String json = gson.toJson(response.body().getData().getPlaceList());
            String crop_json = gson.toJson(response.body().getData());
            saveDataPreference(Constant.PLACE_LIST, json);
            saveDataPreference(Constant.CROP_LIST, crop_json);


        } else if (which_method.equalsIgnoreCase("logoutFromServer")) {

           /* if(selectedLanguage==Constant.ENGLISH){
                startActivity(new Intent(this,LoginActivity.class));
                finish();
            }else{
                deleteSpecificValueFromData(Constant.PLACE_LIST);
                Intent it = new Intent(ShortRegistrationActivity.this, SplashActivity.class);
                it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(it);
                saveDataPreference(Constant.REGISTRATION_STATUS,"yes");
                finish();
            }*/

            deleteDataPreference();
            deleteUserPreference();
            /*deleteSpecificValueFromData(Constant.PLACE_LIST);
            deleteSpecificValueFromData(Constant.SEASON_LIST);*/
            Intent it = new Intent(MainHomeActivity.this, SplashActivity.class);
            it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(it);
            finish();
        } else if (which_method.equalsIgnoreCase("changPassword")) {
            startActivity(new Intent(this, LoginActivity.class));
            createSnackBar(col_holder, response.body().getMessage());
        } else if (which_method.equalsIgnoreCase("getStaticDetailsRegistrationDataFromServer")) {
            Gson gson = new Gson();

            String json = gson.toJson(response.body().getKsAnimalsList());
            saveDataPreference(Constant.ANIMAL_LIST, json);

            json = gson.toJson(response.body().getKsFarmerTypesList());
            saveDataPreference(Constant.FARMER_TYPE_LIST, json);

            json = gson.toJson(response.body().getKsFarmMachinerysList());
            saveDataPreference(Constant.FARM_MACHINARY_LIST, json);

            json = gson.toJson(response.body().getKsSeasonsList());
            saveDataPreference(Constant.SEASON_LIST, json);

            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fm_container, new MyFarmerFragment(), "MyFarmerFragment")
                    .addToBackStack("MyFarmerFragment")
                    .commit();
        } else if (which_method.equalsIgnoreCase("detailRegistration")) {
            createSnackBar(col_holder, response.body().getMessage());
            System.out.println("For:" + getDataPreference(Constant.TYPE, ""));

            if (getDataPreference(Constant.TYPE, "").equalsIgnoreCase("farmer_edit")) {
                System.out.println("For1:" + getDataPreference(Constant.TYPE, ""));
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.fm_container, new HomeMainFragment(), "HomeMainFragment")
                        .addToBackStack("HomeMainFragment")
                        .commit();
            } else if (getDataPreference(Constant.TYPE, "").equalsIgnoreCase("self_edit")) {
                System.out.println("For2:" + getDataPreference(Constant.TYPE, ""));
                //saveUserPreference(Constant.USER_IMAGE,response.body().getUserDetails().get(0).getProfileImageSource().toString());
                startActivity(new Intent(this, SplashActivity.class));
            }

        } else if (which_method.equalsIgnoreCase("TokenHasExpired")) {
            Intent it = new Intent(MainHomeActivity.this, LoginActivity.class);
            it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(it);
            // Toast.makeText(this, response.errorBody().toString(), Toast.LENGTH_SHORT).show();
        } else if (which_method.equalsIgnoreCase("newdetailRegistration")) {
            createSnackBar(col_holder, response.body().getMessage());
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fm_container, new HomeMainFragment(), "HomeMainFragment")
                    .addToBackStack("HomeMainFragment")
                    .commit();
        }
    }

    @Override
    public void onFailure(Response<MSG> response) {
        super.onFailure(response);
        createSnackBar(col_holder, response.body().getMessage());
    }

    @Override
    public void onResponseFailure() {
        super.onResponseFailure();
        createSnackBar(col_holder, getString(R.string.sorry_server_error));
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}