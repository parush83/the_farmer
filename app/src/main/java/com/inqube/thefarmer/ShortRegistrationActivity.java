package com.inqube.thefarmer;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.inqube.thefarmer.Widget.HTMLTextView;
import com.inqube.thefarmer.Widget.MyEditText;
import com.inqube.thefarmer.base.BaseActivity;
import com.inqube.thefarmer.base.BaseActivityWithoutMenu;
import com.inqube.thefarmer.fragment.CreateAgriculturalProblemMyQuestionFragment;
import com.inqube.thefarmer.model.DropDownModelClass;
import com.inqube.thefarmer.model.MSG;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import Utils.AllInterfaces;
import Utils.Constant;
import Utils.MarshMallowPermission;
import Utils.UtilClass;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Field;

/**
 * Created by Tubu on 11/11/2017.
 */

public class ShortRegistrationActivity extends BaseActivity implements View.OnClickListener, AllInterfaces.SendOTP {

    private static boolean settingOn;
    ArrayList<DropDownModelClass> al_user_type;
    /////////////////// UI VARIABLES ///////////////////
    private HTMLTextView htv_type, htv_district, htv_sub_division, htv_block, htv_mouza, htv_State;
    private MyEditText edt_name_of_farmer, edt_mobile_number, edt_password, edt_c_passord;
    private ImageView imv_user_profile_pic, imv_camera;
    private FloatingActionButton fab_submit;
    private RadioGroup rg_language;
    private RadioButton rb_english, rb_hindi, rb_marathi;
    ////////////////////////////////////////////////////
    ////////////// NORMAL VARIABLES/////////////////////
    private Bitmap bitmap;
    private File destination = null;
    private InputStream inputStreamImg;
    private String imgPath = null, bitmapString = "";
    private int selectedLanguage = 0;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private String userChoosenTask;
    private ArrayList<DropDownModelClass> al_district;
    private ArrayList<DropDownModelClass> al_sub;
    private ArrayList<DropDownModelClass> al_block;
    private int district, sub, block, mouza, dist_pos, sub_pos, type;
    private MyCommonAdapter ad;
    private MarshMallowPermission marshMallowPermission;
    private String sendOTP;

    /////////////////////////////////////////////////////
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        onCreate(savedInstanceState, R.layout.short_registration_activity);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        if (!resume) {
            if (isDeviceOnline()) {
                UtilClass.getInstance().getStaticDataFromServer(ShortRegistrationActivity.this, ShortRegistrationActivity.this);
            } else {
                //createSnackBar(col_holder,getString(R.string.please_make_sure_your_device_is_online));
                //Toast.makeText(this, getString(R.string.please_make_sure_your_device_is_online), Toast.LENGTH_SHORT).show();
            }

            setUI();
        }
        resume = true;
        super.onResume();
        if (settingOn) {
            settingOn = false;

        }
    }

    @Override
    protected void setUI() {
        super.setUI();
        setToolbarTitle(R.string.initial_registration);

        col_holder = (CoordinatorLayout) findViewById(R.id.col_holder);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        imv_user_profile_pic = (ImageView) findViewById(R.id.imv_user_profile_pic);

        htv_State = (HTMLTextView) findViewById(R.id.htv_State);

        htv_type = (HTMLTextView) findViewById(R.id.htv_type);
        htv_type.setOnClickListener(this);

        htv_district = (HTMLTextView) findViewById(R.id.htv_district);
        htv_district.setOnClickListener(this);

        htv_sub_division = (HTMLTextView) findViewById(R.id.htv_sub_division);
        htv_sub_division.setOnClickListener(this);

        htv_block = (HTMLTextView) findViewById(R.id.htv_block);
        htv_block.setOnClickListener(this);

        htv_mouza = (HTMLTextView) findViewById(R.id.htv_mouza);
        htv_mouza.setOnClickListener(this);

        edt_name_of_farmer = (MyEditText) findViewById(R.id.edt_name_of_farmer);
        edt_mobile_number = (MyEditText) findViewById(R.id.edt_mobile_number);
        edt_password = (MyEditText) findViewById(R.id.edt_password);
        edt_c_passord = (MyEditText) findViewById(R.id.edt_c_passord);

        imv_camera = (ImageView) findViewById(R.id.imv_camera);
        imv_camera.setOnClickListener(this);

        fab_submit = (FloatingActionButton) findViewById(R.id.fab_submit);
        fab_submit.setOnClickListener(this);

        rb_english = (RadioButton) findViewById(R.id.rb_english);
        rb_hindi = (RadioButton) findViewById(R.id.rb_hindi);
        rb_marathi = (RadioButton) findViewById(R.id.rb_marathi);

        rg_language = (RadioGroup) findViewById(R.id.rg_language);
        rg_language.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (radioGroup.getCheckedRadioButtonId() == R.id.rb_english) {
                    selectedLanguage = Integer.parseInt(BuildConfig.ENHLISH);
//                    Intent intent=new Intent(android.provider.Settings.ACTION_LOCALE_SETTINGS);
//                    startActivity(intent);
                } else if (radioGroup.getCheckedRadioButtonId() == R.id.rb_hindi) {
                    selectedLanguage = Integer.parseInt(BuildConfig.ENHLISH);
//                    Intent intent=new Intent(android.provider.Settings.ACTION_LOCALE_SETTINGS);
//                    startActivity(intent);
                } else if (radioGroup.getCheckedRadioButtonId() == R.id.rb_marathi) {
                    selectedLanguage = Integer.parseInt(BuildConfig.MARATHI);;
                }
                saveUserPreference(Constant.SELECTED_LANGUAGE, "" + selectedLanguage);
            }
        });
       /* al_user_type=UtilClass.getInstance().getUserTypeList(ShortRegistrationActivity.this);
        al_district = UtilClass.getInstance().getDistrictList(ShortRegistrationActivity.this);*/
    }

    @Override
    public void onClick(View view) {
        hideKeyboard(this);
        switch (view.getId()) {
            case R.id.htv_block:
                if (!htv_sub_division.getText().toString().equalsIgnoreCase(getResources().getString(R.string.tap_here))) {
                    if (isDeviceOnline()) {
                        al_block = UtilClass.getInstance().getBolckList(ShortRegistrationActivity.this, sub);
                        dropDown(al_block, getApplicationContext(), 3);
                    } else {
                        //createSnackBar(col_holder, getString(R.string.please_make_sure_your_device_is_online));
                    }
                }
                break;
            case R.id.htv_district:
                if (isDeviceOnline()) {
                    //al_block = UtilClass.getInstance().getBolckList(ShortRegistrationActivity.this, sub);
                    dropDown(al_district, getApplicationContext(), 1);
                } else {
                    // createSnackBar(col_holder, getString(R.string.please_make_sure_your_device_is_online));
                }
                break;
            case R.id.htv_type:
                if (isDeviceOnline()) {
                    //al_block = UtilClass.getInstance().getBolckList(ShortRegistrationActivity.this, sub);
                    dropDown(al_user_type, getApplicationContext(), 5);
                } else {
                    //createSnackBar(col_holder, getString(R.string.please_make_sure_your_device_is_online));
                }

                break;
            case R.id.htv_mouza:
                System.out.println("Block::" + block);
                if (isDeviceOnline()) {
                    UtilClass.getInstance().getMouzaByBlock(ShortRegistrationActivity.this, ShortRegistrationActivity.this, "" + block);
                } else {
                    // createSnackBar(col_holder, getString(R.string.please_make_sure_your_device_is_online));
                }

                break;
            case R.id.htv_sub_division:
                if (!htv_district.getText().toString().equalsIgnoreCase(getResources().getString(R.string.tap_here))) {
                    if (isDeviceOnline()) {
                        // System.out.println(" DIST ID " + district);
                        al_sub = UtilClass.getInstance().getSubDivisionList(ShortRegistrationActivity.this, district);
                        dropDown(al_sub, getApplicationContext(), 2);
                    } else {
                        // createSnackBar(col_holder, getString(R.string.please_make_sure_your_device_is_online));
                    }
                }
                break;
            case R.id.imv_camera:
                selectImage();
                break;
            case R.id.fab_submit:
                if (checkRegistrationValidity() == 0) {
                    if (isDeviceOnline()) {
                        UtilClass.getInstance().sendOTP(ShortRegistrationActivity.this, ShortRegistrationActivity.this, edt_mobile_number.getText().toString().trim());
                    } else {
                        createSnackBar(col_holder, getString(R.string.please_make_sure_your_device_is_online));
                    }
                }
                break;
        }
    }

    private int checkRegistrationValidity() {
        if (htv_type.getText().toString().trim().equalsIgnoreCase(getResources().getString(R.string.tap_here))) {
            createSnackBar(htv_type, getString(R.string.select_type));
            return 1;
        }
        if (TextUtils.isEmpty(edt_name_of_farmer.getText().toString())) {
            createSnackBar(edt_name_of_farmer, getString(R.string.please_enter_farmer_name));
            return 1;
        }
        if (TextUtils.isEmpty(edt_mobile_number.getText().toString())) {
            createSnackBar(edt_mobile_number, getString(R.string.please_enter_ten_digits_mo_number));
            return 1;
        }
        if (edt_mobile_number.getText().toString().trim().length() < 10) {
            createSnackBar(edt_mobile_number, getString(R.string.please_enter_ten_digits_password)
            );
            return 1;
        }
        if (htv_district.getText().toString().trim().equalsIgnoreCase(getResources().getString(R.string.tap_here))) {
            createSnackBar(htv_district, getString(R.string.please_select_district));
            return 1;
        }
        if (htv_sub_division.getText().toString().trim().equalsIgnoreCase(getResources().getString(R.string.tap_here))) {
            createSnackBar(htv_district, getString(R.string.please_select_sub_division));
            return 1;
        }
        if (htv_block.getText().toString().trim().equalsIgnoreCase(getResources().getString(R.string.tap_here))) {
            createSnackBar(htv_district, getString(R.string.please_select_block));
            return 1;
        }
        if (htv_mouza.getText().toString().trim().equalsIgnoreCase(getResources().getString(R.string.tap_here))) {
            createSnackBar(htv_district, getString(R.string.please_select_mouza));
            return 1;
        }
        if (TextUtils.isEmpty(edt_password.getText().toString())) {
            createSnackBar(edt_password, getString(R.string.please_enter_password));
            return 1;
        }
        if (TextUtils.isEmpty(edt_c_passord.getText().toString())) {
            createSnackBar(edt_c_passord, getString(R.string.please_enter_confirm_password));
            return 1;
        }
        if (!edt_password.getText().toString().trim().equalsIgnoreCase(edt_c_passord.getText().toString().trim())) {
            createSnackBar(edt_c_passord, getString(R.string.p_c_p_not_matching));
            return 1;
        }
        if (selectedLanguage == 0) {
            createSnackBar(rg_language, getString(R.string.please_select_a_language));
            return 1;
        }
        return 0;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case UtilClass.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(ShortRegistrationActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = UtilClass.getInstance().checkPermission(ShortRegistrationActivity.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail = UtilClass.getInstance().scaleBitmap(thumbnail, 400, 400);
        //thumbnail.compress(Bitmap.CompressFormat.JPEG, 50, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        imv_user_profile_pic.setImageBitmap(thumbnail);
        if (thumbnail != null) {
            bitmapString = getStringImage(thumbnail);
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                bm = UtilClass.getInstance().scaleBitmap(bm, 400, 400);
                //bm.compress()
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        imv_user_profile_pic.setImageBitmap(bm);
        if (bm != null) {
            bitmapString = getStringImage(bm);
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public String getStringImage(Bitmap bmp) {
        String encodedImage = "";
        if (bmp != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.JPEG, 50, baos);
            byte[] imageBytes = baos.toByteArray();
            encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        }

        return encodedImage;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (settingOn) {
            settingOn = true;
        }
    }

    @Override
    public void onSuccess(Response<MSG> response, String which_method) {
        System.out.println("Inside OnSccess");
        if (which_method.equalsIgnoreCase("getStaticDataFromServer")) {
            Gson gson = new Gson();
            String json = gson.toJson(response.body().getData().getPlaceList());
            String crop_json = gson.toJson(response.body().getData());
            saveDataPreference(Constant.PLACE_LIST, json);
            saveDataPreference(Constant.CROP_LIST, crop_json);
            UtilClass.getInstance().parseUserListJson(ShortRegistrationActivity.this);
            UtilClass.getInstance().parseDistrictListJson(ShortRegistrationActivity.this);
            al_user_type = UtilClass.getInstance().getUserTypeList(ShortRegistrationActivity.this);
            al_district = UtilClass.getInstance().getDistrictList(ShortRegistrationActivity.this);

        } else if (which_method.equalsIgnoreCase("getMouzaByBlock")) {
            System.out.println(" MOUZA SIZE " + response.body().getGetMouzaList().size() + "  " + block);
            ArrayList<DropDownModelClass> temp = UtilClass.getInstance().geMouza(ShortRegistrationActivity.this, response.body().getGetMouzaList(), block);
            dropDown(temp, ShortRegistrationActivity.this, 4);
        } else if (which_method.equalsIgnoreCase("shortRegistrationToServer")) {
            if (selectedLanguage == Integer.parseInt(BuildConfig.ENHLISH)) {
                startActivity(new Intent(this, LoginActivity.class));
                finish();
            } else {
                deleteSpecificValueFromData(Constant.PLACE_LIST);
                Intent it = new Intent(ShortRegistrationActivity.this, SplashActivity.class);
                it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(it);
                saveDataPreference(Constant.REGISTRATION_STATUS, "yes");
                finish();
            }
        } else if (which_method.equalsIgnoreCase("SendOTP")) {
            createSnackBar(col_holder, response.body().getMessage());
            sendOTP = response.body().getOtp_data();
            System.out.println(" OTP " + sendOTP);
            UtilClass.getInstance().sendOTPDialog(ShortRegistrationActivity.this,
                    ShortRegistrationActivity.this, sendOTP);
        }
    }

    @Override
    public void onFailure(Response<MSG> response) {
        System.out.println("Inside onFailure");
        createSnackBar(col_holder, response.body().getMessage());
    }

    @Override
    public void onResponseFailure() {
        System.out.println("Inside onResponseFailure");
        createSnackBar(col_holder, getString(R.string.sorry_server_error));
    }

    @Override
    public void sendOTPResponse(String phoneNumber) {
        System.out.println("BASE 64 " + bitmapString);
        //bitmapString = getStringImage(bitmap);

        /*try{
            (new Thread(new Runnable() {
                @Override
                public void run() {
                    //bitmapString=UtilClass.getInstance().convertBase64(bitmap);
                    bitmapString=getStringImage(bitmap);
                }
            })).start();
        }catch(Exception e){
            createSnackBar(col_holder,getString(R.string.image_false));
        }*/
        System.out.println("String::" + bitmapString);
        UtilClass.getInstance().shortRegistrationToServer(ShortRegistrationActivity.this,
                ShortRegistrationActivity.this, String.valueOf(type),
                edt_name_of_farmer.getText().toString().trim(),
                "", edt_password.getText().toString().trim(),
                BuildConfig.STATE_ID,
                "" + district,
                "" + sub,
                "" + block,
                "" + mouza,
                edt_mobile_number.getText().toString().trim(),
                "" + selectedLanguage,
                bitmapString);

    }

    //////////////////////////  DROP DOWN ///////////////////////////////////////////

    public void dropDown(final ArrayList<DropDownModelClass> temp, Context context, final int flag) {

        final Dialog dialog = new Dialog(ShortRegistrationActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.drop_down_cell);
        dialog.show();
        ListView lv_cell = (ListView) dialog.findViewById(R.id.lv_cell);
        lv_cell.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if (flag == 1) {
                    htv_district.setText("" + al_district.get(position).getName());
                    district = al_district.get(position).getId();
                    sub = -1;
                    block = -1;
                    htv_sub_division.setText(getResources().getString(R.string.tap_here));
                    htv_block.setText(getResources().getString(R.string.tap_here));
                    htv_mouza.setText(getResources().getString(R.string.tap_here));
                    dist_pos = position;
                } else if (flag == 2) {
                    htv_sub_division.setText("" + al_sub.get(position).getName());
                    sub = al_sub.get(position).getId();
                    block = -1;
                    htv_block.setText(getResources().getString(R.string.tap_here));
                    htv_mouza.setText(getResources().getString(R.string.tap_here));
                    sub_pos = position;
                } else if (flag == 3) {
                    htv_block.setText("" + al_block.get(position).getName());
                    block = al_block.get(position).getId();
                    htv_mouza.setText(getResources().getString(R.string.tap_here));
                } else if (flag == 4) {
                    htv_mouza.setText("" + temp.get(position).getName());
                    mouza = temp.get(position).getId();
                } else if (flag == 5) {
                    htv_type.setText("" + al_user_type.get(position).getName());
                    type = al_user_type.get(position).getId();
                }
                dialog.dismiss();
            }
        });
        implementDropDown(temp, context, lv_cell);
    }

    private void implementDropDown(ArrayList<DropDownModelClass> al_rd, Context context, ListView lv) {
        ad = new MyCommonAdapter(context, R.layout.dropdown_list_cell, al_rd);
        lv.setAdapter(ad);
        ad.notifyDataSetChanged();
        lv.setTextFilterEnabled(true);
    }

    public class MyCommonAdapter extends ArrayAdapter<DropDownModelClass> {
        ArrayList<DropDownModelClass> my_al;

        public MyCommonAdapter(Context context, int row, ArrayList<DropDownModelClass> al) {
            super(context, row, al);
            my_al = al;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.dropdown_list_cell, null);
            }

            TextView title = (TextView) convertView.findViewById(R.id.title);
            title.setText("" + my_al.get(position).getName());

            return convertView;
        }
    }
}


