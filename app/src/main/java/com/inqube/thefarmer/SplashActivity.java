package com.inqube.thefarmer;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;

import com.google.gson.Gson;

import com.inqube.thefarmer.base.BaseActivityWithoutMenu;
import com.inqube.thefarmer.model.MSG;

import java.net.URI;

import Utils.AllInterfaces;
import Utils.Constant;
import Utils.UtilClass;
import retrofit2.Response;

public class SplashActivity extends BaseActivityWithoutMenu implements AllInterfaces.KeyboardDialogCallback {

    private static int SPLASH_TIME_OUT = 3000;
    boolean isAppInstalled;

    public static boolean isPackageExisted(Context c, String targetPackage) {

        PackageManager pm = c.getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo(targetPackage,
                    PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        onCreate(savedInstanceState, R.layout.activity_splash);
        super.onCreate(savedInstanceState);
    }

   /* @Override
    public void onSuccess(Response<MSG> response,String which_method) {
        Gson gson = new Gson();
        String json = gson.toJson(response.body().getData().getPlaceList());
        String crop_json=gson.toJson(response.body().getData());
        saveDataPreference(Constant.PLACE_LIST, json);
        saveDataPreference(Constant.CROP_LIST,crop_json);

        if (getUserPreference(Constant.REMEMBER_ME, "no").equalsIgnoreCase("yes")) {
            Intent i = new Intent(SplashActivity.this, MainHomeActivity.class);
            startActivity(i);
        } else {
            Intent i = new Intent(SplashActivity.this, LoginActivity.class);
            startActivity(i);
        }
        finish();
    }*/

    @Override
    protected void setUI() {
        super.setUI();

        col_holder = (CoordinatorLayout) findViewById(R.id.col_holder);
        /*if(getDataPreference(Constant.REGISTRATION_STATUS,"no").equalsIgnoreCase("yes")){
            if(getUserPreference(Constant.SELECTED_LANGUAGE,"").equalsIgnoreCase("2")){
                isAppInstalled = isPackageExisted(SplashActivity.this,Constant.HINDI_KEYBOARD);
            }else if(getUserPreference(Constant.SELECTED_LANGUAGE,"").equalsIgnoreCase("3")){
                isAppInstalled = isPackageExisted(SplashActivity.this,Constant.MARATHI_KEYBOARD);
            }
            if(isAppInstalled){
                isDataAvailable();
            }else{
                UtilClass.getInstance().downloadKeyboard(SplashActivity.this,SplashActivity.this);
            }

        }else {
            isDataAvailable();
        }*/
        isAppInstalled = isPackageExisted(SplashActivity.this, Constant.KEYBOARD);
        if (!isAppInstalled) {
            UtilClass.getInstance().downloadKeyboard(SplashActivity.this, SplashActivity.this);
        } else {
            isDataAvailable();
        }
    }

    @Override
    public void onFailure(Response<MSG> response) {
        createSnackBar(col_holder, response.body().getMessage());
    }

    @Override
    public void onResponseFailure() {
        createSnackBar(col_holder, getString(R.string.sorry_server_error));
    }

    @Override
    public void onOkClick() {
        /*if(getUserPreference(Constant.SELECTED_LANGUAGE,"").equalsIgnoreCase("2")) {
            Intent marketIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constant.HINDI_KEYBOARD1));
            marketIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET | Intent.FLAG_ACTIVITY_MULTIPLE_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(marketIntent);
        }else if(getUserPreference(Constant.SELECTED_LANGUAGE,"").equalsIgnoreCase("3")){
            Intent marketIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constant.MARATHI_KEYBOARD1));
            marketIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET | Intent.FLAG_ACTIVITY_MULTIPLE_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(marketIntent);
        }*/
        Intent marketIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constant.KEYBOARD1));
        marketIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET | Intent.FLAG_ACTIVITY_MULTIPLE_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(marketIntent);
        System.out.println("Not Installed");
        resume = false;
    }

    @Override
    public void onCancleClick() {
        //isDataAvailable();
        startActivity(new Intent(this, LoginActivity.class));
    }

    public void isDataAvailable() {
        //if (getDataPreference(Constant.PLACE_LIST, "").length() > 10) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(i);
                   /* if(getUserPreference(Constant.REMEMBER_ME,"no").equalsIgnoreCase("yes")){
                        Intent i = new Intent(SplashActivity.this, MainHomeActivity.class);
                        startActivity(i);
                    }else{
                        Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                        startActivity(i);
                    }*/
                finish();
            }
        }, SPLASH_TIME_OUT);
        //} else {
            /*if(isDeviceOnline()){
                UtilClass.getInstance().getStaticDataFromServer(SplashActivity.this, SplashActivity.this);
            }
            else{
                createSnackBar(col_holder,getString(R.string.please_make_sure_your_device_is_online));
            }*/
        //}
    }
}