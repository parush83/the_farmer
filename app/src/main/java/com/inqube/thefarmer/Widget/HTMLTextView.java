package com.inqube.thefarmer.Widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.Html;
import android.util.AttributeSet;
import android.util.Log;

import static android.content.ContentValues.TAG;

/**
 * Created by root on 7/13/17.
 */

public class HTMLTextView extends android.support.v7.widget.AppCompatTextView {
    public static Typeface FONT_NAME;

    public HTMLTextView(Context context) {
        super(context);
        if (FONT_NAME == null)
            FONT_NAME = Typeface.createFromAsset(context.getAssets(), "AvenirLTStd-Book.otf");
        this.setTypeface(FONT_NAME);
    }

    public HTMLTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (FONT_NAME == null)
            FONT_NAME = Typeface.createFromAsset(context.getAssets(), "AvenirLTStd-Book.otf");
        this.setTypeface(FONT_NAME);
    }

    public HTMLTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (FONT_NAME == null)
            FONT_NAME = Typeface.createFromAsset(context.getAssets(), "AvenirLTStd-Book.otf");
        this.setTypeface(FONT_NAME);
    }

    private void init() {
        setText(Html.fromHtml(getText().toString()), BufferType.SPANNABLE);
    }
}