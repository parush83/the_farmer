package com.inqube.thefarmer.Widget;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.AttributeSet;

/**
 * Created by root on 12/7/17.
 */

public class MyEditText extends android.support.v7.widget.AppCompatEditText {
    public static Typeface FONT_NAME;

    public MyEditText(Context context) {
        super(context);
        if (FONT_NAME == null)
            FONT_NAME = Typeface.createFromAsset(context.getAssets(), "AvenirLTStd-Book.otf");
        this.setTypeface(FONT_NAME);
    }

    public MyEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (FONT_NAME == null)
            FONT_NAME = Typeface.createFromAsset(context.getAssets(), "AvenirLTStd-Book.otf");
        this.setTypeface(FONT_NAME);
    }

    public MyEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (FONT_NAME == null)
            FONT_NAME = Typeface.createFromAsset(context.getAssets(), "AvenirLTStd-Book.otf");
        this.setTypeface(FONT_NAME);
    }

    private void init() {
        setText(Html.fromHtml(getText().toString()), BufferType.SPANNABLE);
    }

    public void clearText() {
        getText().clear();
        //setText("");
    }

   /* @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        String str=charSequence.toString();

        str=str.replaceAll("०","0")
                .replaceAll("१","1")
                .replaceAll("२","2")
                .replaceAll("३","3")
                .replaceAll("४","4")
                .replaceAll("५","5")
                .replaceAll("६","6")
                .replaceAll("७","7")
                .replaceAll("८","8")
                .replaceAll("९","9");

       // setText(str);

        System.out.println(" TEXT  "+charSequence.hashCode()+"  "+str);

    }

    @Override
    public void afterTextChanged(Editable editable) {


    }*/
}
