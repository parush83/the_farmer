package com.inqube.thefarmer.adapter;

import android.app.Activity;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.inqube.thefarmer.R;
import com.inqube.thefarmer.model.GetList;
import com.inqube.thefarmer.model.GetuserList;

import java.util.List;

import Utils.AllInterfaces;

/**
 * Created by inqube on 28/12/17.
 */

@SuppressWarnings("ALL")
public class FarmerListAdapter extends RecyclerView.Adapter<FarmerListAdapter.MyViewHolder> {
    AllInterfaces.CustomItemClickListener listener;
    private List<GetuserList> userList;
    private Activity ctx;
    private CoordinatorLayout overview_coordinator_layout;

    public FarmerListAdapter(RecyclerView mRecyclerView, Activity ctx, List<GetuserList> userList, CoordinatorLayout overview_coordinator_layout, AllInterfaces.CustomItemClickListener listener) {
        this.userList = userList;
        this.ctx = ctx;
        this.overview_coordinator_layout = overview_coordinator_layout;
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.my_others_questions_listcell_left, parent, false);
        final MyViewHolder mViewHolder = new MyViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(view, mViewHolder.getPosition(), userList.get(mViewHolder.getPosition()).getName());
            }
        });
        return mViewHolder;

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        MyViewHolder customholder = (MyViewHolder) holder;
        customholder.tv_title.setText(userList.get(position).getName());
        //customholder.tv_date.setText(questionList.get(position).getCreatedAt());
        customholder.tv_date.setText(userList.get(position).getFrmPrimaryNumber());
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_title, tv_name, tv_date;


        public MyViewHolder(View view) {
            super(view);
            tv_name = (TextView) view.findViewById(R.id.tv_name);
            tv_date = (TextView) view.findViewById(R.id.tv_date);
            tv_title = (TextView) view.findViewById(R.id.tv_title);

        }


    }
}
