package com.inqube.thefarmer.base;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.inqube.thefarmer.BuildConfig;
import com.inqube.thefarmer.R;
import com.inqube.thefarmer.SplashActivity;
import com.inqube.thefarmer.Widget.HTMLTextView;
import com.inqube.thefarmer.model.MSG;
import com.inqube.thefarmer.service.GPSTracker;
import com.squareup.picasso.Picasso;

import java.net.URI;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import Utils.AllInterfaces;
import Utils.Constant;
import Utils.UtilClass;
import retrofit2.Response;

/**
 * Created by Tubu on 11/16/2017.
 */

public class BaseActivity extends AppCompatActivity implements AllInterfaces.RetrofitResponseToActivityOrFrament, AllInterfaces.IfGPSNotAvailable {

    //GIT URL https://Suman_Ingreens@bitbucket.org/Suman_Ingreens/soil-ingreens.git
    /////////////////NORMAL PARAMS //////////////////////////
    protected boolean resume;
    protected Toolbar toolbar;
    protected HTMLTextView toolbarTitle;
    protected ImageView imv_home_logo;
    ///////////////////////////////////////////////////////////
    ////////////////UI VARIABLES//////////////////////////////
    protected CoordinatorLayout col_holder;

    ////////////////////////////////////////////////////////////
    protected void onCreate(@Nullable Bundle savedInstanceState, int layout) {
        //Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(SoilBaseActivity.this));
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(layout);

        if (getUserPreference(Constant.SELECTED_LANGUAGE, BuildConfig.MARATHI).equalsIgnoreCase(BuildConfig.ENHLISH)) {
            UtilClass.getInstance().setLocale(BaseActivity.this, "en");
        } else if (getUserPreference(Constant.SELECTED_LANGUAGE, BuildConfig.MARATHI).equalsIgnoreCase( BuildConfig.HINDI)) {
            UtilClass.getInstance().setLocale(BaseActivity.this, "hi");
        } else if (getUserPreference(Constant.SELECTED_LANGUAGE, BuildConfig.MARATHI).equalsIgnoreCase(BuildConfig.MARATHI)) {
            UtilClass.getInstance().setLocale(BaseActivity.this, "mar");
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbarTitle = (HTMLTextView) toolbar.findViewById(R.id.toolbar_title);
        imv_home_logo = (ImageView) toolbar.findViewById(R.id.imv_home_logo);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    public void setToolbarTitle(int res) {
        toolbarTitle.setText(res);
    }

    public void setHomeToolbarTitle() {
        toolbarTitle.setText("");
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.top_button_toll_free_call:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void ifGPSNotAvailable() {
        GPSTracker gps = new GPSTracker(BaseActivity.this, BaseActivity.this);
        gps.showSettingsAlert();
    }


    protected void setUI() {
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!resume) {
            setUI();
        }
        resume = true;
    }

    @Override
    public void onBackPressed() {
        finish();

    }

    public void createSnackBar(View v, String str) {
        Snackbar snackbar = Snackbar
                .make(v, str, Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.RED);
        snackbar.setDuration(Snackbar.LENGTH_SHORT);
        View view = snackbar.getView();
        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        snackbar.show();
    }

    public void appCloseDialog(String msg, final BaseActivity ctx) {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                ctx);
        alertDialogBuilder.setTitle(getResources().getString(R.string.app_name_full));
        alertDialogBuilder
                .setMessage("" + msg)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent startMain = new Intent(Intent.ACTION_MAIN);
                        startMain.addCategory(Intent.CATEGORY_HOME);
                        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        ctx.startActivity(startMain);
                        dialog.dismiss();
                        ctx.finish();
                    }
                })
                .setNegativeButton(R.string.cancle, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    protected void hideKeyboard(BaseActivity activity) {
        try {
            InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            Log.e("KeyBoardUtil", e.toString(), e);
        }
    }

    protected void getImage(Context ctx, int int_drawable, String img_url, ImageView imv, int width, int height) {
        try {
            URL url = new URL(img_url.trim());
            URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
            Picasso.with(ctx)
                    .load("" + uri)
                    .error(int_drawable)
                    .resize(width, height)
                    .into(imv);
        } catch (Exception e) {
            Picasso.with(ctx)
                    .load(int_drawable)
                    .into(imv);
        }
    }

    /////////////////////FOR ACTIVITY EXCEPTION CASES //////////////////////////
    protected void exceptionDialog(String msg) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                BaseActivity.this);
        alertDialogBuilder.setTitle(R.string.the_farmer_dailog);
        alertDialogBuilder
                .setMessage("" + msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        //openSettings();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    ////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////// TO OPEN SETTINGS ///////////////////////////////////////
    protected void openSettings() {
        Intent intent = new Intent(android.provider.Settings.ACTION_SETTINGS);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////// FOR ONLINE CHECKING /////////////////////////////////////////
    public boolean isDeviceOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {

        } else {
            exceptionDialog(getString(R.string.sorry_you_not_online_msg));
            return false;
        }
        return true;
    }

    /////////////////////SHARED PREFERENCE ///////////////////////////////////
    public void saveUserPreference(String key, String value) {
        getSharedPreferences("User", 0).edit().putString(key, value).commit();
    }

    public void saveDataPreference(String key, String value) {
        getSharedPreferences("Data", 0).edit().putString(key, value).commit();
    }

    public String getUserPreference(String key, String value) {
        return getSharedPreferences("User", 0).getString(key, value);
    }

    public String getDataPreference(String key, String value) {
        return getSharedPreferences("Data", 0).getString(key, value);
    }

    public void deleteUserPreference() {
        getSharedPreferences("User", 0).edit().clear().commit();
    }

    public void deleteDataPreference() {
        getSharedPreferences("Data", 0).edit().clear().commit();
    }


    public void deleteSpecificValueFromData(String key) {
        getSharedPreferences("Data", 0).edit().remove(key).apply();
    }

    public void deleteSpecificValueFromUser(String key) {
        getSharedPreferences("User", 0).edit().remove(key).apply();
    }

    ///////////////////////////// RETROFIT RESPONSE //////////////////////////////////
    @Override
    public void onSuccess(Response<MSG> response, String which_method) {

    }

    @Override
    public void onFailure(Response<MSG> response) {

    }

    @Override
    public void onResponseFailure() {

    }
}
