package com.inqube.thefarmer.base;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.inqube.thefarmer.BuildConfig;
import com.inqube.thefarmer.R;
import com.inqube.thefarmer.model.MSG;
import com.squareup.picasso.Picasso;

import java.net.URI;
import java.net.URL;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import Utils.AllInterfaces;
import Utils.Constant;
import Utils.UtilClass;
import retrofit2.Response;

/**
 * Created by Tubu on 11/16/2017.
 */

public class BaseActivityWithoutMenu extends AppCompatActivity implements AllInterfaces.RetrofitResponseToActivityOrFrament {

    //GIT URL https://Suman_Ingreens@bitbucket.org/Suman_Ingreens/soil-ingreens.git
    /////////////////NORMAL PARAMS //////////////////////////
    protected boolean resume;
    ///////////////////////////////////////////////////////////
    ////////////////UI VARIABLES//////////////////////////////
    protected CoordinatorLayout col_holder;

    ////////////////////////////////////////////////////////////
    protected void onCreate(@Nullable Bundle savedInstanceState, int layout) {
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        //Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(SoilBaseActivity.this));
        setContentView(layout);
        System.out.println(" SELECTED LANGUAGE " + getDataPreference(Constant.SELECTED_LANGUAGE, BuildConfig.MARATHI));
        if (getUserPreference(Constant.SELECTED_LANGUAGE, BuildConfig.MARATHI).equalsIgnoreCase(BuildConfig.ENHLISH)) {
            UtilClass.getInstance().setLocale(BaseActivityWithoutMenu.this, "en");
        } else if (getUserPreference(Constant.SELECTED_LANGUAGE, BuildConfig.MARATHI).equalsIgnoreCase( BuildConfig.HINDI)) {
            UtilClass.getInstance().setLocale(BaseActivityWithoutMenu.this, "hi");
        } else if (getUserPreference(Constant.SELECTED_LANGUAGE, BuildConfig.MARATHI).equalsIgnoreCase(BuildConfig.MARATHI)) {
            UtilClass.getInstance().setLocale(BaseActivityWithoutMenu.this, "mar");
        }
    }

    protected void setUI() {
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!resume) {
            setUI();
        }
        resume = true;
    }


    @Override
    public void onBackPressed() {
        finish();

    }

    protected void createSnackBar(View v, String str) {
        Snackbar snackbar = Snackbar
                .make(v, str, Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.RED);
        snackbar.setDuration(Snackbar.LENGTH_SHORT);
        View view = snackbar.getView();
        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        snackbar.show();
    }

    public void appCloseDialog(String msg, final BaseActivity ctx) {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                ctx);
        alertDialogBuilder.setTitle(getResources().getString(R.string.app_name_full));
        alertDialogBuilder
                .setMessage("" + msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent startMain = new Intent(Intent.ACTION_MAIN);
                        startMain.addCategory(Intent.CATEGORY_HOME);
                        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        ctx.startActivity(startMain);
                        dialog.dismiss();
                        ctx.finish();
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    protected void hideKeyboard(BaseActivityWithoutMenu activity) {
        try {
            InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            Log.e("KeyBoardUtil", e.toString(), e);
        }
    }

    protected boolean isEmailValid(String email) {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        return matcher.matches();
    }

    protected void getImage(Context ctx, int int_drawable, String img_url, ImageView imv, int width, int height) {
        try {
            URL url = new URL(img_url.trim());
            URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
            Picasso.with(ctx)
                    .load("" + uri)
                    .error(int_drawable)
                    .resize(width, height)
                    .into(imv);
        } catch (Exception e) {
            Picasso.with(ctx)
                    .load(int_drawable)
                    .into(imv);
        }
    }

    /////////////////////FOR ACTIVITY EXCEPTION CASES //////////////////////////
    protected void exceptionDialog(String msg) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                BaseActivityWithoutMenu.this);
        alertDialogBuilder.setTitle(R.string.the_farmer_dailog);
        alertDialogBuilder
                .setMessage("" + msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        //openSettings();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    ////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////// TO OPEN SETTINGS ///////////////////////////////////////
    protected void openSettings() {
        Intent intent = new Intent(android.provider.Settings.ACTION_SETTINGS);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////// FOR ONLINE CHECKING /////////////////////////////////////////
    protected boolean isDeviceOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {

        } else {
            exceptionDialog(getString(R.string.sorry_you_not_online_msg));
            return false;
        }
        return true;
    }

    /////////////////////SHARED PREFERENCE ///////////////////////////////////
    public void saveUserPreference(String key, String value) {
        getSharedPreferences("User", 0).edit().putString(key, value).commit();
    }

    public void saveDataPreference(String key, String value) {
        getSharedPreferences("Data", 0).edit().putString(key, value).commit();
    }

    public String getUserPreference(String key, String value) {
        return getSharedPreferences("User", 0).getString(key, value);
    }

    public String getDataPreference(String key, String value) {
        return getSharedPreferences("Data", 0).getString(key, value);
    }

    public void deleteUserPreference(String key, String value) {
        getSharedPreferences("User", 0).edit().clear();
    }

    public void deleteDataPreference(String key, String value) {
        getSharedPreferences("Data", 0).edit().clear();
    }

    public void deleteSpecificValueFromData(String key) {
        getSharedPreferences("Data", 0).edit().remove(key).apply();
    }

    public void deleteSpecificValueFromUser(String key) {
        getSharedPreferences("User", 0).edit().remove(key).apply();
    }

    ///////////////////////// RetroFit RESPONSE //////////////////////////////////
    @Override
    public void onSuccess(Response<MSG> response, String which_method) {

    }

    @Override
    public void onFailure(Response<MSG> response) {

    }

    @Override
    public void onResponseFailure() {

    }
    //////////////////////////////////////////////////////////////////////////////////
}
