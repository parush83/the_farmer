package com.inqube.thefarmer.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.inqube.thefarmer.R;
import com.inqube.thefarmer.Widget.MyEditText;
import com.inqube.thefarmer.base.BaseActivity;

import org.json.JSONObject;

import Utils.Constant;

/**
 * Created by Tubu on 11/22/2017.
 */

public class BankDetailsFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    /////////////////// UI VARIABLES?/////////////////////
    private MyEditText edt_account_number, edt_bank_name, edt_ifsc_code, edt_branch_name;
    private View view;
    private CoordinatorLayout col_holder;
    //////////////////////////////////////////////////////
    ////////////////// NORMAL VARIABLES/////////////////////
    private boolean resume;
    private OnBankDetailsFragmentInteractionListener mListener;
    ///////////////////////////////////////////////////////

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.bank_information_of_detailed_registration_fragment, container, false);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!resume) {
            setUI(view);
//            view.setFocusableInTouchMode(true);
//            view.requestFocus();
//            view.setOnKeyListener(new View.OnKeyListener() {
//                @Override
//                public boolean onKey(View v, int keyCode, KeyEvent event) {
//                    if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
//                        Log.e("gif--","fragment back key is clicked");
//                        getActivity().getSupportFragmentManager().popBackStack("BankDetailsFragment", FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                        return true;
//                    }
//                    return false;
//                }
//            });
        }
        resume = true;
    }

    private void setUI(View view) {
        FloatingActionButton fab_next = (FloatingActionButton) view.findViewById(R.id.fab_next);
        fab_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkValidity() == 0) {
                    addValueToSharedPrefernce();
                    mListener.onBankDetailsFragmentNextButtonClick();
                }
            }
        });


        col_holder = (CoordinatorLayout) view.findViewById(R.id.col_holder);

        edt_account_number = (MyEditText) view.findViewById(R.id.edt_account_number);
        edt_bank_name = (MyEditText) view.findViewById(R.id.edt_bank_name);
        edt_ifsc_code = (MyEditText) view.findViewById(R.id.edt_ifsc_code);
        edt_branch_name = (MyEditText) view.findViewById(R.id.edt_branch_name);
        if (((BaseActivity) getActivity()).getDataPreference(Constant.TYPE, "").equalsIgnoreCase("self_edit")) {
            System.out.println("Farmer Name" + PersonalDetailsFragment.getUserDetails.getName());
            if (PersonalDetailsFragment.getUserDetails.getFrmBankAccount() != null) {
                edt_account_number.setText(PersonalDetailsFragment.getUserDetails.getFrmBankAccount());
            } else {
                edt_account_number.setHint(getString(R.string.text_here));
            }
            if (PersonalDetailsFragment.getUserDetails.getFrmBankName() != null) {
                edt_bank_name.setText(PersonalDetailsFragment.getUserDetails.getFrmBankName());
            } else {
                edt_bank_name.setHint(getString(R.string.text_here));
            }
            if (PersonalDetailsFragment.getUserDetails.getFrmBankIfsc() != null) {
                edt_ifsc_code.setText(PersonalDetailsFragment.getUserDetails.getFrmBankIfsc());
            } else {
                edt_ifsc_code.setHint(getString(R.string.text_here));
            }
            if (PersonalDetailsFragment.getUserDetails.getFrmBankBranch() != null) {
                edt_branch_name.setText(PersonalDetailsFragment.getUserDetails.getFrmBankBranch());
            } else {
                edt_branch_name.setHint(getString(R.string.text_here));
            }

            //edt_bank_name.setText(PersonalDetailsFragment.getUserDetails.getFrmBankName());
            //edt_ifsc_code.setText(PersonalDetailsFragment.getUserDetails.getFrmBankIfsc());
            //edt_branch_name.setText(PersonalDetailsFragment.getUserDetails.getFrmBankBranch());
        } else if (((BaseActivity) getActivity()).getDataPreference(Constant.TYPE, "").equalsIgnoreCase("farmer_edit")) {
            System.out.println("inside farmer_edit");
            System.out.println("Bank::" + PersonalDetailsFragment.getUserDetails.getFrmBankName());
            if (PersonalDetailsFragment.getUserDetails.getFrmBankAccount() != null) {
                edt_account_number.setText(PersonalDetailsFragment.getUserDetails.getFrmBankAccount());
            } else {
                edt_account_number.setHint(getString(R.string.text_here));

            }
            if (PersonalDetailsFragment.getUserDetails.getFrmBankName() != null) {
                edt_bank_name.setText(PersonalDetailsFragment.getUserDetails.getFrmBankName());
            } else {
                edt_bank_name.setHint(getString(R.string.text_here));
            }
            if (PersonalDetailsFragment.getUserDetails.getFrmBankIfsc() != null) {
                edt_ifsc_code.setText(PersonalDetailsFragment.getUserDetails.getFrmBankIfsc());
            } else {
                edt_ifsc_code.setHint(getString(R.string.text_here));
            }
            if (PersonalDetailsFragment.getUserDetails.getFrmBankBranch() != null) {
                edt_branch_name.setText(PersonalDetailsFragment.getUserDetails.getFrmBankBranch());
            } else {
                edt_branch_name.setHint(getString(R.string.text_here));
            }

            //edt_bank_name.setText(PersonalDetailsFragment.getUserDetails.getFrmBankName());
            //edt_ifsc_code.setText(PersonalDetailsFragment.getUserDetails.getFrmBankIfsc());
            //edt_branch_name.setText(PersonalDetailsFragment.getUserDetails.getFrmBankBranch());
        }
    }

    private int checkValidity() {
        if (TextUtils.isEmpty(edt_ifsc_code.getText().toString().trim())) {
            ((BaseActivity) getActivity()).createSnackBar(col_holder, getString(R.string.please_enter_ifsc_code));
            return 1;
        }
        return 0;
    }

    private void addValueToSharedPrefernce() {

        ((BaseActivity) getActivity()).saveDataPreference("frm_bank_account", edt_account_number.getText().toString().trim());
        ((BaseActivity) getActivity()).saveDataPreference("frm_bank_name", edt_bank_name.getText().toString().trim());
        ((BaseActivity) getActivity()).saveDataPreference("frm_bank_branch", edt_branch_name.getText().toString().trim());
        ((BaseActivity) getActivity()).saveDataPreference("frm_bank_ifsc", edt_ifsc_code.getText().toString().trim());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnBankDetailsFragmentInteractionListener) {
            mListener = (OnBankDetailsFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnBankDetailsFragmentInteractionListener {
        public void onBankDetailsFragmentNextButtonClick();
        // TODO: Update argument type and name
    }
}
