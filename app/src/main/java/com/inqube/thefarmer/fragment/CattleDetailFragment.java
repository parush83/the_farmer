package com.inqube.thefarmer.fragment;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.inqube.thefarmer.R;
import com.inqube.thefarmer.Widget.HTMLTextView;
import com.inqube.thefarmer.Widget.MyEditText;
import com.inqube.thefarmer.base.BaseActivity;
import com.inqube.thefarmer.model.DropDownModelClass;
import com.inqube.thefarmer.model.KsAnimalsList;
import com.inqube.thefarmer.model.KsFarmMachinerysList;

import java.lang.reflect.Type;
import java.util.ArrayList;

import Utils.Constant;

/**
 * Created by Tubu on 11/22/2017.
 */

public class CattleDetailFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    /////////////////// UI VARIABLES?/////////////////////
    private CoordinatorLayout col_holder;
    private HTMLTextView htv_list_of_cattle;
    private View view;
    private MyEditText edt_no_of_cattle, edt_no_of_pond;
    private RadioGroup rg_fish;
    private RadioButton rb_yes, rb_no;
    //////////////////////////////////////////////////////
    ////////////////// NORMAL VARIABLES/////////////////////
    private boolean resume;
    private int cul_fish = 0;
    private MyCommonAdapter myCommonAdapter;
    private ArrayList<KsAnimalsList> al_animal;
    private ArrayList<DropDownModelClass> al_drop_animal;
    private OnCattleDetailFragmentInteractionListener mListener;
    private String animal = "";
    ///////////////////////////////////////////////////////

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.cattle_information_of_detailed_registration_fragment, container, false);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!resume) {
            setUI(view);
//            view.setFocusableInTouchMode(true);
//            view.requestFocus();
//            view.setOnKeyListener(new View.OnKeyListener() {
//                @Override
//                public boolean onKey(View v, int keyCode, KeyEvent event) {
//                    if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
//                        Log.e("gif--","fragment back key is clicked");
//                        getActivity().getSupportFragmentManager().popBackStack("CattleDetailFragment", FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                        return true;
//                    }
//                    return false;
//                }
//            });
        }
        resume = true;
    }

    private void setUI(View view) {
        FloatingActionButton fab_next = (FloatingActionButton) view.findViewById(R.id.fab_next);
        fab_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkValidity() == 0) {
                    addValueToSharedPrefernce();
                    mListener.onCattleDetailFragmentNextButtonClick();
                }
            }
        });

        col_holder = (CoordinatorLayout) view.findViewById(R.id.col_holder);

        htv_list_of_cattle = (HTMLTextView) view.findViewById(R.id.htv_list_of_cattle);
        htv_list_of_cattle.setOnClickListener(this);

        edt_no_of_pond = (MyEditText) view.findViewById(R.id.edt_no_of_pond);
        edt_no_of_cattle = (MyEditText) view.findViewById(R.id.edt_no_of_cattle);

        rg_fish = (RadioGroup) view.findViewById(R.id.rg_fish);
        rg_fish.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (radioGroup.getCheckedRadioButtonId() == R.id.rb_yes) {
                    cul_fish = Constant.YES;
                } else if (radioGroup.getCheckedRadioButtonId() == R.id.rb_no) {
                    cul_fish = Constant.NO;

                } else {
                    cul_fish = 0;
                }
            }
        });

        rb_yes = (RadioButton) view.findViewById(R.id.rb_yes);
        rb_no = (RadioButton) view.findViewById(R.id.rb_no);

        Gson gson = new Gson();
        String json = ((BaseActivity) getActivity()).getDataPreference(Constant.ANIMAL_LIST, null);
        Type type = new TypeToken<ArrayList<KsAnimalsList>>() {
        }.getType();
        al_animal = gson.fromJson(json, type);

        al_drop_animal = new ArrayList<DropDownModelClass>();
        for (int i = 0; i < al_animal.size(); i++) {
            DropDownModelClass dc = new DropDownModelClass();
            dc.setName(al_animal.get(i).getAnimalName());
            dc.setId(al_animal.get(i).getAnimalId());
            al_drop_animal.add(dc);
        }

        if (((BaseActivity) getActivity()).getDataPreference(Constant.TYPE, "").equalsIgnoreCase("self_edit")) {
            System.out.println("inside self edit" + PersonalDetailsFragment.getUserDetails.getFrmAnimalNumber());

            //if (PersonalDetailsFragment.getUserDetails.getFrmAnimalNumber()) {
            //edt_no_of_cattle.setText(PersonalDetailsFragment.getUserDetails.getFrmAnimalNumber());
            //}
            //if(PersonalDetailsFragment.getUserDetails.getFrmAnimalNumber())

            if (PersonalDetailsFragment.getUserDetails.getFarmAnimalsListId() != null) {
                String s1 = PersonalDetailsFragment.getUserDetails.getFarmAnimalsListId();
                System.out.println("al_drop_machine size:" + al_drop_animal.size());

                String[] array = s1.split(",");
                for (int i = 0; i < array.length; i++) {
                    for (int j = 0; j < al_drop_animal.size(); j++) {
                        System.out.println("inside loop  " + al_drop_animal.get(j).getId());
                        System.out.println("inside loop1  " + al_drop_animal.get(j).getName());
                        System.out.println("Id1:" + al_drop_animal.get(j).getId());
                        if (al_drop_animal.get(j).getId() == Integer.parseInt(array[i])) {
                            System.out.println("Id:" + al_drop_animal.get(j).getId());
                            al_drop_animal.get(j).setSelected(true);
                        }
                    }

                    System.out.println(array[i]);
                }
            }
            if (PersonalDetailsFragment.getUserDetails.getFarmAnimalsListText() != null) {
                htv_list_of_cattle.setText(PersonalDetailsFragment.getUserDetails.getFarmAnimalsListText());
                animal = PersonalDetailsFragment.getUserDetails.getFarmAnimalsListId();
            } else {
                htv_list_of_cattle.setHint(getString(R.string.tap_here));
            }

            if ("" + PersonalDetailsFragment.getUserDetails.getFrmAnimalNumber() != null) {
                edt_no_of_cattle.setText("" + PersonalDetailsFragment.getUserDetails.getFrmAnimalNumber());
            } else {
                edt_no_of_cattle.setHint(getString(R.string.text_here));
            }
            if (PersonalDetailsFragment.getUserDetails.getFrmCultivateFish() == 1) {
                rb_yes.setChecked(true);
                rb_no.setChecked(false);
            } else if (PersonalDetailsFragment.getUserDetails.getFrmCultivateFish() == 2) {
                rb_yes.setChecked(false);
                rb_no.setChecked(true);
            } else {

            }
            if ("" + PersonalDetailsFragment.getUserDetails.getFrmCultivatePondNumber() != null) {
                edt_no_of_pond.setText("" + PersonalDetailsFragment.getUserDetails.getFrmCultivatePondNumber());
            } else {
                edt_no_of_pond.setHint(getString(R.string.text_here));
            }
        } else if (((BaseActivity) getActivity()).getDataPreference(Constant.TYPE, "").equalsIgnoreCase("farmer_edit")) {
            System.out.println("inside farmer_edit");
            //if (PersonalDetailsFragment.getUserDetails.getFrmAnimalNumber()) {
            //edt_no_of_cattle.setText(PersonalDetailsFragment.getUserDetails.getFrmAnimalNumber());
            //}
            //if(PersonalDetailsFragment.getUserDetails.getFrmAnimalNumber())

            if (PersonalDetailsFragment.getUserDetails.getFarmAnimalsListId() != null) {
                String s1 = PersonalDetailsFragment.getUserDetails.getFarmAnimalsListId();
                System.out.println("al_drop_machine size:" + al_drop_animal.size());

                String[] array = s1.split(",");
                for (int i = 0; i < array.length; i++) {
                    for (int j = 0; j < al_drop_animal.size(); j++) {
                        System.out.println("inside loop  " + al_drop_animal.get(j).getId());
                        System.out.println("inside loop1  " + al_drop_animal.get(j).getName());
                        System.out.println("Id1:" + al_drop_animal.get(j).getId());
                        if (al_drop_animal.get(j).getId() == Integer.parseInt(array[i])) {
                            System.out.println("Id:" + al_drop_animal.get(j).getId());
                            al_drop_animal.get(j).setSelected(true);
                        }
                    }

                    System.out.println(array[i]);
                }
            }
            if (PersonalDetailsFragment.getUserDetails.getFarmAnimalsListText() != null) {
                htv_list_of_cattle.setText(PersonalDetailsFragment.getUserDetails.getFarmAnimalsListText());
                animal = PersonalDetailsFragment.getUserDetails.getFarmAnimalsListId();
            } else {
                htv_list_of_cattle.setHint(getString(R.string.tap_here));
            }

            if ("" + PersonalDetailsFragment.getUserDetails.getFrmAnimalNumber() != null) {
                edt_no_of_cattle.setText("" + PersonalDetailsFragment.getUserDetails.getFrmAnimalNumber());
            } else {
                edt_no_of_cattle.setHint(getString(R.string.text_here));
            }
            if (PersonalDetailsFragment.getUserDetails.getFrmCultivateFish() == 1) {
                rb_yes.setChecked(true);
                rb_no.setChecked(false);
            } else if (PersonalDetailsFragment.getUserDetails.getFrmCultivateFish() == 2) {
                rb_yes.setChecked(false);
                rb_no.setChecked(true);
            } else {

            }
            if ("" + PersonalDetailsFragment.getUserDetails.getFrmCultivatePondNumber() != null) {
                edt_no_of_pond.setText("" + PersonalDetailsFragment.getUserDetails.getFrmCultivatePondNumber());
            } else {
                edt_no_of_pond.setHint(getString(R.string.text_here));
            }

        }
    }

    private int checkValidity() {
        if (animal.length() < 1) {
            ((BaseActivity) getActivity()).createSnackBar(col_holder, getString(R.string.please_select_cattles));
            return 1;
        }
       /*if(TextUtils.isEmpty(htv_list_of_cattle.getText())){
           ((BaseActivity) getActivity()).createSnackBar(col_holder,getString(R.string.please_select_cattles));
       }*/
        if (TextUtils.isEmpty(edt_no_of_cattle.getText().toString().trim())) {
            ((BaseActivity) getActivity()).createSnackBar(col_holder, getString(R.string.please_mention_number_of_cattle));
            return 1;
        }
        if (cul_fish == 0) {
            ((BaseActivity) getActivity()).createSnackBar(col_holder, getString(R.string.do_you_cultivate_fisheries));
            return 1;
        }

        if (TextUtils.isEmpty(edt_no_of_pond.getText().toString().trim())) {
            ((BaseActivity) getActivity()).createSnackBar(col_holder, getString(R.string.please_mention_number_of_pond));
            return 1;
        }
        return 0;
    }

    private void addValueToSharedPrefernce() {
        ((BaseActivity) getActivity()).saveDataPreference("frm_animal_number", edt_no_of_cattle.getText().toString().trim());
        ((BaseActivity) getActivity()).saveDataPreference("frm_cultivate_fish", "" + cul_fish);
        ((BaseActivity) getActivity()).saveDataPreference("frm_cultivate_pond_number", edt_no_of_pond.getText().toString().trim());
        ((BaseActivity) getActivity()).saveDataPreference("frm_animal_list", animal);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnCattleDetailFragmentInteractionListener) {
            mListener = (OnCattleDetailFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.htv_list_of_cattle:
                dropDown(al_drop_animal, getActivity(), 1);
                break;
        }
    }

    //////////////////////////  DROP DOWN ///////////////////////////////////////////

    public void dropDown(final ArrayList<DropDownModelClass> temp, Context context, final int flag) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.drop_down_cell);
        dialog.show();
        ListView lv_cell = (ListView) dialog.findViewById(R.id.lv_cell);

        implementDropDown(temp, context, lv_cell);

        Button btn_save = (Button) dialog.findViewById(R.id.btn_save);
        btn_save.setVisibility(View.VISIBLE);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (flag == 1) {
                    animal = "";
                    String sel_text = "";
                    for (int i = 0; i < temp.size(); i++) {
                        if (temp.get(i).isSelected()) {
                            sel_text = sel_text + temp.get(i).getName() + ",";
                            animal = animal + temp.get(i).getId() + ",";
                        }
                    }
                    if (sel_text.length() > 2) {
                        sel_text = sel_text.substring(0, sel_text.length() - 1);
                        htv_list_of_cattle.setText(sel_text);
                        animal = animal.substring(0, animal.length() - 1);
                    } else {
                        htv_list_of_cattle.setText("");
                    }
                    System.out.println("Animal List:" + animal);
                }
                dialog.dismiss();
            }
        });
    }

    private void implementDropDown(ArrayList<DropDownModelClass> al_rd, Context context, ListView lv) {
        myCommonAdapter = new MyCommonAdapter(context, R.layout.dropdown_list_cell, al_rd);
        lv.setAdapter(myCommonAdapter);
        myCommonAdapter.notifyDataSetChanged();
        lv.setTextFilterEnabled(true);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnCattleDetailFragmentInteractionListener {
        public void onCattleDetailFragmentNextButtonClick();
        // TODO: Update argument type and name
    }

    public class MyCommonAdapter extends ArrayAdapter<DropDownModelClass> {
        ArrayList<DropDownModelClass> my_al;
        ViewHolder holder;

        public MyCommonAdapter(Context context, int row, ArrayList<DropDownModelClass> al) {
            super(context, row, al);
            my_al = al;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.dropdown_list_cell, null);


                holder = new ViewHolder();
                holder.title = (TextView) convertView.findViewById(R.id.title);
                holder.checkedTitle = (CheckBox) convertView.findViewById(R.id.checkedTitle);
                holder.checkedTitle.setVisibility(View.VISIBLE);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.title.setText("" + my_al.get(position).getName());

            holder.checkedTitle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        my_al.get(position).setSelected(true);
                    } else {
                        my_al.get(position).setSelected(false);
                    }
                }
            });

            holder.checkedTitle.setChecked(my_al.get(position).isSelected());
            return convertView;
        }
    }

    public class ViewHolder {
        protected TextView title;
        protected CheckBox checkedTitle;
    }
}
