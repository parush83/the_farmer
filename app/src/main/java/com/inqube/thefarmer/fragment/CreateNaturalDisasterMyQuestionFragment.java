package com.inqube.thefarmer.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.inqube.thefarmer.BuildConfig;
import com.inqube.thefarmer.FarmerListActivity;
import com.inqube.thefarmer.MainHomeActivity;
import com.inqube.thefarmer.R;
import com.inqube.thefarmer.ShortRegistrationActivity;
import com.inqube.thefarmer.Widget.HTMLTextView;
import com.inqube.thefarmer.Widget.MyEditText;
import com.inqube.thefarmer.base.BaseActivity;
import com.inqube.thefarmer.model.DropDownModelClass;
import com.inqube.thefarmer.model.MSG;
import com.inqube.thefarmer.service.GPSTracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import Utils.AllInterfaces;
import Utils.Constant;
import Utils.MarshMallowPermission;
import Utils.UtilClass;
import retrofit2.Response;

public class CreateNaturalDisasterMyQuestionFragment extends Fragment implements View.OnClickListener, AllInterfaces.RetrofitResponseToActivityOrFrament {
    protected CoordinatorLayout col_holder;
    int flag1, flag, self_other_flag = 1;
    String img_data = "";
    JSONObject img1, img2, img3, img4, img5, img6, img7, img8, img9, img10;
    JSONArray img_array = new JSONArray();
    String strImg_1 = "", strImg_2 = "", strImg_3 = "", strImg_4 = "", strImg_5 = "",
            strImg_6 = "", strImg_7 = "", strImg_8 = "", strImg_9 = "", strImg_10 = "";
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    /////////////////// UI VARIABLES?/////////////////////
    private MyEditText edt_mobile_number, edt_prob_desc, edt_effected_area, edt_ques_title;
    private HTMLTextView htv_name_of_farmer, htv_district, htv_sub_division, htv_block, htv_mouza;
    private View view;
    private ImageView imv_1, imv_2, imv_3, imv_4, imv_5, imv_6, imv_7, imv_8, imv_9, imv_10;
    private FloatingActionButton fab_next;
    private FrameLayout frm1, frm2, frm3, frm4, frm5, frm6, frm7, frm8, frm9, frm10;
    private RadioButton radioButton_self, radioButton_other;
    private RadioGroup rg_self_other;
    private LinearLayout linear;
    //////////////////////////////////////////////////////
    ////////////////// NORMAL VARIABLES/////////////////////
    private boolean resume;
    private OnCreateNaturalDisasterMyQuestionFragmentInteractionListener mListener;
    private ArrayList<DropDownModelClass> al_district;
    private ArrayList<DropDownModelClass> al_sub;
    private ArrayList<DropDownModelClass> al_block;
    private int district, sub, block, mouza, dist_pos, sub_pos;
    private MyCommonAdapter ad;
    private Double latitude, longitude;
    ///////////////////////////////////////////////////////
    ////////////////////////For Camera////////////////////////
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private String userChoosenTask;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.natural_calamity_fragment, container, false);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!resume) {

//            view.setFocusableInTouchMode(true);
//            view.requestFocus();
//            view.setOnKeyListener(new View.OnKeyListener() {
//                @Override
//                public boolean onKey(View v, int keyCode, KeyEvent event) {
//                    if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
//                        Log.e("gif--","fragment back key is clicked");
//                        getActivity().getSupportFragmentManager().popBackStack("CreateNaturalDisasterMyQuestionFragment", FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                        return true;
//                    }
//                    return false;
//                }
//            });

            LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
                    new BroadcastReceiver() {
                        @Override
                        public void onReceive(Context context, Intent intent) {
                            latitude = Double.parseDouble(intent.getStringExtra("EXTRA_LATITUDE"));
                            longitude = Double.parseDouble(intent.getStringExtra("EXTRA_LONGITUDE"));
                            System.out.println("latitude: " + latitude + "longitude: " + longitude);
                            try {
                                /*Toast.makeText(getActivity(), "Lat"+String.valueOf(latitude), Toast.LENGTH_SHORT).show();
                                Toast.makeText(getActivity(), "Lon"+String.valueOf(longitude), Toast.LENGTH_SHORT).show();*/
                            } catch (Exception e) {
                                System.out.println("O Maa go Jhar Kheyechhe");
                            }
                        }
                    }, new IntentFilter(GPSTracker.ACTION_LOCATION_BROADCAST)
            );
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

            } else {
                GPSTracker gps = new GPSTracker(getActivity(), getActivity());
                if (gps.canGetLocation()) {

                    latitude = gps.getLatitude();
                    longitude = gps.getLongitude();

                    try {
                       /* Toast.makeText(getActivity(), "Lat"+String.valueOf(latitude), Toast.LENGTH_SHORT).show();
                        Toast.makeText(getActivity(), "Lon"+String.valueOf(longitude), Toast.LENGTH_SHORT).show();*/
                    } catch (Exception e) {
                        System.out.println("O Maa go Jhar Kheyechhe");
                    }
                    //Toast.makeText(getActivity(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                } else {
                    gps.showSettingsAlert();
                }
            }
            UtilClass.getInstance().parseDistrictListJson((BaseActivity) getActivity());
            setUI(view);
        }

        resume = false;
    }

    private void setUI(View view) {
        ((BaseActivity) getActivity()).setToolbarTitle(R.string.natural_calamity);
        col_holder = (CoordinatorLayout) view.findViewById(R.id.col_holder);
        linear = (LinearLayout) view.findViewById(R.id.linear);
        linear.setVisibility(View.GONE);
        edt_mobile_number = (MyEditText) view.findViewById(R.id.edt_mobile_number);
        edt_effected_area = (MyEditText) view.findViewById(R.id.edt_effected_area);
        edt_prob_desc = (MyEditText) view.findViewById(R.id.edt_prob_desc);
        edt_ques_title = (MyEditText) view.findViewById(R.id.edt_ques_title);

        rg_self_other = (RadioGroup) view.findViewById(R.id.rg_self_other);
        radioButton_self = (RadioButton) view.findViewById(R.id.radioButton_self);
        //radioButton_self.setChecked(true);
        radioButton_other = (RadioButton) view.findViewById(R.id.radioButton_other);
        htv_name_of_farmer = (HTMLTextView) view.findViewById(R.id.htv_name_of_farmer);
        htv_name_of_farmer.setOnClickListener(this);
        htv_district = (HTMLTextView) view.findViewById(R.id.htv_district);
        htv_district.setOnClickListener(this);

        htv_sub_division = (HTMLTextView) view.findViewById(R.id.htv_sub_division);
        htv_sub_division.setOnClickListener(this);

        htv_block = (HTMLTextView) view.findViewById(R.id.htv_block);
        htv_block.setOnClickListener(this);

        htv_mouza = (HTMLTextView) view.findViewById(R.id.htv_mouza);
        htv_mouza.setOnClickListener(this);


        imv_1 = (ImageView) view.findViewById(R.id.imv_1);
        imv_1.setOnClickListener(this);

        imv_2 = (ImageView) view.findViewById(R.id.imv_2);
        imv_2.setOnClickListener(this);

        imv_3 = (ImageView) view.findViewById(R.id.imv_3);
        imv_3.setOnClickListener(this);

        imv_4 = (ImageView) view.findViewById(R.id.imv_4);
        imv_4.setOnClickListener(this);

        imv_5 = (ImageView) view.findViewById(R.id.imv_5);
        imv_5.setOnClickListener(this);

        imv_6 = (ImageView) view.findViewById(R.id.imv_6);
        imv_6.setOnClickListener(this);

        imv_7 = (ImageView) view.findViewById(R.id.imv_7);
        imv_7.setOnClickListener(this);

        imv_8 = (ImageView) view.findViewById(R.id.imv_8);
        imv_8.setOnClickListener(this);

        imv_9 = (ImageView) view.findViewById(R.id.imv_9);
        imv_9.setOnClickListener(this);

        imv_10 = (ImageView) view.findViewById(R.id.imv_10);
        imv_10.setOnClickListener(this);

        frm1 = (FrameLayout) view.findViewById(R.id.frm1);
        frm1.setOnClickListener(this);

        frm2 = (FrameLayout) view.findViewById(R.id.frm2);
        frm2.setOnClickListener(this);

        frm3 = (FrameLayout) view.findViewById(R.id.frm3);
        frm3.setOnClickListener(this);

        frm4 = (FrameLayout) view.findViewById(R.id.frm4);
        frm4.setOnClickListener(this);

        frm5 = (FrameLayout) view.findViewById(R.id.frm5);
        frm5.setOnClickListener(this);

        frm6 = (FrameLayout) view.findViewById(R.id.frm6);
        frm6.setOnClickListener(this);

        frm7 = (FrameLayout) view.findViewById(R.id.frm7);
        frm7.setOnClickListener(this);

        frm8 = (FrameLayout) view.findViewById(R.id.frm8);
        frm8.setOnClickListener(this);

        frm9 = (FrameLayout) view.findViewById(R.id.frm9);
        frm9.setOnClickListener(this);

        frm10 = (FrameLayout) view.findViewById(R.id.frm10);
        frm10.setOnClickListener(this);

        fab_next = (FloatingActionButton) view.findViewById(R.id.fab_next);
        fab_next.setOnClickListener(this);

        al_district = UtilClass.getInstance().getDistrictList((BaseActivity) getActivity());

        rg_self_other.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                if (null != rb && checkedId > -1) {
                    if (rb.getText().toString().equalsIgnoreCase(getString(R.string.self))) {
                        self_other_flag = 1;
                        linear.setVisibility(View.GONE);
                        htv_name_of_farmer.setText("");
                        edt_mobile_number.setText("");
                    } else {
                        self_other_flag = 2;
                        linear.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
        System.out.println("Self_other" + self_other_flag);
        if (self_other_flag == 2) {
            System.out.println("Farmer ::" + ((BaseActivity) getActivity()).getDataPreference(Constant.FRM_NAME, ""));
            System.out.println("Farmer1 ::" + ((BaseActivity) getActivity()).getDataPreference(Constant.FRM_MOB, ""));
            linear.setVisibility(View.VISIBLE);
            // if(((BaseActivity) getActivity()).getDataPreference(Constant.FRM_NAME,"").equalsIgnoreCase("")){
            htv_name_of_farmer.setText(((BaseActivity) getActivity()).getDataPreference(Constant.FRM_NAME, ""));
            edt_mobile_number.setText(((BaseActivity) getActivity()).getDataPreference(Constant.FRM_MOB, ""));
            edt_mobile_number.setEnabled(false);
            //}
        } else {
            linear.setVisibility(View.GONE);
            htv_name_of_farmer.setText("");
            edt_mobile_number.setText("");
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnCreateNaturalDisasterMyQuestionFragmentInteractionListener) {
            mListener = (OnCreateNaturalDisasterMyQuestionFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imv_1:
                flag1 = 1;
                selectImage();
                break;
            case R.id.imv_2:
                flag1 = 2;
                selectImage();
                break;
            case R.id.imv_3:
                flag1 = 3;
                selectImage();
                break;
            case R.id.imv_4:
                flag1 = 4;
                selectImage();
                break;
            case R.id.imv_5:
                flag1 = 5;
                selectImage();
                break;
            case R.id.imv_6:
                flag1 = 6;
                selectImage();
                break;
            case R.id.imv_7:
                flag1 = 7;
                selectImage();
                break;
            case R.id.imv_8:
                flag1 = 8;
                selectImage();
                break;
            case R.id.imv_9:
                flag1 = 9;
                selectImage();
                break;
            case R.id.imv_10:
                flag1 = 10;
                selectImage();
                break;
            case R.id.frm1:
                System.out.println("click1");
                flag = 1;
                frm1.setEnabled(false);
                System.out.println("Token:" + ((BaseActivity) getActivity()).getUserPreference(Constant.TOKEN_ID, ""));
                System.out.println("STRIMG:" + strImg_1);
                if (strImg_1.equalsIgnoreCase("")) {
                } else {
                    UtilClass.getInstance().getImageResponse((BaseActivity) getActivity(), this, strImg_1, ((BaseActivity) getActivity()).getUserPreference(Constant.TOKEN_ID, ""), ((BaseActivity) getActivity()).getUserPreference(Constant.SELECTED_LANGUAGE, BuildConfig.MARATHI));
                }
                break;
            case R.id.frm2:
                System.out.println("click2");
                flag = 2;
                frm2.setEnabled(false);
                System.out.println("Token:" + ((BaseActivity) getActivity()).getUserPreference(Constant.TOKEN_ID, ""));
                System.out.println("STRIMG:" + strImg_2);
                if (strImg_2.equalsIgnoreCase("")) {

                } else {
                    UtilClass.getInstance().getImageResponse((BaseActivity) getActivity(), this, strImg_2, ((BaseActivity) getActivity()).getUserPreference(Constant.TOKEN_ID, ""), ((BaseActivity) getActivity()).getUserPreference(Constant.SELECTED_LANGUAGE, BuildConfig.MARATHI));
                }
                break;
            case R.id.frm3:
                System.out.println("click3");
                flag = 3;
                frm3.setEnabled(false);
                System.out.println("Token:" + ((BaseActivity) getActivity()).getUserPreference(Constant.TOKEN_ID, ""));
                System.out.println("STRIMG:" + strImg_3);
                if (strImg_3.equalsIgnoreCase("")) {

                } else {
                    UtilClass.getInstance().getImageResponse((BaseActivity) getActivity(), this, strImg_3, ((BaseActivity) getActivity()).getUserPreference(Constant.TOKEN_ID, ""), ((BaseActivity) getActivity()).getUserPreference(Constant.SELECTED_LANGUAGE, BuildConfig.MARATHI));
                }
                break;
            case R.id.frm4:
                System.out.println("click4");
                flag = 4;
                frm4.setEnabled(false);
                System.out.println("Token:" + ((BaseActivity) getActivity()).getUserPreference(Constant.TOKEN_ID, ""));
                System.out.println("STRIMG:" + strImg_4);
                if (strImg_4.equalsIgnoreCase("")) {

                } else {
                    UtilClass.getInstance().getImageResponse((BaseActivity) getActivity(), this, strImg_4, ((BaseActivity) getActivity()).getUserPreference(Constant.TOKEN_ID, ""), ((BaseActivity) getActivity()).getUserPreference(Constant.SELECTED_LANGUAGE, BuildConfig.MARATHI));
                }
                break;
            case R.id.frm5:
                System.out.println("click5");
                flag = 5;
                frm5.setEnabled(false);
                System.out.println("Token:" + ((BaseActivity) getActivity()).getUserPreference(Constant.TOKEN_ID, ""));
                System.out.println("STRIMG:" + strImg_5);
                if (strImg_5.equalsIgnoreCase("")) {

                } else {
                    UtilClass.getInstance().getImageResponse((BaseActivity) getActivity(), this, strImg_5, ((BaseActivity) getActivity()).getUserPreference(Constant.TOKEN_ID, ""), ((BaseActivity) getActivity()).getUserPreference(Constant.SELECTED_LANGUAGE, BuildConfig.MARATHI));
                }
                break;
            case R.id.frm6:
                System.out.println("click6");
                flag = 6;
                frm6.setEnabled(false);
                System.out.println("Token:" + ((BaseActivity) getActivity()).getUserPreference(Constant.TOKEN_ID, ""));
                System.out.println("STRIMG:" + strImg_6);
                if (strImg_6.equalsIgnoreCase("")) {

                } else {
                    UtilClass.getInstance().getImageResponse((BaseActivity) getActivity(), this, strImg_6, ((BaseActivity) getActivity()).getUserPreference(Constant.TOKEN_ID, ""), ((BaseActivity) getActivity()).getUserPreference(Constant.SELECTED_LANGUAGE, BuildConfig.MARATHI));
                }
                break;
            case R.id.frm7:
                System.out.println("click7");
                flag = 7;
                frm7.setEnabled(false);
                System.out.println("Token:" + ((BaseActivity) getActivity()).getUserPreference(Constant.TOKEN_ID, ""));
                System.out.println("STRIMG:" + strImg_7);
                if (strImg_7.equalsIgnoreCase("")) {

                } else {
                    UtilClass.getInstance().getImageResponse((BaseActivity) getActivity(), this, strImg_7, ((BaseActivity) getActivity()).getUserPreference(Constant.TOKEN_ID, ""), ((BaseActivity) getActivity()).getUserPreference(Constant.SELECTED_LANGUAGE, BuildConfig.MARATHI));
                }
                break;
            case R.id.frm8:
                System.out.println("click8");
                flag = 8;
                frm8.setEnabled(false);
                System.out.println("Token:" + ((BaseActivity) getActivity()).getUserPreference(Constant.TOKEN_ID, ""));
                System.out.println("STRIMG:" + strImg_8);
                if (strImg_8.equalsIgnoreCase("")) {

                } else {
                    UtilClass.getInstance().getImageResponse((BaseActivity) getActivity(), this, strImg_8, ((BaseActivity) getActivity()).getUserPreference(Constant.TOKEN_ID, ""), ((BaseActivity) getActivity()).getUserPreference(Constant.SELECTED_LANGUAGE, BuildConfig.MARATHI));
                }
                break;
            case R.id.frm9:
                System.out.println("click9");
                flag = 9;
                frm9.setEnabled(false);
                System.out.println("Token:" + ((BaseActivity) getActivity()).getUserPreference(Constant.TOKEN_ID, ""));
                System.out.println("STRIMG:" + strImg_9);
                if (strImg_9.equalsIgnoreCase("")) {

                } else {
                    UtilClass.getInstance().getImageResponse((BaseActivity) getActivity(), this, strImg_9, ((BaseActivity) getActivity()).getUserPreference(Constant.TOKEN_ID, ""), ((BaseActivity) getActivity()).getUserPreference(Constant.SELECTED_LANGUAGE, BuildConfig.MARATHI));
                }
                break;
            case R.id.frm10:
                System.out.println("click10");
                flag = 10;
                frm10.setEnabled(false);
                System.out.println("Token:" + ((BaseActivity) getActivity()).getUserPreference(Constant.TOKEN_ID, ""));
                System.out.println("STRIMG:" + strImg_10);
                if (strImg_10.equalsIgnoreCase("")) {

                } else {
                    UtilClass.getInstance().getImageResponse((BaseActivity) getActivity(), this, strImg_10, ((BaseActivity) getActivity()).getUserPreference(Constant.TOKEN_ID, ""), ((BaseActivity) getActivity()).getUserPreference(Constant.SELECTED_LANGUAGE, BuildConfig.MARATHI));
                }
                break;
            case R.id.htv_district:
                dropDown(al_district, getActivity(), 1);
                break;
            case R.id.htv_sub_division:
                if (!htv_district.getText().toString().equalsIgnoreCase(getResources().getString(R.string.tap_here))) {
                    System.out.println(" DIST ID " + district);
                    al_sub = UtilClass.getInstance().getSubDivisionList(getActivity(), district);
                    dropDown(al_sub, getActivity(), 2);
                }
                break;
            case R.id.htv_block:
                if (!htv_sub_division.getText().toString().equalsIgnoreCase(getResources().getString(R.string.tap_here))) {
                    al_block = UtilClass.getInstance().getBolckList((BaseActivity) getActivity(), sub);
                    dropDown(al_block, getActivity(), 3);
                }
                break;
            case R.id.htv_mouza:
                System.out.println("Block::" + block);
                if (((BaseActivity) getActivity()).isDeviceOnline()) {
                    UtilClass.getInstance().getMouzaByBlock((BaseActivity) getActivity(), this, "" + block);
                } else {
                    ((BaseActivity) getActivity()).createSnackBar(col_holder, getString(R.string.please_make_sure_your_device_is_online));
                }
                break;
            case R.id.htv_name_of_farmer:
                startActivity(new Intent(getActivity(), FarmerListActivity.class));
                break;
            case R.id.fab_next:
                if (checkQuestionValidity() == 0) {
                    UtilClass.getInstance().naturalDisasterSubmit((BaseActivity) getActivity(), this,
                            String.valueOf(self_other_flag), "3",
                            ((BaseActivity) getActivity()).getUserPreference(Constant.USER_ID, ""), htv_name_of_farmer.getText().toString(),
                            edt_mobile_number.getText().toString(), "1", String.valueOf(district), String.valueOf(sub),
                            String.valueOf(block), String.valueOf(mouza), edt_effected_area.getText().toString(),
                            String.valueOf(latitude), String.valueOf(longitude),
                            edt_ques_title.getText().toString(), edt_prob_desc.getText().toString(),
                            String.valueOf(img_array), ((BaseActivity) getActivity()).getUserPreference(Constant.TOKEN_ID, ""),
                            ((BaseActivity) getActivity()).getUserPreference(Constant.SELECTED_LANGUAGE, BuildConfig.MARATHI));
                    // call web service
                    mListener.onCreateNaturalDisasterMyQuestionFragmentNextInteractionListener();
                }
                break;
        }
    }

    private int checkQuestionValidity() {
        if (self_other_flag == 2) {
            if (TextUtils.isEmpty(htv_name_of_farmer.getText().toString())) {
                ((BaseActivity) getActivity()).createSnackBar(htv_name_of_farmer, getString(R.string.please_enter_farmer_name));
                return 1;
            }
            if (TextUtils.isEmpty(edt_mobile_number.getText().toString())) {
                ((BaseActivity) getActivity()).createSnackBar(edt_mobile_number, getString(R.string.please_enter_ten_digits_mo_number));
                return 1;
            }
            if (edt_mobile_number.getText().toString().trim().length() < 10) {
                ((BaseActivity) getActivity()).createSnackBar(edt_mobile_number, getString(R.string.please_enter_ten_digits_password)
                );
                return 1;
            }
        }
        if (htv_district.getText().toString().trim().equalsIgnoreCase(getResources().getString(R.string.tap_here))) {
            ((BaseActivity) getActivity()).createSnackBar(htv_district, getString(R.string.please_select_district));
            return 1;
        }
        if (htv_sub_division.getText().toString().trim().equalsIgnoreCase(getResources().getString(R.string.tap_here))) {
            ((BaseActivity) getActivity()).createSnackBar(htv_sub_division, getString(R.string.please_select_sub_division));
            return 1;
        }
        if (htv_mouza.getText().toString().trim().equalsIgnoreCase(getResources().getString(R.string.tap_here))) {
            ((BaseActivity) getActivity()).createSnackBar(htv_mouza, getString(R.string.please_select_mouza));
            return 1;
        }
        if (TextUtils.isEmpty(edt_effected_area.getText().toString())) {
            ((BaseActivity) getActivity()).createSnackBar(edt_effected_area, getString(R.string.please_mention_affected_area_acre));
            return 1;
        }
        if (TextUtils.isEmpty(edt_ques_title.getText().toString())) {
            ((BaseActivity) getActivity()).createSnackBar(edt_ques_title, getString(R.string.enter_ques_title));
            return 1;
        }
        if (TextUtils.isEmpty(edt_prob_desc.getText().toString())) {
            ((BaseActivity) getActivity()).createSnackBar(edt_prob_desc, getString(R.string.please_enter_prob_desc));
            return 1;
        }
        System.out.println("Array length:" + img_array.length());
        if (img_array.length() < 1) {
            ((BaseActivity) getActivity()).createSnackBar(imv_1, getString(R.string.upload_image));
            return 1;
        }

        return 0;
    }

    @Override
    public void onSuccess(Response<MSG> response, String which_method) {
        // Toast.makeText(getActivity(), "Inside Success", Toast.LENGTH_SHORT).show();
        System.out.println("Result:" + response.body());
        System.out.println("Result1:" + response.body().getMessage());
        if (which_method.equalsIgnoreCase("getMouzaByBlock")) {
            System.out.println("Block:" + block);
            ArrayList<DropDownModelClass> temp = UtilClass.getInstance().geMouza(getActivity(), response.body().getGetMouzaList(), block);
            dropDown(temp, getActivity(), 4);
        } else if (which_method.equalsIgnoreCase("imageUploadToServer")) {
            img_data = response.body().getImg_data().toString();
            if (flag == 1) {
                frm1.setEnabled(false);
                img1 = new JSONObject();
                try {
                    img1.put("image_data", img_data);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                img_array.put(img1);
            } else if (flag == 2) {
                img2 = new JSONObject();
                try {
                    img2.put("image_data", img_data);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                img_array.put(img2);
            } else if (flag == 3) {
                img3 = new JSONObject();
                try {
                    img3.put("image_data", img_data);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                img_array.put(img3);
            } else if (flag == 4) {
                img4 = new JSONObject();
                try {
                    img4.put("image_data", img_data);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                img_array.put(img4);
            } else if (flag == 5) {
                img5 = new JSONObject();
                try {
                    img5.put("image_data", img_data);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                img_array.put(img5);
            } else if (flag == 6) {
                img6 = new JSONObject();
                try {
                    img6.put("image_data", img_data);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                img_array.put(img6);
            } else if (flag == 7) {
                img7 = new JSONObject();
                try {
                    img7.put("image_data", img_data);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                img_array.put(img7);
            } else if (flag == 8) {
                img8 = new JSONObject();
                try {
                    img8.put("image_data", img_data);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                img_array.put(img8);
            } else if (flag == 9) {
                img9 = new JSONObject();
                try {
                    img9.put("image_data", img_data);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                img_array.put(img9);
            } else if (flag == 10) {
                img10 = new JSONObject();
                try {
                    img10.put("image_data", img_data);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                img_array.put(img10);
            }

            System.out.println("Image Data:" + img_data);
            System.out.println("Image Array:" + img_array);
        } else if (which_method.equalsIgnoreCase("naturalDisasterSubmitted")) {
            ((BaseActivity) getActivity()).deleteSpecificValueFromData(Constant.FRM_NAME);
            ((BaseActivity) getActivity()).deleteSpecificValueFromData(Constant.FRM_MOB);
            startActivity(new Intent(getActivity(), MainHomeActivity.class));

            //Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFailure(Response<MSG> response) {
        System.out.println("Inside onFailure");
    }

    @Override
    public void onResponseFailure() {
        System.out.println("Inside onResponseFailure");
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = UtilClass.getInstance().checkPermission(getActivity());

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
           /* frm1.setEnabled(true);
            frm2.setEnabled(true);
            frm3.setEnabled(true);
            frm4.setEnabled(true);
            frm5.setEnabled(true);
            frm6.setEnabled(true);
            frm7.setEnabled(true);
            frm8.setEnabled(true);
            frm9.setEnabled(true);
            frm10.setEnabled(true);*/
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (flag1 == 1) {
            imv_1.setImageBitmap(thumbnail);
            frm1.setEnabled(true);
            strImg_1 = getStringImage(thumbnail);
        }
        if (flag1 == 2) {
            imv_2.setImageBitmap(thumbnail);
            frm2.setEnabled(true);
            strImg_2 = getStringImage(thumbnail);
        }
        if (flag1 == 3) {
            imv_3.setImageBitmap(thumbnail);
            frm3.setEnabled(true);
            strImg_3 = getStringImage(thumbnail);
        }
        if (flag1 == 4) {
            imv_4.setImageBitmap(thumbnail);
            frm4.setEnabled(true);
            strImg_4 = getStringImage(thumbnail);
        }
        if (flag1 == 5) {
            imv_5.setImageBitmap(thumbnail);
            frm5.setEnabled(true);
            strImg_5 = getStringImage(thumbnail);
        }
        if (flag1 == 6) {
            imv_6.setImageBitmap(thumbnail);
            frm6.setEnabled(true);
            strImg_6 = getStringImage(thumbnail);
        }
        if (flag1 == 7) {
            imv_7.setImageBitmap(thumbnail);
            frm7.setEnabled(true);
            strImg_7 = getStringImage(thumbnail);
        }
        if (flag1 == 8) {
            imv_8.setImageBitmap(thumbnail);
            frm8.setEnabled(true);
            strImg_8 = getStringImage(thumbnail);
        }
        if (flag1 == 9) {
            imv_9.setImageBitmap(thumbnail);
            frm9.setEnabled(true);
            strImg_9 = getStringImage(thumbnail);
        }
        if (flag1 == 10) {
            imv_10.setImageBitmap(thumbnail);
            frm10.setEnabled(true);
            strImg_10 = getStringImage(thumbnail);
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (flag1 == 1) {
            imv_1.setImageBitmap(bm);
            frm1.setEnabled(true);
            strImg_1 = getStringImage(bm);
        }
        if (flag1 == 2) {
            imv_2.setImageBitmap(bm);
            frm2.setEnabled(true);
            strImg_2 = getStringImage(bm);
        }
        if (flag1 == 3) {
            imv_3.setImageBitmap(bm);
            frm3.setEnabled(true);
            strImg_3 = getStringImage(bm);
        }
        if (flag1 == 4) {
            imv_4.setImageBitmap(bm);
            frm4.setEnabled(true);
            strImg_4 = getStringImage(bm);
        }
        if (flag1 == 5) {
            imv_5.setImageBitmap(bm);
            frm5.setEnabled(true);
            strImg_5 = getStringImage(bm);
        }
        if (flag1 == 6) {
            imv_6.setImageBitmap(bm);
            frm6.setEnabled(true);
            strImg_6 = getStringImage(bm);
        }
        if (flag1 == 7) {
            imv_7.setImageBitmap(bm);
            frm7.setEnabled(true);
            strImg_7 = getStringImage(bm);
        }
        if (flag1 == 8) {
            imv_8.setImageBitmap(bm);
            frm8.setEnabled(true);
            strImg_8 = getStringImage(bm);
        }
        if (flag1 == 9) {
            imv_9.setImageBitmap(bm);
            frm9.setEnabled(true);
            strImg_9 = getStringImage(bm);
        }
        if (flag1 == 10) {
            imv_10.setImageBitmap(bm);
            frm10.setEnabled(true);
            strImg_10 = getStringImage(bm);
        }

        //ivImage.setImageBitmap(bm);
    }

    public String getStringImage(Bitmap bmp) {
        String encodedImage = "";
        if (bmp != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.JPEG, 25, baos);
            byte[] imageBytes = baos.toByteArray();
            encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        }

        return encodedImage;
    }

    public void dropDown(final ArrayList<DropDownModelClass> temp, Context context, final int flag) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.drop_down_cell);
        dialog.show();
        ListView lv_cell = (ListView) dialog.findViewById(R.id.lv_cell);
        lv_cell.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if (flag == 1) {
                    htv_district.setText("" + al_district.get(position).getName());
                    district = al_district.get(position).getId();
                    htv_sub_division.setText(getResources().getString(R.string.tap_here));
                    htv_block.setText(getResources().getString(R.string.tap_here));
                    htv_mouza.setText(getResources().getString(R.string.tap_here));
                    dist_pos = position;
                } else if (flag == 2) {
                    htv_sub_division.setText("" + al_sub.get(position).getName());
                    sub = al_sub.get(position).getId();
                    htv_block.setText(getResources().getString(R.string.tap_here));
                    htv_mouza.setText(getResources().getString(R.string.tap_here));
                    sub_pos = position;
                } else if (flag == 3) {
                    htv_block.setText("" + al_block.get(position).getName());
                    block = al_block.get(position).getId();
                    htv_mouza.setText(getResources().getString(R.string.tap_here));
                } else if (flag == 4) {
                    System.out.println("Name::" + temp.get(position).getName());
                    htv_mouza.setText("" + temp.get(position).getName());
                    mouza = temp.get(position).getId();
                }
                dialog.dismiss();
            }
        });
        implementDropDown(temp, context, lv_cell);
    }
    //////////////////////////  DROP DOWN ///////////////////////////////////////////

    private void implementDropDown(ArrayList<DropDownModelClass> al_rd, Context context, ListView lv) {
        ad = new MyCommonAdapter(context, R.layout.dropdown_list_cell, al_rd);
        lv.setAdapter(ad);
        ad.notifyDataSetChanged();
        lv.setTextFilterEnabled(true);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    GPSTracker gps = new GPSTracker(getActivity(), getActivity());
                    if (gps.canGetLocation()) {
                        latitude = gps.getLatitude();
                        longitude = gps.getLongitude();
                        try {
                            /*Toast.makeText(gps, "Lat"+String.valueOf(latitude), Toast.LENGTH_SHORT).show();
                            Toast.makeText(gps, "Lon"+String.valueOf(longitude), Toast.LENGTH_SHORT).show();*/
                            /*tv_latitude.setText(String.valueOf(latitude));
                            tv_longitude.setText(String.valueOf(longitude));*/
                        } catch (Exception e) {
                            System.out.println("O Maa go Jhar Kheyechhe");
                        }
                    } else {
                        gps.showSettingsAlert();
                    }
                } else {
                }
                return;
            }
            case UtilClass.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnCreateNaturalDisasterMyQuestionFragmentInteractionListener {
        // TODO: Update argument type and name
        void onCreateNaturalDisasterMyQuestionFragmentNextInteractionListener();
    }

    public class MyCommonAdapter extends ArrayAdapter<DropDownModelClass> {
        ArrayList<DropDownModelClass> my_al;

        public MyCommonAdapter(Context context, int row, ArrayList<DropDownModelClass> al) {
            super(context, row, al);
            my_al = al;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.dropdown_list_cell, null);
            }

            TextView title = (TextView) convertView.findViewById(R.id.title);
            title.setText("" + my_al.get(position).getName());

            return convertView;
        }
    }
}
