package com.inqube.thefarmer.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.inqube.thefarmer.R;
import com.inqube.thefarmer.ShortRegistrationActivity;
import com.inqube.thefarmer.Widget.HTMLTextView;
import com.inqube.thefarmer.base.BaseActivity;
import com.inqube.thefarmer.model.MSG;

import java.text.ParseException;

import Utils.AllInterfaces;
import Utils.Constant;
import Utils.UtilClass;
import retrofit2.Response;

public class HomeMainFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    /////////////////// UI VARIABLES?/////////////////////
    private View view;
    private ImageView imv_user_pic;
    private ImageButton imb_agri_problem, imb_nd_problem, imb_detail_registration, imb_govt_contact,
            imb_govt_scheme, imb_expert_opinion, imb_agri_instrument, imb_track_farmer;
    private HTMLTextView htv_name, htv_last_login;
    //////////////////////////////////////////////////////
    ////////////////// NORMAL VARIABLES/////////////////////
    private boolean resume;
    private OnHomeMainFragmentInteractionListener mListener;
    ////////////////////////////////////////////////////////////

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home_main, container, false);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!resume) {

            setUI(view);
//            view.setFocusableInTouchMode(true);
//            view.requestFocus();
//            view.setOnKeyListener(new View.OnKeyListener() {
//                @Override
//                public boolean onKey(View v, int keyCode, KeyEvent event) {
//                    if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
//                        Log.e("gif--", "fragment back key is clicked");
//                        //getActivity().getSupportFragmentManager().popBackStack("HomeMainFragment", FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                        return true;
//                    }
//                    return false;
//                }
//            });
        }
        //resume = true;
    }

    private void setUI(View view) {
        ((BaseActivity) getActivity()).setHomeToolbarTitle();
        htv_name = (HTMLTextView) view.findViewById(R.id.htv_name);
        htv_name.setText(((BaseActivity) getActivity()).getUserPreference(Constant.USER_NAME, ""));

        htv_last_login = (HTMLTextView) view.findViewById(R.id.htv_last_login);
        try {
            String date = UtilClass.getInstance().getSomeDate(((BaseActivity) getActivity()).getUserPreference(Constant.LAST_LOGIN, ""));
            htv_last_login.setText(date);
        } catch (ParseException e) {
            System.out.println("  DATE EXCEPTION " + e);
        }
        imv_user_pic = (ImageView) view.findViewById(R.id.imv_user_pic);
        UtilClass.getInstance().setProfilePicassoImage(getActivity(), imv_user_pic, ((BaseActivity) getActivity()).getUserPreference(Constant.USER_IMAGE, " "));

        imb_agri_problem = (ImageButton) view.findViewById(R.id.imb_agri_problem);
        imb_agri_problem.setOnClickListener(this);

        imb_nd_problem = (ImageButton) view.findViewById(R.id.imb_nd_problem);
        imb_nd_problem.setOnClickListener(this);

        imb_detail_registration = (ImageButton) view.findViewById(R.id.imb_detail_registration);
        imb_detail_registration.setOnClickListener(this);

        imb_govt_contact = (ImageButton) view.findViewById(R.id.imb_govt_contact);
        imb_govt_contact.setOnClickListener(this);

        imb_govt_scheme = (ImageButton) view.findViewById(R.id.imb_govt_scheme);
        imb_govt_scheme.setOnClickListener(this);

        imb_expert_opinion = (ImageButton) view.findViewById(R.id.imb_expert_opinion);
        imb_expert_opinion.setOnClickListener(this);

        imb_agri_instrument = (ImageButton) view.findViewById(R.id.imb_agri_instrument);
        imb_agri_instrument.setOnClickListener(this);

        imb_track_farmer = (ImageButton) view.findViewById(R.id.imb_track_farmer);
        imb_track_farmer.setOnClickListener(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnHomeMainFragmentInteractionListener) {
            mListener = (OnHomeMainFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imb_agri_problem:
                System.out.println(" AGRI ");
                ((BaseActivity) getActivity()).saveDataPreference(Constant.CLICK, "Agri Problem");
                mListener.onHomeMainFragmentAgriProblemInteractionListener();
                break;
            case R.id.imb_nd_problem:
                ((BaseActivity) getActivity()).saveDataPreference(Constant.CLICK, "Natural Calamity Problem");
                mListener.onHomeMainFragmentNaturalDisasterInteractionListener();
                break;
            case R.id.imb_detail_registration:
                mListener.onHomeMainFragmentDetailRegistrationInteractionListener();
                break;
            case R.id.imb_govt_contact:
                mListener.onHomeMainFragmentGovtContactsInteractionListener();
                break;
            case R.id.imb_govt_scheme:
                mListener.onHomeMainFragmentGovtSchemeInteractionListener();
                break;
            case R.id.imb_expert_opinion:
                mListener.onHomeMainFragmentExpertOpinionInteractionListener();
                break;
            case R.id.imb_agri_instrument:
                mListener.onHomeMainFragmentAgriInstrumentInteractionListener();
                break;
            case R.id.imb_track_farmer:
                mListener.onHomeMainFragmentTrackFarmerInteractionListener();
                break;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnHomeMainFragmentInteractionListener {
        // TODO: Update argument type and name
        void onHomeMainFragmentInteractionListener(Uri uri);

        void onHomeMainFragmentAgriProblemInteractionListener();

        void onHomeMainFragmentNaturalDisasterInteractionListener();

        void onHomeMainFragmentDetailRegistrationInteractionListener();

        void onHomeMainFragmentGovtContactsInteractionListener();

        void onHomeMainFragmentGovtSchemeInteractionListener();

        void onHomeMainFragmentExpertOpinionInteractionListener();

        void onHomeMainFragmentAgriInstrumentInteractionListener();

        void onHomeMainFragmentTrackFarmerInteractionListener();
    }

}
