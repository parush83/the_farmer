package com.inqube.thefarmer.fragment;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.inqube.thefarmer.R;
import com.inqube.thefarmer.Widget.HTMLTextView;
import com.inqube.thefarmer.Widget.MyEditText;
import com.inqube.thefarmer.base.BaseActivity;
import com.inqube.thefarmer.model.CropsList;
import com.inqube.thefarmer.model.DropDownModelClass;
import com.inqube.thefarmer.model.KsFarmMachinerysList;
import com.inqube.thefarmer.model.KsFarmerTypesList;
import com.inqube.thefarmer.model.MSG;

import java.lang.reflect.Type;
import java.util.ArrayList;

import Utils.Constant;
import Utils.UtilClass;
import retrofit2.Response;

/**
 * Created by Tubu on 11/22/2017.
 */

public class LandCropDetailFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    /////////////////// UI VARIABLES?/////////////////////
    private MyEditText edt_cultivating_area;
    private RadioGroup rg_farm_mechinary;
    private RadioButton rb_yes, rb_no;
    private HTMLTextView htv_machinery, htv_kharif_crop, htv_rabi, htv_pre_kharif;
    private View view;
    private CoordinatorLayout col_holder;
    //////////////////////////////////////////////////////
    ////////////////// NORMAL VARIABLES/////////////////////
    private boolean resume;
    private int has_machinery = 0;
    private MyCommonAdapter myCommonAdapter;
    private ArrayList<KsFarmMachinerysList> al_machine;
    private ArrayList<DropDownModelClass> al_rabi;
    private ArrayList<DropDownModelClass> al_kharif;
    private ArrayList<DropDownModelClass> al_prekharif;
    private ArrayList<DropDownModelClass> al_drop_machine;
    private OnLandCropDetailFragmentInteractionListener mListener;
    private String rabi = "", kharif = "", prekharif = "", machinary = "";
    ///////////////////////////////////////////////////////

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.land_and_crop_information_of_detailed_registration_fragment, container, false);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!resume) {
            setUI(view);
//            view.setFocusableInTouchMode(true);
//            view.requestFocus();
//            view.setOnKeyListener(new View.OnKeyListener() {
//                @Override
//                public boolean onKey(View v, int keyCode, KeyEvent event) {
//                    if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
//                        Log.e("gif--", "fragment back key is clicked");
//                        getActivity().getSupportFragmentManager().popBackStack("LandCropDetailFragment", FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                        return true;
//                    }
//                    return false;
//                }
//            });
        }
        resume = true;
    }

    private void setUI(View view) {
        FloatingActionButton fab_next = (FloatingActionButton) view.findViewById(R.id.fab_next);
        fab_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkValidity() == 0) {
                    addValueToSharedPrefernce();
                    mListener.onLandCropDetailFragmentNextButtonClick();
                }
            }
        });

        col_holder = (CoordinatorLayout) view.findViewById(R.id.col_holder);

        edt_cultivating_area = (MyEditText) view.findViewById(R.id.edt_cultivating_area);

        htv_machinery = (HTMLTextView) view.findViewById(R.id.htv_machinery);
        htv_machinery.setOnClickListener(this);

        htv_kharif_crop = (HTMLTextView) view.findViewById(R.id.htv_kharif_crop);
        htv_kharif_crop.setOnClickListener(this);

        htv_rabi = (HTMLTextView) view.findViewById(R.id.htv_rabi);
        htv_rabi.setOnClickListener(this);

        htv_pre_kharif = (HTMLTextView) view.findViewById(R.id.htv_pre_kharif);
        htv_pre_kharif.setOnClickListener(this);

        Gson gson = new Gson();
        String json = ((BaseActivity) getActivity()).getDataPreference(Constant.FARM_MACHINARY_LIST, null);
        Type type = new TypeToken<ArrayList<KsFarmMachinerysList>>() {
        }.getType();
        al_machine = gson.fromJson(json, type);
        al_drop_machine = new ArrayList<DropDownModelClass>();
        for (int i = 0; i < al_machine.size(); i++) {
            DropDownModelClass dc = new DropDownModelClass();
            dc.setName(al_machine.get(i).getFarmMachineryName());
            dc.setId(al_machine.get(i).getFarmMachineryId());
            al_drop_machine.add(dc);
        }

        rg_farm_mechinary = (RadioGroup) view.findViewById(R.id.rg_farm_mechinary);


        rb_yes = (RadioButton) view.findViewById(R.id.rb_yes);
        rb_no = (RadioButton) view.findViewById(R.id.rb_no);

       /* String s1= PersonalDetailsFragment.getUserDetails.getFarmMachinieryListId();
        System.out.println("al_drop_machine size:"+al_drop_machine.size());

        String[]  array = s1.split(",");
        for(int i=0;i<array.length;i++)
        {
            for (int j=1;j<al_drop_machine.size();j++){
                System.out.println("inside loop");
              if(Integer.parseInt(array[i]) ==al_drop_machine.get(j).getId())  {
                  System.out.println("Id:"+al_drop_machine.get(j).getId());
                  al_drop_machine.get(j).setSelected(true);
                  break;
              }
            }

            System.out.println(array[i]);
        }*/


        UtilClass.getInstance().parseCroptListJson(((BaseActivity) getActivity()));
        al_rabi = UtilClass.getInstance().getAllCropList(((BaseActivity) getActivity()));
        al_kharif = UtilClass.getInstance().getAllCropList(((BaseActivity) getActivity()));
        al_prekharif = UtilClass.getInstance().getAllCropList(((BaseActivity) getActivity()));

        rg_farm_mechinary.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (radioGroup.getCheckedRadioButtonId() == R.id.rb_yes) {
                    has_machinery = Constant.YES;
                   /* getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {*/
                    for (int k = 0; k < al_drop_machine.size(); k++) {
                        al_drop_machine.get(k).setSelected(false);
                    }
                       /* }
                    });*/

                } else if (radioGroup.getCheckedRadioButtonId() == R.id.rb_no) {
                    has_machinery = Constant.NO;
                    htv_machinery.setText("");
                } else {
                    has_machinery = 0;
                    htv_machinery.setText("");
                }
                System.out.println("Has Machinary::" + has_machinery);
                // Toast.makeText(getActivity(), "Has Machinary::"+has_machinery, Toast.LENGTH_SHORT).show();
            }
        });
        if (((BaseActivity) getActivity()).getDataPreference(Constant.TYPE, "").equalsIgnoreCase("self_edit")) {
            System.out.println("inside self edit");
            if (PersonalDetailsFragment.getUserDetails.getFrmTotalCultivabeLand() != null) {
                edt_cultivating_area.setText(PersonalDetailsFragment.getUserDetails.getFrmTotalCultivabeLand());
            } else {
                edt_cultivating_area.setHint(getString(R.string.text_here));
            }
            if (PersonalDetailsFragment.getUserDetails.getFrmFarmMachinery() == 1) {
                rb_yes.setChecked(true);
                rb_no.setChecked(false);
            } else if (PersonalDetailsFragment.getUserDetails.getFrmFarmMachinery() == 2) {
                rb_yes.setChecked(false);
                rb_no.setChecked(true);
            } else {

            }
            if (PersonalDetailsFragment.getUserDetails.getFarmMachinieryListText() != null) {
                htv_machinery.setText(PersonalDetailsFragment.getUserDetails.getFarmMachinieryListText());
                machinary = PersonalDetailsFragment.getUserDetails.getFarmMachinieryListId();
            } else {
                htv_machinery.setHint(getString(R.string.tap_here));
            }
            String s1 = "";
            String[] array;
            if (PersonalDetailsFragment.getUserDetails.getFarmMachinieryListId() != null) {
                s1 = PersonalDetailsFragment.getUserDetails.getFarmMachinieryListId();
                System.out.println("al_drop_machine size:" + al_drop_machine.size());

                array = s1.split(",");
                for (int i = 0; i < array.length; i++) {
                    for (int j = 0; j < al_drop_machine.size(); j++) {
                        System.out.println("inside loop  " + al_drop_machine.get(j).getId());
                        System.out.println("inside loop1  " + al_drop_machine.get(j).getName());
                        System.out.println("Id1:" + al_drop_machine.get(j).getId());
                        if (al_drop_machine.get(j).getId() == Integer.parseInt(array[i])) {
                            System.out.println("Id:" + al_drop_machine.get(j).getId());
                            al_drop_machine.get(j).setSelected(true);
                        }
                    }

                    System.out.println(array[i]);
                }
            }


            if (PersonalDetailsFragment.getUserDetails.getFarmRabiListText() != null) {
                rabi = PersonalDetailsFragment.getUserDetails.getFarmRabiListId();
                System.out.println("Rabi list:" + rabi);
                htv_rabi.setText(PersonalDetailsFragment.getUserDetails.getFarmRabiListText());
            } else {
                htv_rabi.setHint(getString(R.string.tap_here));
            }
            if (PersonalDetailsFragment.getUserDetails.getFarmRabiListId() != null) {
                s1 = PersonalDetailsFragment.getUserDetails.getFarmRabiListId();
                array = s1.split(",");
                for (int i = 0; i < array.length; i++) {
                    for (int j = 0; j < al_rabi.size(); j++) {
                        if (al_rabi.get(j).getId() == Integer.parseInt(array[i])) {
                            System.out.println("Id:" + al_rabi.get(j).getId());
                            al_rabi.get(j).setSelected(true);
                        }
                    }

                    System.out.println(array[i]);
                }
            }

            if (PersonalDetailsFragment.getUserDetails.getFarmKharifListText() != null) {
                htv_kharif_crop.setText(PersonalDetailsFragment.getUserDetails.getFarmKharifListText());
                kharif = PersonalDetailsFragment.getUserDetails.getFarmKharifListId();
            } else {
                htv_kharif_crop.setHint(getString(R.string.tap_here));
            }
            if (PersonalDetailsFragment.getUserDetails.getFarmKharifListId() != null) {
                s1 = PersonalDetailsFragment.getUserDetails.getFarmKharifListId();
                array = s1.split(",");
                for (int i = 0; i < array.length; i++) {
                    for (int j = 0; j < al_kharif.size(); j++) {
                        if (al_kharif.get(j).getId() == Integer.parseInt(array[i])) {
                            System.out.println("Id:" + al_kharif.get(j).getId());
                            al_kharif.get(j).setSelected(true);
                        }
                    }

                    System.out.println(array[i]);
                }
            }
            if (PersonalDetailsFragment.getUserDetails.getFarmPreKharifListText() != null) {
                htv_pre_kharif.setText(PersonalDetailsFragment.getUserDetails.getFarmPreKharifListText());
                prekharif = PersonalDetailsFragment.getUserDetails.getFarmPreKharifListId();
            } else {
                htv_pre_kharif.setHint(getString(R.string.tap_here));
            }
            if (PersonalDetailsFragment.getUserDetails.getFarmPreKharifListId() != null) {
                s1 = PersonalDetailsFragment.getUserDetails.getFarmPreKharifListId();
                array = s1.split(",");
                for (int i = 0; i < array.length; i++) {
                    for (int j = 0; j < al_prekharif.size(); j++) {
                        if (al_prekharif.get(j).getId() == Integer.parseInt(array[i])) {
                            System.out.println("Id:" + al_prekharif.get(j).getId());
                            al_prekharif.get(j).setSelected(true);
                        }
                    }

                    System.out.println(array[i]);
                }
            }

        } else if (((BaseActivity) getActivity()).getDataPreference(Constant.TYPE, "").equalsIgnoreCase("farmer_edit")) {
            System.out.println("inside farmer_edit");
            System.out.println("Rabii::" + PersonalDetailsFragment.getUserDetails.getFarmRabiListText());
            if (PersonalDetailsFragment.getUserDetails.getFrmTotalCultivabeLand() != null) {
                edt_cultivating_area.setText(PersonalDetailsFragment.getUserDetails.getFrmTotalCultivabeLand());
            } else {
                edt_cultivating_area.setHint(getString(R.string.text_here));
            }
            if (PersonalDetailsFragment.getUserDetails.getFrmFarmMachinery() == 1) {
                rb_yes.setChecked(true);
                rb_no.setChecked(false);
            } else if (PersonalDetailsFragment.getUserDetails.getFrmFarmMachinery() == 2) {
                rb_yes.setChecked(false);
                rb_no.setChecked(true);
            } else {

            }
            if (PersonalDetailsFragment.getUserDetails.getFarmMachinieryListText() != null) {
                htv_machinery.setText(PersonalDetailsFragment.getUserDetails.getFarmMachinieryListText());
                machinary = PersonalDetailsFragment.getUserDetails.getFarmMachinieryListId();
            } else {
                htv_machinery.setHint(getString(R.string.tap_here));
            }
            String s1 = "";
            String[] array;
            if (PersonalDetailsFragment.getUserDetails.getFarmMachinieryListId() != null) {
                s1 = PersonalDetailsFragment.getUserDetails.getFarmMachinieryListId();
                System.out.println("al_drop_machine size:" + al_drop_machine.size());

                array = s1.split(",");
                for (int i = 0; i < array.length; i++) {
                    for (int j = 0; j < al_drop_machine.size(); j++) {
                        System.out.println("inside loop  " + al_drop_machine.get(j).getId());
                        System.out.println("inside loop1  " + al_drop_machine.get(j).getName());
                        System.out.println("Id1:" + al_drop_machine.get(j).getId());
                        if (al_drop_machine.get(j).getId() == Integer.parseInt(array[i])) {
                            System.out.println("Id:" + al_drop_machine.get(j).getId());
                            al_drop_machine.get(j).setSelected(true);
                        }
                    }

                    System.out.println(array[i]);
                }
            }


            if (PersonalDetailsFragment.getUserDetails.getFarmRabiListText() != null) {
                rabi = PersonalDetailsFragment.getUserDetails.getFarmRabiListId();
                System.out.println("Rabi list:" + rabi);
                htv_rabi.setText(PersonalDetailsFragment.getUserDetails.getFarmRabiListText());
            } else {
                htv_rabi.setHint(getString(R.string.tap_here));
            }
            if (PersonalDetailsFragment.getUserDetails.getFarmRabiListId() != null) {
                s1 = PersonalDetailsFragment.getUserDetails.getFarmRabiListId();
                array = s1.split(",");
                for (int i = 0; i < array.length; i++) {
                    for (int j = 0; j < al_rabi.size(); j++) {
                        if (al_rabi.get(j).getId() == Integer.parseInt(array[i])) {
                            System.out.println("Id:" + al_rabi.get(j).getId());
                            al_rabi.get(j).setSelected(true);
                        }
                    }

                    System.out.println(array[i]);
                }
            }

            if (PersonalDetailsFragment.getUserDetails.getFarmKharifListText() != null) {
                htv_kharif_crop.setText(PersonalDetailsFragment.getUserDetails.getFarmKharifListText());
                kharif = PersonalDetailsFragment.getUserDetails.getFarmKharifListId();
            } else {
                htv_kharif_crop.setHint(getString(R.string.tap_here));
            }
            if (PersonalDetailsFragment.getUserDetails.getFarmKharifListId() != null) {
                s1 = PersonalDetailsFragment.getUserDetails.getFarmKharifListId();
                array = s1.split(",");
                for (int i = 0; i < array.length; i++) {
                    for (int j = 0; j < al_kharif.size(); j++) {
                        if (al_kharif.get(j).getId() == Integer.parseInt(array[i])) {
                            System.out.println("Id:" + al_kharif.get(j).getId());
                            al_kharif.get(j).setSelected(true);
                        }
                    }

                    System.out.println(array[i]);
                }
            }

            if (PersonalDetailsFragment.getUserDetails.getFarmPreKharifListText() != null) {
                htv_pre_kharif.setText(PersonalDetailsFragment.getUserDetails.getFarmPreKharifListText());
                prekharif = PersonalDetailsFragment.getUserDetails.getFarmPreKharifListId();
            } else {
                htv_pre_kharif.setHint(getString(R.string.tap_here));
            }
            if (PersonalDetailsFragment.getUserDetails.getFarmPreKharifListId() != null) {
                s1 = PersonalDetailsFragment.getUserDetails.getFarmPreKharifListId();
                array = s1.split(",");
                for (int i = 0; i < array.length; i++) {
                    for (int j = 0; j < al_prekharif.size(); j++) {
                        if (al_prekharif.get(j).getId() == Integer.parseInt(array[i])) {
                            System.out.println("Id:" + al_prekharif.get(j).getId());
                            al_prekharif.get(j).setSelected(true);
                        }
                    }

                    System.out.println(array[i]);
                }
            }
        }

        System.out.println("MACHINE CROP LIST SIZE " + al_kharif.size() + "   " + al_machine.size());

    }

    private int checkValidity() {
        if (TextUtils.isEmpty(edt_cultivating_area.getText().toString().trim())) {
            ((BaseActivity) getActivity()).createSnackBar(col_holder, getString(R.string.please_enter_cultivated_land));
            return 1;
        }
        if (has_machinery == 0) {
            ((BaseActivity) getActivity()).createSnackBar(col_holder, getString(R.string.has_machinary));
            return 1;
        }
        if (machinary.length() < 1 && has_machinery == 1) {
            ((BaseActivity) getActivity()).createSnackBar(col_holder, getString(R.string.please_select_machinary));
            return 1;
        }
        return 0;
    }

    private void addValueToSharedPrefernce() {

        ((BaseActivity) getActivity()).saveDataPreference("frm_total_cultivabe_land", edt_cultivating_area.getText().toString().trim());
        ((BaseActivity) getActivity()).saveDataPreference("frm_farm_machinery_list", machinary);
        ((BaseActivity) getActivity()).saveDataPreference("frm_season_rabi_list", rabi);
        ((BaseActivity) getActivity()).saveDataPreference("frm_season_kharif_list", kharif);
        ((BaseActivity) getActivity()).saveDataPreference("frm_season_pre_kharif_list", prekharif);
        ((BaseActivity) getActivity()).saveDataPreference("frm_farm_machinery", "" + has_machinery);

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnLandCropDetailFragmentInteractionListener) {
            mListener = (OnLandCropDetailFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.htv_rabi:
                dropDown(al_rabi, getActivity(), 1);
                break;
            case R.id.htv_machinery:
                if (has_machinery == 1) {
                    dropDown(al_drop_machine, getActivity(), 2);
                } else {

                }
                break;
            case R.id.htv_pre_kharif:
                dropDown(al_prekharif, getActivity(), 3);
                break;
            case R.id.htv_kharif_crop:
                dropDown(al_kharif, getActivity(), 4);
                break;
        }
    }


    //////////////////////////  DROP DOWN ///////////////////////////////////////////

    public void dropDown(final ArrayList<DropDownModelClass> temp, Context context, final int flag) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.drop_down_cell);
        dialog.show();
        ListView lv_cell = (ListView) dialog.findViewById(R.id.lv_cell);

        implementDropDown(temp, context, lv_cell);

        Button btn_save = (Button) dialog.findViewById(R.id.btn_save);
        btn_save.setVisibility(View.VISIBLE);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (flag == 1) {
                    rabi = "";
                    //rabi=rabi;
                    String sel_text = "";

                    for (int i = 0; i < temp.size(); i++) {
                        if (temp.get(i).isSelected()) {
                            sel_text = sel_text + temp.get(i).getName() + ",";
                            rabi = rabi + temp.get(i).getId() + ",";

                        }
                    }
                    if (sel_text.length() > 2) {
                        sel_text = sel_text.substring(0, sel_text.length() - 1);
                        htv_rabi.setText(sel_text);
                        rabi = rabi.substring(0, rabi.length() - 1);
                    } else {
                        htv_rabi.setText("");
                    }
                    System.out.println("Rabi List:" + rabi);

                } else if (flag == 2) {
                    String sel_text = "";
                    machinary = "";
                    for (int i = 0; i < temp.size(); i++) {
                        if (temp.get(i).isSelected()) {
                            sel_text = sel_text + temp.get(i).getName() + ",";
                            System.out.println(" MACHINE LIST " + temp.get(i).getName());
                            machinary = machinary + temp.get(i).getId() + ",";
                        }
                    }
                    if (sel_text.length() > 2) {
                        sel_text = sel_text.substring(0, sel_text.length() - 1);
                        htv_machinery.setText(sel_text);
                        machinary = machinary.substring(0, machinary.length() - 1);
                    } else {
                        htv_machinery.setText("");
                    }
                } else if (flag == 3) {
                    String sel_text = "";
                    prekharif = "";
                    for (int i = 0; i < temp.size(); i++) {
                        if (temp.get(i).isSelected()) {
                            sel_text = sel_text + temp.get(i).getName() + ",";
                            prekharif = prekharif + temp.get(i).getId() + ",";
                        }
                    }
                    if (sel_text.length() > 2) {
                        sel_text = sel_text.substring(0, sel_text.length() - 1);
                        htv_pre_kharif.setText(sel_text);
                        prekharif = prekharif.substring(0, prekharif.length() - 1);
                    } else {
                        htv_pre_kharif.setText("");
                    }
                } else if (flag == 4) {
                    String sel_text = "";
                    kharif = "";
                    for (int i = 0; i < temp.size(); i++) {
                        if (temp.get(i).isSelected()) {
                            sel_text = sel_text + temp.get(i).getName() + ",";
                            kharif = kharif + temp.get(i).getId() + ",";
                        }
                    }
                    if (sel_text.length() > 2) {
                        sel_text = sel_text.substring(0, sel_text.length() - 1);
                        htv_kharif_crop.setText(sel_text);
                        kharif = kharif.substring(0, kharif.length() - 1);
                    } else {
                        htv_kharif_crop.setText("");
                    }
                    System.out.println("Kharif List:" + kharif);
                }
                dialog.dismiss();
            }
        });
    }

    private void implementDropDown(ArrayList<DropDownModelClass> al_rd, Context context, ListView lv) {
        myCommonAdapter = new MyCommonAdapter(context, R.layout.dropdown_list_cell, al_rd);
        lv.setAdapter(myCommonAdapter);
        myCommonAdapter.notifyDataSetChanged();
        lv.setTextFilterEnabled(true);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnLandCropDetailFragmentInteractionListener {
        public void onLandCropDetailFragmentNextButtonClick();
        // TODO: Update argument type and name
    }

    public class MyCommonAdapter extends ArrayAdapter<DropDownModelClass> {
        ArrayList<DropDownModelClass> my_al;
        ViewHolder holder;

        public MyCommonAdapter(Context context, int row, ArrayList<DropDownModelClass> al) {
            super(context, row, al);
            my_al = al;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.dropdown_list_cell, null);


                holder = new ViewHolder();
                holder.title = (TextView) convertView.findViewById(R.id.title);
                holder.checkedTitle = (CheckBox) convertView.findViewById(R.id.checkedTitle);
                holder.checkedTitle.setVisibility(View.VISIBLE);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.title.setText("" + my_al.get(position).getName());

            holder.checkedTitle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        my_al.get(position).setSelected(true);
                    } else {
                        my_al.get(position).setSelected(false);
                    }
                }
            });

            holder.checkedTitle.setChecked(my_al.get(position).isSelected());
            return convertView;
        }
    }

    public class ViewHolder {
        protected TextView title;
        protected CheckBox checkedTitle;
    }
}
