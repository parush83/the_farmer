package com.inqube.thefarmer.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.inqube.thefarmer.R;

public class LeftPanelFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    /////////////////// UI VARIABLES?/////////////////////
    private LinearLayout ll_home, ll_change_password, ll_about, ll_logout;
    private View view;
    //////////////////////////////////////////////////////
    ////////////////// NORMAL VARIABLES/////////////////////
    private boolean resume;
    private OnLeftPanelFragmentInteractionListener mListener;
    ///////////////////////////////////////////////////////

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_left_panel, container, false);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!resume) {
            setUI(view);
        }
        resume = false;
    }

    private void setUI(View view) {
        ll_home = (LinearLayout) view.findViewById(R.id.ll_home);
        ll_home.setOnClickListener(this);

        ll_change_password = (LinearLayout) view.findViewById(R.id.ll_change_password);
        ll_change_password.setOnClickListener(this);

        ll_about = (LinearLayout) view.findViewById(R.id.ll_about);
        ll_about.setOnClickListener(this);

        ll_logout = (LinearLayout) view.findViewById(R.id.ll_logout);
        ll_logout.setOnClickListener(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnLeftPanelFragmentInteractionListener) {
            mListener = (OnLeftPanelFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_home:
                mListener.onLeftPanelFragmentHomeButtonInteractionListener();
                break;
            case R.id.ll_change_password:
                mListener.onLeftPanelFragmentChangePasswordButtonInteractionListener();
                break;
            case R.id.ll_about:
                mListener.onLeftPanelFragmentAboutButtonInteractionListener();
                break;
            case R.id.ll_logout:
                mListener.onLeftPanelFragmentLogoutButtonInteractionListener();
                break;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnLeftPanelFragmentInteractionListener {
        // TODO: Update argument type and name
        void onLeftPanelFragmentHomeButtonInteractionListener();

        void onLeftPanelFragmentChangePasswordButtonInteractionListener();

        void onLeftPanelFragmentAboutButtonInteractionListener();

        void onLeftPanelFragmentLogoutButtonInteractionListener();

        void onLeftPanelFragmentInteractionListener(Uri uri);
    }
}
