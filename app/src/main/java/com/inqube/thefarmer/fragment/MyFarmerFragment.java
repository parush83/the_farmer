package com.inqube.thefarmer.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.inqube.thefarmer.R;
import com.inqube.thefarmer.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import Utils.Constant;

/**
 * Created by inqube on 21/12/17.
 */

public class MyFarmerFragment extends Fragment implements View.OnClickListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    /////////////////// UI VARIABLES?/////////////////////
    private CoordinatorLayout col_holder;
    private View view;
    private RecyclerView recyclerview;
    private FloatingActionButton fab_add;
    private ViewPager viewPager;
    ///////////////////////////////////////////////////////
    //////////////////////////////////////////////////////
    ////////////////// NORMAL VARIABLES/////////////////////
    private boolean resume;
    private OnMyFarmerFragmentInteractionListener mListener;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment QuestionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static QuestionFragment newInstance(String param1, String param2) {
        QuestionFragment fragment = new QuestionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.my_farmer_fragment, container, false);

        fab_add = (FloatingActionButton) view.findViewById(R.id.fab_add);
        fab_add.setOnClickListener(this);

        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        TabLayout tabs = (TabLayout) view.findViewById(R.id.result_tabs);
        tabs.setupWithViewPager(viewPager);
        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                System.out.println("position:" + tab.getPosition());
                switch (tab.getPosition()) {
                    case 0:
                        fab_add.setVisibility(View.VISIBLE);
                         //((BaseActivity)getActivity()).saveDataPreference(Constant.TYPE,"farmer_edit");
                        break;
                    case 1:
                        ((BaseActivity)getActivity()).saveDataPreference(Constant.TYPE,"self_edit");
                        System.out.println(" TYPE SAVE " + ((BaseActivity) getActivity()).getDataPreference(Constant.TYPE, ""));
                        fab_add.setVisibility(View.GONE);
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {


            }
        });

        setupViewPager(viewPager);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

//        view.setFocusableInTouchMode(true);
//        view.requestFocus();
//        view.setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
//                    Log.e("gif--","MY fragment back key is clicked");
//                    getActivity().getSupportFragmentManager().popBackStack("MyFarmerFragment", FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                    return true;
//                }
//                return false;
//            }
//        });

        if (!resume) {

        }
        resume = true;
    }

    /*public void changeBankDetails(){
        Adapter adapter = new Adapter(getChildFragmentManager());
        adapter.addFragment(new MyFarmerListFragment(), getString(R.string.my_farmers));
        //adapter.addFragment(new MyProfileFragment(), getString(R.string.my_profile));
        adapter.addFragment(new BankDetailsFragment(), getString(R.string.my_profile));
        *//*adapter.addFragment( MyQuestionFragment.newInstance("",""), "My Question");
        adapter.addFragment(OthersQuestionFragment.newInstance("",""), "Others Question");*//*

        adapter.notifyDataSetChanged();
        viewPager.setCurrentItem(1);
    }*/

    private void setupViewPager(ViewPager viewPager) {
        Adapter adapter = new Adapter(getChildFragmentManager());
        adapter.addFragment(new MyFarmerListFragment(), getString(R.string.my_farmers));
        //adapter.addFragment(new MyProfileFragment(), getString(R.string.my_profile));
        adapter.addFragment(new PersonalDetailsFragment(), getString(R.string.my_profile));
        /*adapter.addFragment( MyQuestionFragment.newInstance("",""), "My Question");
        adapter.addFragment(OthersQuestionFragment.newInstance("",""), "Others Question");*/
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(2);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof CattleDetailFragment.OnCattleDetailFragmentInteractionListener) {
            mListener = (OnMyFarmerFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab_add:
                mListener.onFramerFragmenAddtInteractionListener(viewPager.getCurrentItem());
                ((BaseActivity) getActivity()).saveDataPreference(Constant.TYPE, "new");
                break;
        }
    }

    public interface OnMyFarmerFragmentInteractionListener {
        void onFramerFragmenAddtInteractionListener(int position);
    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            System.out.println(" CALLED ");
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
