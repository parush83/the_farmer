package com.inqube.thefarmer.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.inqube.thefarmer.BuildConfig;
import com.inqube.thefarmer.R;
import com.inqube.thefarmer.adapter.FarmerListAdapter;
import com.inqube.thefarmer.base.BaseActivity;
import com.inqube.thefarmer.model.MSG;

import Utils.AllInterfaces;
import Utils.Constant;
import Utils.UtilClass;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MyFarmerListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MyFarmerListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyFarmerListFragment extends Fragment implements AllInterfaces.RetrofitResponseToActivityOrFrament {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    protected CoordinatorLayout col_holder;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    ////////////////// NORMAL VARIABLES/////////////////////
    private boolean resume;
    private View view;
    /////////////////Ui Variables///////////////////////
    private RecyclerView recyclerview;
    private OnFragmentInteractionListener mListener;

    public MyFarmerListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyFarmerListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyFarmerListFragment newInstance(String param1, String param2) {
        MyFarmerListFragment fragment = new MyFarmerListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_my_farmer_list, container, false);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!resume) {
            setUI(view);
            //UtilClass.getInstance().getQuestionsList((BaseActivity) getActivity(), this, ((BaseActivity) getActivity()).getUserPreference(Constant.USER_ID, ""), ((BaseActivity) getActivity()).getUserPreference(Constant.TOKEN_ID, ""), ((BaseActivity) getActivity()).getDataPreference(Constant.WHICH_PROBLEM, ""));
        }
        //UtilClass.getInstance().getOtherQuestionsList((BaseActivity)getActivity(),this,((BaseActivity )  getActivity()).getUserPreference(Constant.USER_ID,""),((BaseActivity )  getActivity()).getUserPreference(Constant.TOKEN_ID,""),"1");
        UtilClass.getInstance().getFarmerList((BaseActivity) getActivity(), this, ((BaseActivity) getActivity()).getUserPreference(Constant.USER_ID, ""), ((BaseActivity) getActivity()).getUserPreference(Constant.TOKEN_ID, ""), ((BaseActivity) getActivity()).getUserPreference(Constant.SELECTED_LANGUAGE, BuildConfig.MARATHI +
                "" +
                ""));
        resume = false;
    }

    private void setUI(View view) {

        //((BaseActivity) getActivity()).setToolbarTitle(R.string.crop_problem);
        col_holder = (CoordinatorLayout) view.findViewById(R.id.col_holder);
        recyclerview = (RecyclerView) view.findViewById(R.id.recyclerview);
        GridLayoutManager llm = new GridLayoutManager(getActivity(), 1);
        recyclerview.setLayoutManager(llm);
        //questionsList=new ArrayList<GetList>();
        //adapter=new QuestionsListAdapter(recyclerview,getActivity(),questionsList,col_holder);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onSuccess(Response<MSG> response, String which_method) {
        if (which_method.equalsIgnoreCase("getFarmerList")) {
            System.out.println("inside getFramerList Success");
            //question_loaded=true;

            final MSG msg = new Gson().fromJson(new Gson().toJson(response.body()), MSG.class);
            System.out.println("Size:" + msg.getGetuserList().size());
            //list.setAdapter( adapter );
            recyclerview.setAdapter(new FarmerListAdapter(recyclerview, getActivity(), msg.getGetuserList(), col_holder, new AllInterfaces.CustomItemClickListener() {
                @Override
                public void onItemClick(View v, int position, String name) {
                    ((BaseActivity) getActivity()).saveDataPreference(Constant.TYPE, "farmer_edit");
                    ((BaseActivity) getActivity()).saveDataPreference(Constant.FARMER_ID, "" + msg.getGetuserList().get(position).getId());
                    System.out.println("Farmer Id::" + ((BaseActivity) getActivity()).getDataPreference(Constant.FARMER_ID, ""));
                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.fm_container, new PersonalDetailsFragment(), "PersonalDetailsFragment")
                            .addToBackStack("PersonalDetailsFragment")
                            .commit();
                }
            }));
        }
    }

    @Override
    public void onFailure(Response<MSG> response) {

    }

    @Override
    public void onResponseFailure() {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
