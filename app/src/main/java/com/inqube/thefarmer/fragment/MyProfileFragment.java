/*
package com.inqube.thefarmer.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.inqube.thefarmer.R;
import com.inqube.thefarmer.Widget.HTMLTextView;
import com.inqube.thefarmer.Widget.MyEditText;
import com.inqube.thefarmer.base.BaseActivity;
import com.inqube.thefarmer.model.DropDownModelClass;
import com.inqube.thefarmer.model.KsFarmerTypesList;

import java.lang.reflect.Type;
import java.util.ArrayList;

import Utils.Constant;

*/
/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MyProfileFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MyProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 * <p>
 * Use this factory method to create a new instance of
 * this fragment using the provided parameters.
 *
 * @param param1 Parameter 1.
 * @param param2 Parameter 2.
 * @return A new instance of fragment MyProfileFragment.
 * <p>
 * This interface must be implemented by activities that contain this
 * fragment to allow an interaction in this fragment to be communicated
 * to the activity and potentially other fragments contained in that
 * activity.
 * <p>
 * See the Android Training lesson <a href=
 * "http://developer.android.com/training/basics/fragments/communicating.html"
 * >Communicating with Other Fragments</a> for more information.
 *//*

public class MyProfileFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private HTMLTextView htv_district, htv_sub_division, htv_block, htv_mouza, htv_group, htv_type_of_farmer, htv_type_of_land;
    private MyEditText edt_farmer_name, edt_father_name, edt_pin_code, edt_mobile_number, edt_kcc, edt_id_father_name, edt_id;
    private RadioGroup rg_gender, rg_kcc,rg_id;
    private RadioButton rb_male, rb_female, rb_kcc_yes, rb_kcc_no,rb_aadhar,rb_voter_id;
    private View view;
    private CoordinatorLayout col_holder;

    private OnFragmentInteractionListener mListener;

    public MyProfileFragment() {
        // Required empty public constructor
    }

    */
/**
 * Use this factory method to create a new instance of
 * this fragment using the provided parameters.
 *
 * @param param1 Parameter 1.
 * @param param2 Parameter 2.
 * @return A new instance of fragment MyProfileFragment.
 *//*

    // TODO: Rename and change types and number of parameters
    public static MyProfileFragment newInstance(String param1, String param2) {
        MyProfileFragment fragment = new MyProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.personal_information_of_detailed_registration_fragment, container, false);
    }

    private void setUI(View view) {
        col_holder = (CoordinatorLayout) view.findViewById(R.id.col_holder);

        htv_district = (HTMLTextView) view.findViewById(R.id.htv_district);
        htv_district.setOnClickListener(this);

        htv_sub_division = (HTMLTextView) view.findViewById(R.id.htv_sub_division);
        htv_sub_division.setOnClickListener(this);

        htv_block = (HTMLTextView) view.findViewById(R.id.htv_block);
        htv_block.setOnClickListener(this);

        htv_mouza = (HTMLTextView) view.findViewById(R.id.htv_mouza);
        htv_mouza.setOnClickListener(this);

        htv_group = (HTMLTextView) view.findViewById(R.id.htv_group);
        htv_group.setOnClickListener(this);

        htv_type_of_farmer = (HTMLTextView) view.findViewById(R.id.htv_type_of_farmer);
        htv_type_of_farmer.setOnClickListener(this);

        htv_type_of_land = (HTMLTextView) view.findViewById(R.id.htv_type_of_land);
        htv_type_of_land.setOnClickListener(this);

        edt_farmer_name = (MyEditText) view.findViewById(R.id.edt_farmer_name);
        edt_farmer_name.setText(((BaseActivity) getActivity()).getUserPreference(Constant.FARMER_NAME,""));
        edt_farmer_name.setEnabled(false);

        edt_father_name = (MyEditText) view.findViewById(R.id.edt_father_name);
        edt_pin_code = (MyEditText) view.findViewById(R.id.edt_pin_code);

        edt_mobile_number = (MyEditText) view.findViewById(R.id.edt_mobile_number);
        edt_mobile_number.setText(((BaseActivity) getActivity()).getUserPreference(Constant.FARMER_PRIMARY_NUMBER,""));
        edt_mobile_number.setEnabled(false);

        edt_kcc = (MyEditText) view.findViewById(R.id.edt_kcc);
        edt_id= (MyEditText) view.findViewById(R.id.edt_id);
        edt_id_father_name = (MyEditText) view.findViewById(R.id.edt_id_father_name);

        rg_gender = (RadioGroup) view.findViewById(R.id.rg_gender);
        */
/*rg_gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (radioGroup.getCheckedRadioButtonId() == R.id.rb_male) {
                    gender = Constant.MALE;
                } else if (radioGroup.getCheckedRadioButtonId() == R.id.rb_female) {
                    gender = Constant.FEMALE;
                } else {
                    gender = 0;
                }
            }
        });*//*

        rg_kcc = (RadioGroup) view.findViewById(R.id.rg_kcc);
       */
/* rg_kcc.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (radioGroup.getCheckedRadioButtonId() == R.id.rb_kcc_yes) {
                    kcc = Constant.YES;
                } else if (radioGroup.getCheckedRadioButtonId() == R.id.rb_kcc_no) {
                    kcc = Constant.NO;
                } else {
                    kcc = 0;
                }
            }
        });*//*


        rg_id = (RadioGroup) view.findViewById(R.id.rg_id);
        */
/*rg_id.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (radioGroup.getCheckedRadioButtonId() == R.id.rb_aadhar) {
                    has_id_proof = Constant.AADHAR;
                } else if (radioGroup.getCheckedRadioButtonId() == R.id.rb_voter_id) {
                    has_id_proof = Constant.VOTER_ID;
                } else {
                    has_id_proof = 0;
                }
            }
        });*//*

        rb_male = (RadioButton) view.findViewById(R.id.rb_male);
        rb_female = (RadioButton) view.findViewById(R.id.rb_female);
        rb_kcc_yes = (RadioButton) view.findViewById(R.id.rb_kcc_yes);
        rb_kcc_no = (RadioButton) view.findViewById(R.id.rb_kcc_no);
        rb_aadhar = (RadioButton) view.findViewById(R.id.rb_aadhar);
        rb_voter_id = (RadioButton) view.findViewById(R.id.rb_voter_id);


        */
/*FloatingActionButton fab_next = (FloatingActionButton) view.findViewById(R.id.fab_next);
        fab_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkValidity() == 0) {
                    addValueToSharedPrefernce();
                    mListener.onPersonalDetailsFragmentNextButtonClick();
                }
            }
        });

        al_group = new ArrayList<DropDownModelClass>();
        DropDownModelClass dm = new DropDownModelClass();

        dm.setId(1);
        dm.setName(getString(R.string.general));
        al_group.add(dm);

        dm = new DropDownModelClass();

        dm.setId(2);
        dm.setName(getString(R.string.sc));
        al_group.add(dm);

        dm = new DropDownModelClass();
        dm.setId(3);
        dm.setName(getString(R.string.st));
        al_group.add(dm);

        dm = new DropDownModelClass();
        dm.setId(4);
        dm.setName(getString(R.string.minor));
        al_group.add(dm);

        al_land_type = new ArrayList<DropDownModelClass>();
        dm = new DropDownModelClass();

        dm.setId(1);
        dm.setName(getString(R.string.irrigated));
        al_land_type.add(dm);

        dm = new DropDownModelClass();

        dm.setId(2);
        dm.setName(getString(R.string.rainfed));
        al_land_type.add(dm);


        Gson gson = new Gson();
        String json = ((BaseActivity)getActivity()).getDataPreference(Constant.FARMER_TYPE_LIST, null);
        Type type = new TypeToken<ArrayList<KsFarmerTypesList>>() {}.getType();
        al_ks_farmer_type = gson.fromJson(json, type);

        System.out.println(" FRAMER LIST SIZE "+al_ks_farmer_type.size());

        al_farmer_type=new ArrayList<DropDownModelClass>();
        for(int i=0;i<al_ks_farmer_type.size();i++){
            dm=new DropDownModelClass();
            dm.setId(al_ks_farmer_type.get(i).getId());
            dm.setName(al_ks_farmer_type.get(i).getFarmerTypeName());
            al_farmer_type.add(dm);
        }*//*

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {

    }

    */
/**
 * This interface must be implemented by activities that contain this
 * fragment to allow an interaction in this fragment to be communicated
 * to the activity and potentially other fragments contained in that
 * activity.
 * <p>
 * See the Android Training lesson <a href=
 * "http://developer.android.com/training/basics/fragments/communicating.html"
 * >Communicating with Other Fragments</a> for more information.
 *//*

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
*/
