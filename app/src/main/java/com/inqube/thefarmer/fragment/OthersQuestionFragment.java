package com.inqube.thefarmer.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.inqube.thefarmer.AnswerListActivity;
import com.inqube.thefarmer.BuildConfig;
import com.inqube.thefarmer.R;
import com.inqube.thefarmer.adapter.OtherQuestionAdapter;
import com.inqube.thefarmer.adapter.QuestionsListAdapter;
import com.inqube.thefarmer.base.BaseActivity;
import com.inqube.thefarmer.model.GetList;
import com.inqube.thefarmer.model.MSG;

import java.util.List;

import Utils.AllInterfaces;
import Utils.Constant;
import Utils.UtilClass;
import retrofit2.Response;

public class OthersQuestionFragment extends Fragment implements AllInterfaces.RetrofitResponseToActivityOrFrament {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    protected CoordinatorLayout col_holder;
    ///////////////UI VARIABLE/////////////////////
    RecyclerView recyclerview;
    List<GetList> questionsList;
    QuestionsListAdapter adapter;
    TextView txt;
    ////////////NORMAL VARIABLE////////////////
    private boolean resume;
    private View view;
    private boolean question_loaded, isVisible;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    private OnOthersQuestionFragmentInteractionListener mListener;

    public OthersQuestionFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static OthersQuestionFragment newInstance(String param1, String param2) {
        OthersQuestionFragment fragment = new OthersQuestionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.my_and_others_question_fragment, container, false);
        setUserVisibleHint(true);
        UtilClass.getInstance().getOtherQuestionsList((BaseActivity) getActivity(), this, ((BaseActivity) getActivity()).getUserPreference(Constant.USER_ID, ""), ((BaseActivity) getActivity()).getUserPreference(Constant.TOKEN_ID, ""), ((BaseActivity) getActivity()).getDataPreference(Constant.WHICH_PROBLEM, ""), ((BaseActivity) getActivity()).getUserPreference(Constant.SELECTED_LANGUAGE, BuildConfig.MARATHI));
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!resume) {
            // UtilClass.getInstance().getOtherQuestionsList((BaseActivity) getActivity(), this, ((BaseActivity) getActivity()).getUserPreference(Constant.USER_ID, ""), ((BaseActivity) getActivity()).getUserPreference(Constant.TOKEN_ID, ""), ((BaseActivity) getActivity()).getDataPreference(Constant.WHICH_PROBLEM, ""));
            System.out.println(" OTHER QUSETION  ");
        }
        resume = true;
    }

    private void setUI(View view) {
        if (((BaseActivity) getActivity()).getDataPreference(Constant.CLICK, "").equalsIgnoreCase("Agri Problem")) {
            ((BaseActivity) getActivity()).setToolbarTitle(R.string.crop_problem);
        } else {
            ((BaseActivity) getActivity()).setToolbarTitle(R.string.natural_calamity);
        }
        //((BaseActivity) getActivity()).setToolbarTitle(R.string.crop_problem);
        col_holder = (CoordinatorLayout) view.findViewById(R.id.col_holder);
        recyclerview = (RecyclerView) view.findViewById(R.id.recyclerview);
        GridLayoutManager llm = new GridLayoutManager(getActivity(), 1);
        recyclerview.setLayoutManager(llm);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnOthersQuestionFragmentInteractionListener) {
            mListener = (OnOthersQuestionFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onSuccess(Response<MSG> response, String which_method) {
        if (which_method.equalsIgnoreCase("getOtherQuestionsList")) {
            //question_loaded = true;
            setUI(view);
            final MSG msg = new Gson().fromJson(new Gson().toJson(response.body()), MSG.class);
            System.out.println("Size:" + msg.getGetList().size());
//            LinearLayoutManager llm = new LinearLayoutManager(getActivity());
//            llm.setOrientation(LinearLayoutManager.VERTICAL);
//            recyclerview.setLayoutManager(llm);
//            //list.setAdapter( adapter );
            recyclerview.setAdapter(new OtherQuestionAdapter(recyclerview, getActivity(), msg.getGetList().get(0).getData(), col_holder, new AllInterfaces.CustomItemClickListener() {
                @Override
                public void onItemClick(View v, int position, String question_title) {
                    System.out.println(" POSITION " + position);
                    Intent ans = new Intent(getActivity(), AnswerListActivity.class);

                    ans.putExtra("ques_id", msg.getGetList().get(0).getData().get(position).getId().toString());
                    ans.putExtra("question_type_id", ((BaseActivity) getActivity()).getUserPreference(Constant.WHICH_PROBLEM, Constant.AGRI));
                    ans.putExtra("question", question_title);
                    startActivity(ans);
                }
            }));
        }
    }

    @Override
    public void onFailure(Response<MSG> response) {

    }

    @Override
    public void onResponseFailure() {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnOthersQuestionFragmentInteractionListener {
        // TODO: Update argument type and name
        void onOthersQuestionFragmentInteraction(Uri uri);
    }
}