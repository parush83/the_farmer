package com.inqube.thefarmer.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.inqube.thefarmer.BuildConfig;
import com.inqube.thefarmer.R;
import com.inqube.thefarmer.ShortRegistrationActivity;
import com.inqube.thefarmer.Widget.HTMLTextView;
import com.inqube.thefarmer.Widget.MyEditText;
import com.inqube.thefarmer.base.BaseActivity;
import com.inqube.thefarmer.model.DropDownModelClass;
import com.inqube.thefarmer.model.GetUserDetails;
import com.inqube.thefarmer.model.KsFarmerTypesList;
import com.inqube.thefarmer.model.MSG;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

import Utils.AllInterfaces;
import Utils.Constant;
import Utils.UtilClass;
import retrofit2.Response;

/**
 * Created by Tubu on 11/22/2017.
 */

public class PersonalDetailsFragment extends Fragment implements View.OnClickListener, AllInterfaces.RetrofitResponseToActivityOrFrament {
    public static GetUserDetails getUserDetails;
    public static String img = "";
    LinearLayout linear_pass, linear_conf_pass;
    View view1, view2;
    String strImg = "";
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    /////////////////// UI VARIABLES?/////////////////////
    private HTMLTextView htv_district, htv_sub_division, htv_block, htv_mouza, htv_group, htv_type_of_farmer, htv_type_of_land;
    private MyEditText edt_farmer_name, edt_father_name, edt_pin_code, edt_mobile_number, edt_kcc, edt_id_father_name, edt_id, edt_pass, edt_conf_pass;
    private RadioGroup rg_gender, rg_kcc, rg_id;
    private RadioButton rb_male, rb_female, rb_kcc_yes, rb_kcc_no, rb_aadhar, rb_voter_id;
    private View view;
    private CoordinatorLayout col_holder;
    private ImageView imv_user_pic;
    private RadioGroup rg_language;
    private RadioButton rb_english, rb_hindi, rb_marathi;
    private int selectedLanguage = 0;
    //////////////////////////////////////////////////////
    ////////////////// NORMAL VARIABLES/////////////////////
    private boolean resume;//,camera_flag;
    private int gender = 0, kcc = 0, has_id_proof = 0;
    private OnPersonalDetailsFragmentInteractionListener mListener;
    private MyCommonAdapter myCommonAdapter;
    private ArrayList<DropDownModelClass> al_district;
    private ArrayList<DropDownModelClass> al_sub;
    private ArrayList<DropDownModelClass> al_block;
    private ArrayList<DropDownModelClass> al_group;
    private ArrayList<DropDownModelClass> al_land_type;
    private ArrayList<KsFarmerTypesList> al_ks_farmer_type;
    private ArrayList<DropDownModelClass> al_farmer_type;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private String userChoosenTask;
    private int district, sub, block, mouza, group, type_of_land, type_of_farmer;
    ///////////////////////////////////////////////////////


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            resume = false;
            onResume();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.personal_information_of_detailed_registration_fragment, container, false);
        System.out.println("2nd execute");
        return view;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("dist", "Hello");
    }

    /*@Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            //Restore the fragment's state here
            htv_district.setText(savedInstanceState.getString("dist"));
        }
    }*/


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        System.out.println("Inside onActivityCreated");

        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            System.out.println("Value:" + savedInstanceState.getString("dist"));
            //Restore the fragment's state here
            htv_district.setText(savedInstanceState.getString("dist"));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!resume) {
            setUI(view);
            System.out.println("Inside Resume " + resume);
//            view.setFocusableInTouchMode(true);
//            view.requestFocus();
//            view.setOnKeyListener(new View.OnKeyListener() {
//                @Override
//                public boolean onKey(View v, int keyCode, KeyEvent event) {
//                    if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
//                        Log.e("gif--", "fragment back key is clicked");
//                        if(((BaseActivity) getActivity()).getDataPreference(Constant.TYPE,"").equalsIgnoreCase("new")||((BaseActivity) getActivity()).getDataPreference(Constant.TYPE,"").equalsIgnoreCase("farmer_edit")) {
//                            ((BaseActivity)getActivity()).saveDataPreference(Constant.TYPE,"self_edit");
//                        }
//                        getActivity().getSupportFragmentManager().popBackStack("PersonalDetailsFragment", FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                        return true;
//                    }
//                    return false;
//                }
//            });
            UtilClass.getInstance().parseDistrictListJson(((BaseActivity) getActivity()));
            al_district = UtilClass.getInstance().getDistrictList(((BaseActivity) getActivity()));
        }
        resume = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        // resume=false;
       /* if(camera_flag){
            resume=false;
            //camera_flag=false;
            System.out.println("Inside Pause1 "+resume);
        }else{
            resume=true;
        }*/
        System.out.println("Inside Pause " + resume);
    }

    @Override
    public void onStop() {
        super.onStop();
        System.out.println("Inside STOP " + resume);
    }

    private void setUI(View view) {

        System.out.println("Value:" + ((BaseActivity) getActivity()).getDataPreference(Constant.TYPE, ""));
        col_holder = (CoordinatorLayout) view.findViewById(R.id.col_holder);

        imv_user_pic = (ImageView) view.findViewById(R.id.imv_user_pic);
        imv_user_pic.setOnClickListener(this);
        htv_district = (HTMLTextView) view.findViewById(R.id.htv_district);
        htv_district.setOnClickListener(this);

        htv_sub_division = (HTMLTextView) view.findViewById(R.id.htv_sub_division);
        htv_sub_division.setOnClickListener(this);

        htv_block = (HTMLTextView) view.findViewById(R.id.htv_block);
        htv_block.setOnClickListener(this);

        htv_mouza = (HTMLTextView) view.findViewById(R.id.htv_mouza);
        htv_mouza.setOnClickListener(this);

        htv_group = (HTMLTextView) view.findViewById(R.id.htv_group);
        htv_group.setOnClickListener(this);

        htv_type_of_farmer = (HTMLTextView) view.findViewById(R.id.htv_type_of_farmer);
        htv_type_of_farmer.setOnClickListener(this);

        htv_type_of_land = (HTMLTextView) view.findViewById(R.id.htv_type_of_land);
        htv_type_of_land.setOnClickListener(this);

        edt_farmer_name = (MyEditText) view.findViewById(R.id.edt_farmer_name);
        /*edt_farmer_name.setText(((BaseActivity) getActivity()).getUserPreference(Constant.FARMER_NAME,""));
        edt_farmer_name.setEnabled(false);*/

        //edt_father_name = (MyEditText) view.findViewById(R.id.edt_father_name);
        edt_pin_code = (MyEditText) view.findViewById(R.id.edt_pin_code);

        edt_mobile_number = (MyEditText) view.findViewById(R.id.edt_mobile_number);
        /*edt_mobile_number.setText(((BaseActivity) getActivity()).getUserPreference(Constant.FARMER_PRIMARY_NUMBER,""));
        edt_mobile_number.setEnabled(false);*/

        edt_kcc = (MyEditText) view.findViewById(R.id.edt_kcc);
        edt_id = (MyEditText) view.findViewById(R.id.edt_id);
        edt_id_father_name = (MyEditText) view.findViewById(R.id.edt_id_father_name);
        edt_pass = (MyEditText) view.findViewById(R.id.edt_pass);
        edt_conf_pass = (MyEditText) view.findViewById(R.id.edt_conf_pass);

        rg_gender = (RadioGroup) view.findViewById(R.id.rg_gender);
        rg_gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (radioGroup.getCheckedRadioButtonId() == R.id.rb_male) {
                    gender = Constant.MALE;
                } else if (radioGroup.getCheckedRadioButtonId() == R.id.rb_female) {
                    gender = Constant.FEMALE;
                } else {
                    gender = 0;
                }
            }
        });

        rg_kcc = (RadioGroup) view.findViewById(R.id.rg_kcc);
        rg_kcc.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (radioGroup.getCheckedRadioButtonId() == R.id.rb_kcc_yes) {
                    kcc = Constant.YES;
                    edt_kcc.setEnabled(true);
                } else if (radioGroup.getCheckedRadioButtonId() == R.id.rb_kcc_no) {
                    kcc = Constant.NO;
                    edt_kcc.clearText();
                    edt_kcc.setEnabled(false);
                } else {
                    kcc = 0;
                }
            }
        });


        rg_id = (RadioGroup) view.findViewById(R.id.rg_id);
        rg_id.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (radioGroup.getCheckedRadioButtonId() == R.id.rb_aadhar) {
                    has_id_proof = Constant.AADHAR;
                } else if (radioGroup.getCheckedRadioButtonId() == R.id.rb_voter_id) {
                    has_id_proof = Constant.VOTER_ID;
                } else {
                    has_id_proof = 0;
                }
            }
        });

        rb_male = (RadioButton) view.findViewById(R.id.rb_male);
        rb_female = (RadioButton) view.findViewById(R.id.rb_female);
        rb_kcc_yes = (RadioButton) view.findViewById(R.id.rb_kcc_yes);
        rb_kcc_no = (RadioButton) view.findViewById(R.id.rb_kcc_no);
        rb_aadhar = (RadioButton) view.findViewById(R.id.rb_aadhar);
        rb_voter_id = (RadioButton) view.findViewById(R.id.rb_voter_id);

        rb_english = (RadioButton) view.findViewById(R.id.rb_english);
        rb_hindi = (RadioButton) view.findViewById(R.id.rb_hindi);
        rb_marathi = (RadioButton) view.findViewById(R.id.rb_marathi);
        view1 = (View) view.findViewById(R.id.view1);
        view2 = (View) view.findViewById(R.id.view2);

        rg_language = (RadioGroup) view.findViewById(R.id.rg_language);
        rg_language.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (radioGroup.getCheckedRadioButtonId() == R.id.rb_english) {
                    selectedLanguage = Integer.parseInt(BuildConfig.ENHLISH);
//                    Intent intent=new Intent(android.provider.Settings.ACTION_LOCALE_SETTINGS);
//                    startActivity(intent);
                } else if (radioGroup.getCheckedRadioButtonId() == R.id.rb_hindi) {
                    selectedLanguage = Integer.parseInt(BuildConfig.HINDI);
//                    Intent intent=new Intent(android.provider.Settings.ACTION_LOCALE_SETTINGS);
//                    startActivity(intent);
                } else if (radioGroup.getCheckedRadioButtonId() == R.id.rb_marathi) {
                    selectedLanguage = Integer.parseInt(BuildConfig.MARATHI);
                }
                //saveUserPreference(Constant.SELECTED_LANGUAGE, "" + selectedLanguage);
            }
        });
        linear_pass = (LinearLayout) view.findViewById(R.id.linear_pass);
        linear_conf_pass = (LinearLayout) view.findViewById(R.id.linear_conf_pass);


        FloatingActionButton fab_next = (FloatingActionButton) view.findViewById(R.id.fab_next);
        fab_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkValidity() == 0) {
                    addValueToSharedPrefernce();
                    if (((BaseActivity) getActivity()).getDataPreference(Constant.TYPE, "").equalsIgnoreCase("self_edit")) {
                        System.out.println("Inside");
                        mListener.onPersonalDetailsFragmentNextButtonClickForSelfEdit();
                    } else {
                        mListener.onPersonalDetailsFragmentNextButtonClick();
                    }

                }
            }
        });

        al_group = new ArrayList<DropDownModelClass>();
        DropDownModelClass dm = new DropDownModelClass();

        dm.setId(1);
        dm.setName(getString(R.string.general));
        al_group.add(dm);

        dm = new DropDownModelClass();

        dm.setId(2);
        dm.setName(getString(R.string.sc));
        al_group.add(dm);

        dm = new DropDownModelClass();
        dm.setId(3);
        dm.setName(getString(R.string.st));
        al_group.add(dm);

        dm = new DropDownModelClass();
        dm.setId(4);
        dm.setName(getString(R.string.minor));
        al_group.add(dm);

        al_land_type = new ArrayList<DropDownModelClass>();
        dm = new DropDownModelClass();

        dm.setId(1);
        dm.setName(getString(R.string.irrigated));
        al_land_type.add(dm);

        dm = new DropDownModelClass();

        dm.setId(2);
        dm.setName(getString(R.string.rainfed));
        al_land_type.add(dm);


        Gson gson = new Gson();
        String json = ((BaseActivity) getActivity()).getDataPreference(Constant.FARMER_TYPE_LIST, null);
        Type type = new TypeToken<ArrayList<KsFarmerTypesList>>() {
        }.getType();
        al_ks_farmer_type = gson.fromJson(json, type);

        System.out.println(" FRAMER LIST SIZE " + al_ks_farmer_type.size());

        al_farmer_type = new ArrayList<DropDownModelClass>();
        for (int i = 0; i < al_ks_farmer_type.size(); i++) {
            dm = new DropDownModelClass();
            dm.setId(al_ks_farmer_type.get(i).getFarmerTypeId());
            dm.setName(al_ks_farmer_type.get(i).getFarmerTypeName());
            al_farmer_type.add(dm);
        }

        ///////////////////////////For My Profile Edit/////////////////////
        System.out.println("Inside Framer Edit " + ((BaseActivity) getActivity()).getDataPreference(Constant.TYPE, ""));
        if (((BaseActivity) getActivity()).getDataPreference(Constant.TYPE, "").equalsIgnoreCase("farmer_edit")) {
            System.out.println("Inside Framer Edit");
            linear_pass.setVisibility(View.GONE);
            linear_conf_pass.setVisibility(View.GONE);
            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            UtilClass.getInstance().getUserDetails((BaseActivity) getActivity(), this, ((BaseActivity) getActivity()).getDataPreference(Constant.FARMER_ID, ""), ((BaseActivity) getActivity()).getUserPreference(Constant.SELECTED_LANGUAGE, BuildConfig.MARATHI));

        } else if (((BaseActivity) getActivity()).getDataPreference(Constant.TYPE, "").equalsIgnoreCase("self_edit")) {
            System.out.println("inside self edit");
            linear_pass.setVisibility(View.GONE);
            linear_conf_pass.setVisibility(View.GONE);
            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            UtilClass.getInstance().getUserDetails((BaseActivity) getActivity(), this, ((BaseActivity) getActivity()).getUserPreference(Constant.USER_ID, ""), ((BaseActivity) getActivity()).getUserPreference(Constant.SELECTED_LANGUAGE, BuildConfig.MARATHI));

        } else {
            linear_pass.setVisibility(View.VISIBLE);
            linear_conf_pass.setVisibility(View.VISIBLE);
            view1.setVisibility(View.VISIBLE);
            view2.setVisibility(View.VISIBLE);
            System.out.println("Inside Condition not working");
        }
    }

    private void addValueToSharedPrefernce() {
        ((BaseActivity) getActivity()).saveDataPreference("frm_name", edt_farmer_name.getText().toString().trim());
        ((BaseActivity) getActivity()).saveDataPreference("frm_primary_number", edt_mobile_number.getText().toString().trim());
        ((BaseActivity) getActivity()).saveDataPreference("frm_gender", "" + gender);
        ((BaseActivity) getActivity()).saveDataPreference("frm_pincode", edt_pin_code.getText().toString().trim());
        ((BaseActivity) getActivity()).saveDataPreference("frm_father_name", edt_id_father_name.getText().toString().trim());
        ((BaseActivity) getActivity()).saveDataPreference("frm_category", "" + group);
        ((BaseActivity) getActivity()).saveDataPreference("frm_farmer_type", "" + type_of_farmer);
        ((BaseActivity) getActivity()).saveDataPreference("frm_farmer_land_type", "" + type_of_land);
        ((BaseActivity) getActivity()).saveDataPreference("frm_id_proof_number", edt_id.getText().toString().trim());
        ((BaseActivity) getActivity()).saveDataPreference("frm_id_proof", "" + has_id_proof);
        ((BaseActivity) getActivity()).saveDataPreference("frm_kcc_card", "" + kcc);
        if (kcc == 1) {
            ((BaseActivity) getActivity()).saveDataPreference("frm_kcc_card_number", edt_kcc.getText().toString().trim());
        } else {
            ((BaseActivity) getActivity()).saveDataPreference("frm_kcc_card_number", "");
        }
        ((BaseActivity) getActivity()).saveDataPreference("frm_state", "" + BuildConfig.STATE_ID);
        ((BaseActivity) getActivity()).saveDataPreference("frm_district", "" + district);
        ((BaseActivity) getActivity()).saveDataPreference("frm_subdivision", "" + sub);
        ((BaseActivity) getActivity()).saveDataPreference("frm_block", "" + block);
        ((BaseActivity) getActivity()).saveDataPreference("frm_mouza", "" + mouza);
        ((BaseActivity) getActivity()).saveDataPreference("frm_lang_id", "" + selectedLanguage);
        ((BaseActivity) getActivity()).saveDataPreference("frm_password", "" + edt_pass.getText().toString());
        if (strImg != null) {
            ((BaseActivity) getActivity()).saveDataPreference("frm_img", "" + strImg);
        } else {
            ((BaseActivity) getActivity()).saveDataPreference("frm_img", "" + ((BaseActivity) getActivity()).getUserPreference(Constant.USER_IMAGE, " "));
        }


    }

    private int checkValidity() {

        if (htv_district.getText().toString().equalsIgnoreCase(getString(R.string.tap_here))) {
            ((BaseActivity) getActivity()).createSnackBar(col_holder, getString(R.string.please_select_district));
            return 1;
        }
        if (htv_sub_division.getText().toString().equalsIgnoreCase(getString(R.string.tap_here))) {
            ((BaseActivity) getActivity()).createSnackBar(col_holder, getString(R.string.please_select_sub_division));
            return 1;
        }
        if (htv_block.getText().toString().trim().equalsIgnoreCase(getResources().getString(R.string.tap_here))) {
            ((BaseActivity) getActivity()).createSnackBar(htv_district, getString(R.string.please_select_block));
            return 1;
        }
        if (TextUtils.isEmpty(edt_farmer_name.getText().toString())) {
            ((BaseActivity) getActivity()).createSnackBar(col_holder, getString(R.string.please_enter_farmer_name));
            return 1;
        }
       /* if (TextUtils.isEmpty(edt_father_name.getText().toString())) {
            ((BaseActivity) getActivity()).createSnackBar(col_holder, getString(R.string.please_enter_father_name));
            return 1;
        }*/
        if (gender == 0) {
            ((BaseActivity) getActivity()).createSnackBar(col_holder, getString(R.string.please_select_your_gender));
            return 1;
        }
        if (TextUtils.isEmpty(edt_pin_code.getText().toString())) {
            ((BaseActivity) getActivity()).createSnackBar(col_holder, getString(R.string.please_enter_pin_code));
            return 1;
        }
        if (edt_pin_code.getText().toString().trim().length() < 6) {
            ((BaseActivity) getActivity()).createSnackBar(col_holder, getString(R.string.enter_6_digit_pin));

            return 1;
        }
        if (TextUtils.isEmpty(edt_mobile_number.getText().toString())) {
            ((BaseActivity) getActivity()).createSnackBar(col_holder, getString(R.string.please_enter_ten_digits_mo_number));
            return 1;
        }
        if (edt_mobile_number.getText().toString().trim().length() < 10) {
            ((BaseActivity) getActivity()).createSnackBar(col_holder, getString(R.string.please_enter_ten_digits_password)
            );
            return 1;
        }
        if (htv_group.getText().toString().trim().equalsIgnoreCase(getResources().getString(R.string.tap_here))) {
            ((BaseActivity) getActivity()).createSnackBar(htv_district, getString(R.string.please_select_your_caste));
            return 1;
        }
        if (htv_type_of_farmer.getText().toString().trim().equalsIgnoreCase(getResources().getString(R.string.tap_here))) {
            ((BaseActivity) getActivity()).createSnackBar(htv_district, getString(R.string.please_select_type_of_farmer));
            return 1;
        }
        if (htv_type_of_land.getText().toString().trim().equalsIgnoreCase(getResources().getString(R.string.tap_here))) {
            ((BaseActivity) getActivity()).createSnackBar(htv_district, getString(R.string.please_select_type_of_land));
            return 1;
        }
        if (has_id_proof == 0) {
            ((BaseActivity) getActivity()).createSnackBar(col_holder, getString(R.string.has_aadhar_or_voter_card));
            return 1;
        }
        if (TextUtils.isEmpty(edt_id.getText().toString())) {
            ((BaseActivity) getActivity()).createSnackBar(col_holder, getString(R.string.please_enter_id_number));
            return 1;
        }
        if (TextUtils.isEmpty(edt_id_father_name.getText().toString())) {
            ((BaseActivity) getActivity()).createSnackBar(col_holder, getString(R.string.please_enter_id_father_name));
            return 1;
        }
        if (kcc == 0) {
            ((BaseActivity) getActivity()).createSnackBar(col_holder, getString(R.string.kvv_holder_or_not));
            return 1;
        }
        if (kcc == 1) {
            if (TextUtils.isEmpty(edt_kcc.getText().toString())) {
                ((BaseActivity) getActivity()).createSnackBar(col_holder, getString(R.string.please_enter_kcc_number));
                return 1;
            }
        }
        if (((BaseActivity) getActivity()).getDataPreference(Constant.TYPE, "").equalsIgnoreCase("self_edit")) {

        } else if (((BaseActivity) getActivity()).getDataPreference(Constant.TYPE, "").equalsIgnoreCase("farmer_edit")) {

        } else {
            if (TextUtils.isEmpty(edt_pass.getText().toString())) {
                ((BaseActivity) getActivity()).createSnackBar(edt_pass, getString(R.string.please_enter_password));
                return 1;
            }
            if (TextUtils.isEmpty(edt_conf_pass.getText().toString())) {
                ((BaseActivity) getActivity()).createSnackBar(edt_conf_pass, getString(R.string.please_enter_confirm_password));
                return 1;
            }
            if (!edt_pass.getText().toString().trim().equalsIgnoreCase(edt_conf_pass.getText().toString().trim())) {
                ((BaseActivity) getActivity()).createSnackBar(edt_conf_pass, getString(R.string.p_c_p_not_matching));
                return 1;
            }
        }

        if (selectedLanguage == 0) {
            ((BaseActivity) getActivity()).createSnackBar(rg_language, getString(R.string.please_select_a_language));
            return 1;
        }
        return 0;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnPersonalDetailsFragmentInteractionListener) {
            mListener = (OnPersonalDetailsFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.htv_district:
                dropDown(al_district, getActivity(), 1);
                break;
            case R.id.htv_sub_division:
                if (!htv_district.getText().toString().equalsIgnoreCase(getResources().getString(R.string.tap_here))) {
                    System.out.println(" DIST ID " + district);
                    al_sub = UtilClass.getInstance().getSubDivisionList(getActivity(), district);
                    dropDown(al_sub, getActivity(), 2);
                }

                break;
            case R.id.htv_block:
                if (!htv_sub_division.getText().toString().equalsIgnoreCase(getResources().getString(R.string.tap_here))) {
                    al_block = UtilClass.getInstance().getBolckList(getActivity(), sub);
                    dropDown(al_block, getActivity(), 3);
                }
                break;
            case R.id.htv_mouza:
                System.out.println("Block::" + block);
                if (((BaseActivity) getActivity()).isDeviceOnline()) {
                    UtilClass.getInstance().getMouzaByBlock(((BaseActivity) getActivity()), this, "" + block);
                } else {
                    ((BaseActivity) getActivity()).createSnackBar(col_holder, getString(R.string.please_make_sure_your_device_is_online));
                }
                break;
            case R.id.htv_group:
                dropDown(al_group, getActivity(), 5);
                break;
            case R.id.htv_type_of_farmer:
                dropDown(al_farmer_type, getActivity(), 6);
                break;
            case R.id.htv_type_of_land:
                dropDown(al_land_type, getActivity(), 7);
                break;
            case R.id.imv_user_pic:
                selectImage();
                break;
        }
    }

    //////////////////////////////Image//////////////////////////////
    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = UtilClass.getInstance().checkPermission(getActivity());

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            //camera_flag=true;
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
            /*frm1.setEnabled(true);
            frm2.setEnabled(true);
            frm3.setEnabled(true);
            frm4.setEnabled(true);
            frm5.setEnabled(true);
            frm6.setEnabled(true);
            frm7.setEnabled(true);
            frm8.setEnabled(true);
            frm9.setEnabled(true);
            frm10.setEnabled(true);*/

        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //imv_user_pic.setImageBitmap(thumbnail);
        //frm1.setEnabled(true);
        strImg = getStringImage(thumbnail);
        img = String.valueOf(getImageUri(getContext(), thumbnail));
        UtilClass.getInstance().setProfilePicassoImage(getActivity(), imv_user_pic, img);
        //((BaseActivity)getActivity()).saveUserPreference(Constant.USER_IMAGE,strImg);


    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //imv_user_pic.setImageBitmap(bm);
        //resume=true;
        strImg = getStringImage(bm);
        img = String.valueOf(getImageUri(getContext(), bm));
        UtilClass.getInstance().setProfilePicassoImage(getActivity(), imv_user_pic, img);
        //((BaseActivity)getActivity()).saveUserPreference(Constant.USER_IMAGE,strImg);
        //ivImage.setImageBitmap(bm);
    }

    public String getStringImage(Bitmap bmp) {
        String encodedImage = "";
        if (bmp != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] imageBytes = baos.toByteArray();
            encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        }

        return encodedImage;
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    //////////////////////////  DROP DOWN ///////////////////////////////////////////

    public void dropDown(final ArrayList<DropDownModelClass> temp, Context context, final int flag) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.drop_down_cell);
        dialog.show();
        ListView lv_cell = (ListView) dialog.findViewById(R.id.lv_cell);
        lv_cell.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if (flag == 1) {
                    htv_district.setText("" + temp.get(position).getName());
                    district = temp.get(position).getId();
                    htv_sub_division.setText(getResources().getString(R.string.tap_here));
                    htv_block.setText(getResources().getString(R.string.tap_here));
                    htv_mouza.setText(getResources().getString(R.string.tap_here));

                } else if (flag == 2) {
                    htv_sub_division.setText("" + temp.get(position).getName());
                    sub = temp.get(position).getId();
                    htv_block.setText(getResources().getString(R.string.tap_here));
                    htv_mouza.setText(getResources().getString(R.string.tap_here));
                } else if (flag == 3) {
                    htv_block.setText("" + temp.get(position).getName());
                    block = temp.get(position).getId();
                    htv_mouza.setText(getResources().getString(R.string.tap_here));
                } else if (flag == 4) {
                    htv_mouza.setText("" + temp.get(position).getName());
                    mouza = temp.get(position).getId();
                } else if (flag == 5) {
                    htv_group.setText("" + temp.get(position).getName());
                    group = temp.get(position).getId();
                } else if (flag == 6) {
                    htv_type_of_farmer.setText("" + temp.get(position).getName());
                    type_of_farmer = temp.get(position).getId();
                } else if (flag == 7) {
                    htv_type_of_land.setText("" + temp.get(position).getName());
                    type_of_land = temp.get(position).getId();
                }
                dialog.dismiss();
            }
        });
        implementDropDown(temp, context, lv_cell);
    }

    private void implementDropDown(ArrayList<DropDownModelClass> al_rd, Context context, ListView lv) {
        myCommonAdapter = new MyCommonAdapter(context, R.layout.dropdown_list_cell, al_rd);
        lv.setAdapter(myCommonAdapter);
        myCommonAdapter.notifyDataSetChanged();
        lv.setTextFilterEnabled(true);
    }

    @Override
    public void onSuccess(Response<MSG> response, String which_method) {
        if (which_method.equalsIgnoreCase("getMouzaByBlock")) {
            System.out.println(" MOUZA SIZE " + response.body().getGetMouzaList().size() + "  " + block);
            ArrayList<DropDownModelClass> temp = UtilClass.getInstance().geMouza(getActivity(), response.body().getGetMouzaList(), block);
            dropDown(temp, getActivity(), 4);
        } else if (which_method.equalsIgnoreCase("getUserDetails")) {
            System.out.println("Inside Get User Details");
            getUserDetails = response.body().getGetUserDetails();
            System.out.println("Response Code:" + response.code());
            System.out.println("Name:" + response.body().getGetUserDetails().getFrmPrimaryNumber());
            System.out.println("Bank Name:" + getUserDetails.getFrmBankBranch());
            System.out.println("Group:" + response.body().getGetUserDetails().getFrmCategory());
            if (response.body().getGetUserDetails().getFrmDistrictName() != null) {
                htv_district.setText(response.body().getGetUserDetails().getFrmDistrictName());
                district = response.body().getGetUserDetails().getFrmDistrict();
            } else {
                htv_district.setHint(getString(R.string.tap_here));
            }
            if (response.body().getGetUserDetails().getFrmSubdivisionName() != null) {
                htv_sub_division.setText(response.body().getGetUserDetails().getFrmSubdivisionName());
                sub = response.body().getGetUserDetails().getFrmSubdivision();
            } else {
                htv_sub_division.setHint(getString(R.string.tap_here));
            }
            if (response.body().getGetUserDetails().getFrmBlockName() != null) {
                htv_block.setText(response.body().getGetUserDetails().getFrmBlockName());
                block = response.body().getGetUserDetails().getFrmBlock();

            } else {
                htv_block.setHint(getString(R.string.tap_here));
            }
            if (response.body().getGetUserDetails().getFrmMouzaName() != null) {
                htv_mouza.setText(response.body().getGetUserDetails().getFrmMouzaName());
                mouza = response.body().getGetUserDetails().getFrmMouza();
                //htv_mouza.setEnabled(false);
            } else {
                htv_mouza.setHint(getString(R.string.tap_here));
            }
            if (response.body().getGetUserDetails().getName() != null) {
                edt_farmer_name.setText(response.body().getGetUserDetails().getName());
                //edt_farmer_name.setEnabled(false);
            } else {

            }
            if (response.body().getGetUserDetails().getFrmLanguageId() == 1) {
                rb_english.setChecked(true);
                rb_hindi.setChecked(false);
                rb_marathi.setChecked(false);

            } else if (response.body().getGetUserDetails().getFrmLanguageId() == 2) {
                rb_english.setChecked(false);
                rb_hindi.setChecked(true);
                rb_marathi.setChecked(false);
            } else if (response.body().getGetUserDetails().getFrmLanguageId() == 3) {
                rb_english.setChecked(false);
                rb_hindi.setChecked(false);
                rb_marathi.setChecked(true);
            } else {

            }
            edt_mobile_number.setText(response.body().getGetUserDetails().getFrmPrimaryNumber());
            if (((BaseActivity) getActivity()).getDataPreference(Constant.TYPE, "").equalsIgnoreCase("farmer_edit")) {
                System.out.println("Inside farmerrrrrr");
                htv_district.setEnabled(false);
                img = response.body().getGetUserDetails().getProfileImageSource();
                UtilClass.getInstance().setProfilePicassoImage(getActivity(), imv_user_pic, img);
               /* htv_sub_division.setEnabled(true);
                htv_mouza.setEnabled(true);
                htv_block.setEnabled(true);
                edt_farmer_name.setEnabled(true);
                edt_mobile_number.setEnabled(true);*/
            } else if (((BaseActivity) getActivity()).getDataPreference(Constant.TYPE, "").equalsIgnoreCase("self_edit")) {
                System.out.println("Inside selffffff");
                img = ((BaseActivity) getActivity()).getUserPreference(Constant.USER_IMAGE, " ");
                UtilClass.getInstance().setProfilePicassoImage(getActivity(), imv_user_pic, img);
                System.out.println("Inside Else1");
                htv_district.setEnabled(false);
                htv_sub_division.setEnabled(false);
                htv_mouza.setEnabled(false);
                htv_block.setEnabled(false);
                edt_farmer_name.setEnabled(false);
                edt_mobile_number.setEnabled(false);
            }

            System.out.println("Inside Dist" + response.body().getGetUserDetails().getFrmDistrictName());
            System.out.println("Inside Pin" + response.body().getGetUserDetails().getFrmPincode());

            edt_pin_code.setText(response.body().getGetUserDetails().getFrmPincode());
            edt_id_father_name.setText(response.body().getGetUserDetails().getFrmFatherName());
            //edt_mobile_number.setEnabled(false);
            if (response.body().getGetUserDetails().getFrmCategory() == 1) {
                htv_group.setText(getString(R.string.general));
                group = response.body().getGetUserDetails().getFrmCategory();
            } else if (response.body().getGetUserDetails().getFrmCategory() == 2) {
                htv_group.setText(getString(R.string.sc));
                group = response.body().getGetUserDetails().getFrmCategory();
            } else if (response.body().getGetUserDetails().getFrmCategory() == 3) {
                htv_group.setText(getString(R.string.st));
                group = response.body().getGetUserDetails().getFrmCategory();
            } else if (response.body().getGetUserDetails().getFrmCategory() == 4) {
                htv_group.setText(getString(R.string.minor));
                group = response.body().getGetUserDetails().getFrmCategory();
            } else {
                htv_group.setText(getString(R.string.tap_here));
            }
            if (response.body().getGetUserDetails().getFrmFarmerLandType() == 1) {
                htv_type_of_land.setText(getString(R.string.irrigated));
                type_of_land = response.body().getGetUserDetails().getFrmFarmerLandType();
            } else if (response.body().getGetUserDetails().getFrmFarmerLandType() == 2) {
                htv_type_of_land.setText(getString(R.string.rainfed));
                type_of_land = response.body().getGetUserDetails().getFrmFarmerLandType();
            } else {
                htv_type_of_land.setText(getString(R.string.tap_here));
            }
            if (response.body().getGetUserDetails().getFrmIdProof() == 1) {
                rb_aadhar.setChecked(true);
                rb_voter_id.setChecked(false);
                has_id_proof = response.body().getGetUserDetails().getFrmIdProof();
            } else if (response.body().getGetUserDetails().getFrmIdProof() == 2) {
                rb_voter_id.setChecked(true);
                rb_aadhar.setChecked(false);
                has_id_proof = response.body().getGetUserDetails().getFrmIdProof();
            } else {

            }
            if (response.body().getGetUserDetails().getFrmKccCard() == 1) {
                rb_kcc_yes.setChecked(true);
                rb_kcc_no.setChecked(false);
                kcc = response.body().getGetUserDetails().getFrmKccCard();
            } else if (response.body().getGetUserDetails().getFrmKccCard() == 2) {
                rb_kcc_no.setChecked(true);
                rb_kcc_yes.setChecked(false);
                kcc = response.body().getGetUserDetails().getFrmKccCard();
            }
            if (response.body().getGetUserDetails().getFrmKccCardNumber() != null) {
                edt_kcc.setText(response.body().getGetUserDetails().getFrmKccCardNumber());
            } else {
                edt_kcc.setHint(getString(R.string.text_here));
            }
            // edt_father_name.setText(response.body().getGetUserDetails().getFrmFatherName());
            if (response.body().getGetUserDetails().getFrmGender().equalsIgnoreCase("1")) {
                rb_male.setChecked(true);
                rb_female.setChecked(false);
                gender = Integer.parseInt(response.body().getGetUserDetails().getFrmGender());
            } else if (response.body().getGetUserDetails().getFrmGender().equalsIgnoreCase("2")) {
                rb_female.setChecked(true);
                rb_male.setChecked(false);
                gender = Integer.parseInt(response.body().getGetUserDetails().getFrmGender());
            } else {
                gender = 0;
            }
            if (response.body().getGetUserDetails().getFrmFarmerTypeText() != null) {
                htv_type_of_farmer.setText(response.body().getGetUserDetails().getFrmFarmerTypeText());
                // htv_type_of_farmer.setEnabled(false);
                type_of_farmer = response.body().getGetUserDetails().getFrmFarmerType();
            } else {
                htv_type_of_farmer.setHint(getString(R.string.tap_here));
            }
            if (response.body().getGetUserDetails().getFrmIdProofNumber() != null) {
                edt_id.setText(response.body().getGetUserDetails().getFrmIdProofNumber());
            } else {
                edt_id.setHint(getString(R.string.text_here));
            }
        }
    }

    @Override
    public void onFailure(Response<MSG> response) {

    }

    @Override
    public void onResponseFailure() {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnPersonalDetailsFragmentInteractionListener {
        public void onPersonalDetailsFragmentNextButtonClick();

        public void onPersonalDetailsFragmentNextButtonClickForSelfEdit();
        // TODO: Update argument type and name
    }

    public class MyCommonAdapter extends ArrayAdapter<DropDownModelClass> {
        ArrayList<DropDownModelClass> my_al;

        public MyCommonAdapter(Context context, int row, ArrayList<DropDownModelClass> al) {
            super(context, row, al);
            my_al = al;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.dropdown_list_cell, null);
            }

            TextView title = (TextView) convertView.findViewById(R.id.title);
            title.setText("" + my_al.get(position).getName());
            return convertView;
        }
    }
}
