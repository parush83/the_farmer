package com.inqube.thefarmer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sanjib on 21/11/17.
 */

public class CropGrowthStageList {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("growth_stage_name")
    @Expose
    private String growthStageName;
    @SerializedName("growth_stage_id")
    @Expose
    private Integer growthStageId;
    @SerializedName("language_name")
    @Expose
    private String languageName;
    @SerializedName("crop_id")
    @Expose
    private Integer crop_id;

    public Integer getCrop_id() {
        return crop_id;
    }

    public void setCrop_id(Integer crop_id) {
        this.crop_id = crop_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGrowthStageName() {
        return growthStageName;
    }

    public void setGrowthStageName(String growthStageName) {
        this.growthStageName = growthStageName;
    }

    public Integer getGrowthStageId() {
        return growthStageId;
    }

    public void setGrowthStageId(Integer growthStageId) {
        this.growthStageId = growthStageId;
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }
}
