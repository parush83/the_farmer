package com.inqube.thefarmer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by sanjib on 18/11/17.
 */

public class CropTypeList {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("crops_type_name")
    @Expose
    private String cropsTypeName;
    @SerializedName("crops_types_language_id")
    @Expose
    private Integer cropsTypesLanguageId;
    @SerializedName("crops_types_id")
    @Expose
    private Integer cropsTypesId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("language_name")
    @Expose
    private String languageName;
    @SerializedName("crops_list")
    @Expose
    private List<CropsList> cropsList = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCropsTypeName() {
        return cropsTypeName;
    }

    public void setCropsTypeName(String cropsTypeName) {
        this.cropsTypeName = cropsTypeName;
    }

    public Integer getCropsTypesLanguageId() {
        return cropsTypesLanguageId;
    }

    public void setCropsTypesLanguageId(Integer cropsTypesLanguageId) {
        this.cropsTypesLanguageId = cropsTypesLanguageId;
    }

    public Integer getCropsTypesId() {
        return cropsTypesId;
    }

    public void setCropsTypesId(Integer cropsTypesId) {
        this.cropsTypesId = cropsTypesId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }

    public List<CropsList> getCropsList() {
        return cropsList;
    }

    public void setCropsList(List<CropsList> cropsList) {
        this.cropsList = cropsList;
    }
}
