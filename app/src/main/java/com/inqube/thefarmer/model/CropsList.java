package com.inqube.thefarmer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by sanjib on 18/11/17.
 */

public class CropsList {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("crops_name")
    @Expose
    private String cropsName;
    @SerializedName("crops_language_id")
    @Expose
    private Integer cropsLanguageId;
    @SerializedName("crops_type_id")
    @Expose
    private Integer cropsTypeId;
    @SerializedName("crops_id")
    @Expose
    private Integer cropsId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("language_name")
    @Expose
    private String languageName;
    @SerializedName("crop_growth_stage_list")
    @Expose
    private List<CropGrowthStageList> cropGrowthStageList = null;

    public List<CropGrowthStageList> getCropGrowthStageList() {
        return cropGrowthStageList;
    }

    public void setCropGrowthStageList(List<CropGrowthStageList> cropGrowthStageList) {
        this.cropGrowthStageList = cropGrowthStageList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCropsName() {
        return cropsName;
    }

    public void setCropsName(String cropsName) {
        this.cropsName = cropsName;
    }

    public Integer getCropsLanguageId() {
        return cropsLanguageId;
    }

    public void setCropsLanguageId(Integer cropsLanguageId) {
        this.cropsLanguageId = cropsLanguageId;
    }

    public Integer getCropsTypeId() {
        return cropsTypeId;
    }

    public void setCropsTypeId(Integer cropsTypeId) {
        this.cropsTypeId = cropsTypeId;
    }

    public Integer getCropsId() {
        return cropsId;
    }

    public void setCropsId(Integer cropsId) {
        this.cropsId = cropsId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }
}
