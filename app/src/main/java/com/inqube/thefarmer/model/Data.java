package com.inqube.thefarmer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by sanjib on 17/11/17.
 */

public class Data {
    @SerializedName("get_role_list")
    @Expose
    private List<GetRoleList> getRoleList = null;
    @SerializedName("ks_problem_types_list")
    @Expose
    private List<KsProblemTypesList> ksProblemTypesList = null;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("frm_father_name")
    @Expose
    private String frmFatherName;
    @SerializedName("frm_gender")
    @Expose
    private String frmGender;
    @SerializedName("frm_primary_number")
    @Expose
    private String frmPrimaryNumber;
    @SerializedName("frm_secondary_number")
    @Expose
    private String frmSecondaryNumber;
    @SerializedName("frm_pincode")
    @Expose
    private String frmPincode;
    @SerializedName("frm_category")
    @Expose
    private Integer frmCategory;
    @SerializedName("frm_farmer_type")
    @Expose
    private Integer frmFarmerType;
    @SerializedName("frm_farmer_land_type")
    @Expose
    private Integer frmFarmerLandType;
    @SerializedName("frm_id_proof")
    @Expose
    private Integer frmIdProof;
    @SerializedName("frm_kcc_card")
    @Expose
    private Integer frmKccCard;
    @SerializedName("frm_kcc_card_number")
    @Expose
    private String frmKccCardNumber;
    @SerializedName("frm_bank_account")
    @Expose
    private String frmBankAccount;
    @SerializedName("frm_bank_name")
    @Expose
    private String frmBankName;
    @SerializedName("frm_bank_branch")
    @Expose
    private String frmBankBranch;
    @SerializedName("frm_bank_ifsc")
    @Expose
    private String frmBankIfsc;
    @SerializedName("frm_state")
    @Expose
    private Integer frmState;
    @SerializedName("frm_district")
    @Expose
    private Integer frmDistrict;
    @SerializedName("frm_subdivision")
    @Expose
    private Integer frmSubdivision;
    @SerializedName("frm_block")
    @Expose
    private Integer frmBlock;
    @SerializedName("frm_mouza")
    @Expose
    private Integer frmMouza;
    @SerializedName("frm_total_cultivabe_land")
    @Expose
    private String frmTotalCultivabeLand;
    @SerializedName("frm_farm_machinery")
    @Expose
    private Integer frmFarmMachinery;
    @SerializedName("frm_animal_number")
    @Expose
    private Integer frmAnimalNumber;
    @SerializedName("frm_cultivate_fish")
    @Expose
    private Integer frmCultivateFish;
    @SerializedName("frm_cultivate_pond_number")
    @Expose
    private Integer frmCultivatePondNumber;
    @SerializedName("frm_language_id")
    @Expose
    private Integer frmLanguageId;
    @SerializedName("is_active")
    @Expose
    private Integer isActive;
    @SerializedName("frm_last_login")
    @Expose
    private String frmLastLogin;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("frm_state_name")
    @Expose
    private String frmStateName;
    @SerializedName("frm_district_name")
    @Expose
    private String frmDistrictName;
    @SerializedName("frm_subdivision_name")
    @Expose
    private String frmSubdivisionName;
    @SerializedName("frm_block_name")
    @Expose
    private String frmBlockName;
    @SerializedName("frm_mouza_name")
    @Expose
    private String frmMouzaName;
    @SerializedName("profile_image_source")
    @Expose
    private String profileImageSource;
    @SerializedName("place_list")
    @Expose
    private List<PlaceList> placeList = null;
    @SerializedName("crop_type_list")
    @Expose
    private List<CropTypeList> cropTypeList = null;

    public List<KsProblemTypesList> getKsProblemTypesList() {
        return ksProblemTypesList;
    }

    public void setKsProblemTypesList(List<KsProblemTypesList> ksProblemTypesList) {
        this.ksProblemTypesList = ksProblemTypesList;
    }

    public List<GetRoleList> getGetRoleList() {
        return getRoleList;
    }

    public void setGetRoleList(List<GetRoleList> getRoleList) {
        this.getRoleList = getRoleList;
    }

    public List<CropTypeList> getCropTypeList() {
        return cropTypeList;
    }

    public void setCropTypeList(List<CropTypeList> cropTypeList) {
        this.cropTypeList = cropTypeList;
    }

    public String getFrmStateName() {
        return frmStateName;
    }

    public void setFrmStateName(String frmStateName) {
        this.frmStateName = frmStateName;
    }

    public String getFrmDistrictName() {
        return frmDistrictName;
    }

    public void setFrmDistrictName(String frmDistrictName) {
        this.frmDistrictName = frmDistrictName;
    }

    public String getFrmSubdivisionName() {
        return frmSubdivisionName;
    }

    public void setFrmSubdivisionName(String frmSubdivisionName) {
        this.frmSubdivisionName = frmSubdivisionName;
    }

    public String getFrmBlockName() {
        return frmBlockName;
    }

    public void setFrmBlockName(String frmBlockName) {
        this.frmBlockName = frmBlockName;
    }

    public String getFrmMouzaName() {
        return frmMouzaName;
    }

    public void setFrmMouzaName(String frmMouzaName) {
        this.frmMouzaName = frmMouzaName;
    }

    public String getProfileImageSource() {
        return profileImageSource;
    }

    public void setProfileImageSource(String profileImageSource) {
        this.profileImageSource = profileImageSource;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFrmFatherName() {
        return frmFatherName;
    }

    public void setFrmFatherName(String frmFatherName) {
        this.frmFatherName = frmFatherName;
    }

    public String getFrmGender() {
        return frmGender;
    }

    public void setFrmGender(String frmGender) {
        this.frmGender = frmGender;
    }

    public String getFrmPrimaryNumber() {
        return frmPrimaryNumber;
    }

    public void setFrmPrimaryNumber(String frmPrimaryNumber) {
        this.frmPrimaryNumber = frmPrimaryNumber;
    }

    public String getFrmSecondaryNumber() {
        return frmSecondaryNumber;
    }

    public void setFrmSecondaryNumber(String frmSecondaryNumber) {
        this.frmSecondaryNumber = frmSecondaryNumber;
    }

    public String getFrmPincode() {
        return frmPincode;
    }

    public void setFrmPincode(String frmPincode) {
        this.frmPincode = frmPincode;
    }

    public Integer getFrmCategory() {
        return frmCategory;
    }

    public void setFrmCategory(Integer frmCategory) {
        this.frmCategory = frmCategory;
    }

    public Integer getFrmFarmerType() {
        return frmFarmerType;
    }

    public void setFrmFarmerType(Integer frmFarmerType) {
        this.frmFarmerType = frmFarmerType;
    }

    public Integer getFrmFarmerLandType() {
        return frmFarmerLandType;
    }

    public void setFrmFarmerLandType(Integer frmFarmerLandType) {
        this.frmFarmerLandType = frmFarmerLandType;
    }

    public Integer getFrmIdProof() {
        return frmIdProof;
    }

    public void setFrmIdProof(Integer frmIdProof) {
        this.frmIdProof = frmIdProof;
    }

    public Integer getFrmKccCard() {
        return frmKccCard;
    }

    public void setFrmKccCard(Integer frmKccCard) {
        this.frmKccCard = frmKccCard;
    }

    public String getFrmKccCardNumber() {
        return frmKccCardNumber;
    }

    public void setFrmKccCardNumber(String frmKccCardNumber) {
        this.frmKccCardNumber = frmKccCardNumber;
    }

    public String getFrmBankAccount() {
        return frmBankAccount;
    }

    public void setFrmBankAccount(String frmBankAccount) {
        this.frmBankAccount = frmBankAccount;
    }

    public String getFrmBankName() {
        return frmBankName;
    }

    public void setFrmBankName(String frmBankName) {
        this.frmBankName = frmBankName;
    }

    public String getFrmBankBranch() {
        return frmBankBranch;
    }

    public void setFrmBankBranch(String frmBankBranch) {
        this.frmBankBranch = frmBankBranch;
    }

    public String getFrmBankIfsc() {
        return frmBankIfsc;
    }

    public void setFrmBankIfsc(String frmBankIfsc) {
        this.frmBankIfsc = frmBankIfsc;
    }

    public Integer getFrmState() {
        return frmState;
    }

    public void setFrmState(Integer frmState) {
        this.frmState = frmState;
    }

    public Integer getFrmDistrict() {
        return frmDistrict;
    }

    public void setFrmDistrict(Integer frmDistrict) {
        this.frmDistrict = frmDistrict;
    }

    public Integer getFrmSubdivision() {
        return frmSubdivision;
    }

    public void setFrmSubdivision(Integer frmSubdivision) {
        this.frmSubdivision = frmSubdivision;
    }

    public Integer getFrmBlock() {
        return frmBlock;
    }

    public void setFrmBlock(Integer frmBlock) {
        this.frmBlock = frmBlock;
    }

    public Integer getFrmMouza() {
        return frmMouza;
    }

    public void setFrmMouza(Integer frmMouza) {
        this.frmMouza = frmMouza;
    }

    public String getFrmTotalCultivabeLand() {
        return frmTotalCultivabeLand;
    }

    public void setFrmTotalCultivabeLand(String frmTotalCultivabeLand) {
        this.frmTotalCultivabeLand = frmTotalCultivabeLand;
    }

    public Integer getFrmFarmMachinery() {
        return frmFarmMachinery;
    }

    public void setFrmFarmMachinery(Integer frmFarmMachinery) {
        this.frmFarmMachinery = frmFarmMachinery;
    }

    public Integer getFrmAnimalNumber() {
        return frmAnimalNumber;
    }

    public void setFrmAnimalNumber(Integer frmAnimalNumber) {
        this.frmAnimalNumber = frmAnimalNumber;
    }

    public Integer getFrmCultivateFish() {
        return frmCultivateFish;
    }

    public void setFrmCultivateFish(Integer frmCultivateFish) {
        this.frmCultivateFish = frmCultivateFish;
    }

    public Integer getFrmCultivatePondNumber() {
        return frmCultivatePondNumber;
    }

    public void setFrmCultivatePondNumber(Integer frmCultivatePondNumber) {
        this.frmCultivatePondNumber = frmCultivatePondNumber;
    }

    public Integer getFrmLanguageId() {
        return frmLanguageId;
    }

    public void setFrmLanguageId(Integer frmLanguageId) {
        this.frmLanguageId = frmLanguageId;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public String getFrmLastLogin() {
        return frmLastLogin;
    }

    public void setFrmLastLogin(String frmLastLogin) {
        this.frmLastLogin = frmLastLogin;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<PlaceList> getPlaceList() {
        return placeList;
    }

    public void setPlaceList(List<PlaceList> placeList) {
        this.placeList = placeList;
    }

}
