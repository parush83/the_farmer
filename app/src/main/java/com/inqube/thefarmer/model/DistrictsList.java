package com.inqube.thefarmer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by sanjib on 17/11/17.
 */

public class DistrictsList {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("district_name")
    @Expose
    private String districtName;
    @SerializedName("state_id")
    @Expose
    private Integer stateId;
    @SerializedName("district_id")
    @Expose
    private Integer districtId;
    @SerializedName("district_language_id")
    @Expose
    private Integer districtLanguageId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("language_name")
    @Expose
    private String languageName;
    @SerializedName("district_map_image")
    @Expose
    private String district_map_image;
    @SerializedName("subdivision_list")
    @Expose
    private List<SubdivisionList> subdivisionList = null;

    public String getDistrict_map_image() {
        return district_map_image;
    }

    public void setDistrict_map_image(String district_map_image) {
        this.district_map_image = district_map_image;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public Integer getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    public Integer getDistrictLanguageId() {
        return districtLanguageId;
    }

    public void setDistrictLanguageId(Integer districtLanguageId) {
        this.districtLanguageId = districtLanguageId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }


    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }

    public List<SubdivisionList> getSubdivisionList() {
        return subdivisionList;
    }

    public void setSubdivisionList(List<SubdivisionList> subdivisionList) {
        this.subdivisionList = subdivisionList;
    }


}
