package com.inqube.thefarmer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sanjib on 17/11/17.
 */

public class GetMouzaList {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("mouza_name")
    @Expose
    private String mouzaName;
    @SerializedName("block_id")
    @Expose
    private Integer blockId;
    @SerializedName("jl_number")
    @Expose
    private Integer jlNumber;
    @SerializedName("mouza_language_id")
    @Expose
    private Integer mouzaLanguageId;
    @SerializedName("mouza_id")
    @Expose
    private Integer mouzaId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("language_name")
    @Expose
    private String languageName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMouzaName() {
        return mouzaName;
    }

    public void setMouzaName(String mouzaName) {
        this.mouzaName = mouzaName;
    }

    public Integer getBlockId() {
        return blockId;
    }

    public void setBlockId(Integer blockId) {
        this.blockId = blockId;
    }

    public Integer getJlNumber() {
        return jlNumber;
    }

    public void setJlNumber(Integer jlNumber) {
        this.jlNumber = jlNumber;
    }

    public Integer getMouzaLanguageId() {
        return mouzaLanguageId;
    }

    public void setMouzaLanguageId(Integer mouzaLanguageId) {
        this.mouzaLanguageId = mouzaLanguageId;
    }

    public Integer getMouzaId() {
        return mouzaId;
    }

    public void setMouzaId(Integer mouzaId) {
        this.mouzaId = mouzaId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }

}