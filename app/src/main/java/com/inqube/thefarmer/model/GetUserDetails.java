package com.inqube.thefarmer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by inqube on 26/12/17.
 */

public class GetUserDetails {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("frm_father_name")
    @Expose
    private String frmFatherName;
    @SerializedName("frm_gender")
    @Expose
    private String frmGender;
    @SerializedName("frm_primary_number")
    @Expose
    private String frmPrimaryNumber;
    @SerializedName("frm_secondary_number")
    @Expose
    private String frmSecondaryNumber;
    @SerializedName("frm_pincode")
    @Expose
    private String frmPincode;
    @SerializedName("frm_category")
    @Expose
    private int frmCategory;
    @SerializedName("frm_farmer_type")
    @Expose
    private int frmFarmerType;
    @SerializedName("frm_farmer_land_type")
    @Expose
    private int frmFarmerLandType;
    @SerializedName("frm_id_proof")
    @Expose
    private int frmIdProof;
    @SerializedName("frm_id_proof_number")
    @Expose
    private String frmIdProofNumber;
    @SerializedName("frm_kcc_card")
    @Expose
    private int frmKccCard;
    @SerializedName("frm_kcc_card_number")
    @Expose
    private String frmKccCardNumber;
    @SerializedName("frm_bank_account")
    @Expose
    private String frmBankAccount = "";
    @SerializedName("frm_bank_name")
    @Expose
    private String frmBankName;
    @SerializedName("frm_bank_branch")
    @Expose
    private String frmBankBranch;
    @SerializedName("frm_bank_ifsc")
    @Expose
    private String frmBankIfsc;
    @SerializedName("frm_state")
    @Expose
    private int frmState;
    @SerializedName("frm_district")
    @Expose
    private int frmDistrict;
    @SerializedName("frm_subdivision")
    @Expose
    private int frmSubdivision;
    @SerializedName("frm_block")
    @Expose
    private int frmBlock;
    @SerializedName("frm_mouza")
    @Expose
    private int frmMouza;
    @SerializedName("frm_total_cultivabe_land")
    @Expose
    private String frmTotalCultivabeLand;
    @SerializedName("frm_farm_machinery")
    @Expose
    private int frmFarmMachinery;
    @SerializedName("frm_animal_number")
    @Expose
    private int frmAnimalNumber;
    @SerializedName("frm_cultivate_fish")
    @Expose
    private int frmCultivateFish;
    @SerializedName("frm_cultivate_pond_number")
    @Expose
    private int frmCultivatePondNumber;
    @SerializedName("frm_language_id")
    @Expose
    private int frmLanguageId;
    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("is_active")
    @Expose
    private int isActive;
    @SerializedName("frm_latitude")
    @Expose
    private String frmLatitude;
    @SerializedName("frm_longitude")
    @Expose
    private String frmLongitude;
    @SerializedName("frm_last_login")
    @Expose
    private String frmLastLogin;
    @SerializedName("approved_by_user_id")
    @Expose
    private int approvedByUserId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("frm_state_name")
    @Expose
    private String frmStateName;
    @SerializedName("frm_district_name")
    @Expose
    private String frmDistrictName;
    @SerializedName("frm_subdivision_name")
    @Expose
    private String frmSubdivisionName;
    @SerializedName("frm_block_name")
    @Expose
    private String frmBlockName;
    @SerializedName("frm_mouza_name")
    @Expose
    private String frmMouzaName;
    @SerializedName("profile_image_source")
    @Expose
    private String profileImageSource;
    @SerializedName("farm_rabi_list_id")
    @Expose
    private String farmRabiListId;
    @SerializedName("farm_rabi_list_text")
    @Expose
    private String farmRabiListText;
    @SerializedName("farm_kharif_list_id")
    @Expose
    private String farmKharifListId;
    @SerializedName("farm_kharif_list_text")
    @Expose
    private String farmKharifListText;
    @SerializedName("farm_pre_kharif_list_id")
    @Expose
    private String farmPreKharifListId;
    @SerializedName("farm_pre_kharif_list_text")
    @Expose
    private String farmPreKharifListText;
    @SerializedName("farm_machiniery_list_text")
    @Expose
    private String farmMachinieryListText;
    @SerializedName("farm_machiniery_list_id")
    @Expose
    private String farmMachinieryListId;
    @SerializedName("farm_animals_list_id")
    @Expose
    private String farmAnimalsListId;
    @SerializedName("farm_animals_list_text")
    @Expose
    private String farmAnimalsListText;
    @SerializedName("frm_farmer_type_text")
    @Expose
    private String frmFarmerTypeText;

    public String getFarmRabiListId() {
        return farmRabiListId;
    }

    public void setFarmRabiListId(String farmRabiListId) {
        this.farmRabiListId = farmRabiListId;
    }

    public String getFarmRabiListText() {
        return farmRabiListText;
    }

    public void setFarmRabiListText(String farmRabiListText) {
        this.farmRabiListText = farmRabiListText;
    }

    public String getFarmKharifListId() {
        return farmKharifListId;
    }

    public void setFarmKharifListId(String farmKharifListId) {
        this.farmKharifListId = farmKharifListId;
    }

    public String getFarmKharifListText() {
        return farmKharifListText;
    }

    public void setFarmKharifListText(String farmKharifListText) {
        this.farmKharifListText = farmKharifListText;
    }

    public String getFarmPreKharifListId() {
        return farmPreKharifListId;
    }

    public void setFarmPreKharifListId(String farmPreKharifListId) {
        this.farmPreKharifListId = farmPreKharifListId;
    }

    public String getFarmPreKharifListText() {
        return farmPreKharifListText;
    }

    public void setFarmPreKharifListText(String farmPreKharifListText) {
        this.farmPreKharifListText = farmPreKharifListText;
    }

    public String getFarmMachinieryListText() {
        return farmMachinieryListText;
    }

    public void setFarmMachinieryListText(String farmMachinieryListText) {
        this.farmMachinieryListText = farmMachinieryListText;
    }

    public String getFarmMachinieryListId() {
        return farmMachinieryListId;
    }

    public void setFarmMachinieryListId(String farmMachinieryListId) {
        this.farmMachinieryListId = farmMachinieryListId;
    }

    public String getFarmAnimalsListId() {
        return farmAnimalsListId;
    }

    public void setFarmAnimalsListId(String farmAnimalsListId) {
        this.farmAnimalsListId = farmAnimalsListId;
    }

    public String getFarmAnimalsListText() {
        return farmAnimalsListText;
    }

    public void setFarmAnimalsListText(String farmAnimalsListText) {
        this.farmAnimalsListText = farmAnimalsListText;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFrmFatherName() {
        return frmFatherName;
    }

    public void setFrmFatherName(String frmFatherName) {
        this.frmFatherName = frmFatherName;
    }

    public String getFrmGender() {
        return frmGender;
    }

    public void setFrmGender(String frmGender) {
        this.frmGender = frmGender;
    }

    public String getFrmPrimaryNumber() {
        return frmPrimaryNumber;
    }

    public void setFrmPrimaryNumber(String frmPrimaryNumber) {
        this.frmPrimaryNumber = frmPrimaryNumber;
    }

    public String getFrmSecondaryNumber() {
        return frmSecondaryNumber;
    }

    public void setFrmSecondaryNumber(String frmSecondaryNumber) {
        this.frmSecondaryNumber = frmSecondaryNumber;
    }

    public String getFrmPincode() {
        return frmPincode;
    }

    public void setFrmPincode(String frmPincode) {
        this.frmPincode = frmPincode;
    }

    public int getFrmCategory() {
        return frmCategory;
    }

    public void setFrmCategory(int frmCategory) {
        this.frmCategory = frmCategory;
    }

    public int getFrmFarmerType() {
        return frmFarmerType;
    }

    public void setFrmFarmerType(int frmFarmerType) {
        this.frmFarmerType = frmFarmerType;
    }

    public int getFrmFarmerLandType() {
        return frmFarmerLandType;
    }

    public void setFrmFarmerLandType(int frmFarmerLandType) {
        this.frmFarmerLandType = frmFarmerLandType;
    }

    public int getFrmIdProof() {
        return frmIdProof;
    }

    public void setFrmIdProof(int frmIdProof) {
        this.frmIdProof = frmIdProof;
    }

    public String getFrmIdProofNumber() {
        return frmIdProofNumber;
    }

    public void setFrmIdProofNumber(String frmIdProofNumber) {
        this.frmIdProofNumber = frmIdProofNumber;
    }

    public int getFrmKccCard() {
        return frmKccCard;
    }

    public void setFrmKccCard(int frmKccCard) {
        this.frmKccCard = frmKccCard;
    }

    public String getFrmKccCardNumber() {
        return frmKccCardNumber;
    }

    public void setFrmKccCardNumber(String frmKccCardNumber) {
        this.frmKccCardNumber = frmKccCardNumber;
    }

    public String
    getFrmBankAccount() {
        return frmBankAccount;
    }

    public void setFrmBankAccount(String frmBankAccount) {
        this.frmBankAccount = frmBankAccount;
    }

    public String getFrmBankName() {
        return frmBankName;
    }

    public void setFrmBankName(String frmBankName) {
        this.frmBankName = frmBankName;
    }

    public String getFrmBankBranch() {
        return frmBankBranch;
    }

    public void setFrmBankBranch(String frmBankBranch) {
        this.frmBankBranch = frmBankBranch;
    }

    public String getFrmBankIfsc() {
        return frmBankIfsc;
    }

    public void setFrmBankIfsc(String frmBankIfsc) {
        this.frmBankIfsc = frmBankIfsc;
    }

    public int getFrmState() {
        return frmState;
    }

    public void setFrmState(int frmState) {
        this.frmState = frmState;
    }

    public int getFrmDistrict() {
        return frmDistrict;
    }

    public void setFrmDistrict(int frmDistrict) {
        this.frmDistrict = frmDistrict;
    }

    public int getFrmSubdivision() {
        return frmSubdivision;
    }

    public void setFrmSubdivision(int frmSubdivision) {
        this.frmSubdivision = frmSubdivision;
    }

    public int getFrmBlock() {
        return frmBlock;
    }

    public void setFrmBlock(int frmBlock) {
        this.frmBlock = frmBlock;
    }

    public int getFrmMouza() {
        return frmMouza;
    }

    public void setFrmMouza(int frmMouza) {
        this.frmMouza = frmMouza;
    }

    public String getFrmTotalCultivabeLand() {
        return frmTotalCultivabeLand;
    }

    public void setFrmTotalCultivabeLand(String frmTotalCultivabeLand) {
        this.frmTotalCultivabeLand = frmTotalCultivabeLand;
    }

    public int getFrmFarmMachinery() {
        return frmFarmMachinery;
    }

    public void setFrmFarmMachinery(int frmFarmMachinery) {
        this.frmFarmMachinery = frmFarmMachinery;
    }

    public int getFrmAnimalNumber() {
        return frmAnimalNumber;
    }

    public void setFrmAnimalNumber(int frmAnimalNumber) {
        this.frmAnimalNumber = frmAnimalNumber;
    }

    public int getFrmCultivateFish() {
        return frmCultivateFish;
    }

    public void setFrmCultivateFish(int frmCultivateFish) {
        this.frmCultivateFish = frmCultivateFish;
    }

    public int getFrmCultivatePondNumber() {
        return frmCultivatePondNumber;
    }

    public void setFrmCultivatePondNumber(int frmCultivatePondNumber) {
        this.frmCultivatePondNumber = frmCultivatePondNumber;
    }

    public int getFrmLanguageId() {
        return frmLanguageId;
    }

    public void setFrmLanguageId(int frmLanguageId) {
        this.frmLanguageId = frmLanguageId;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public String getFrmLatitude() {
        return frmLatitude;
    }

    public void setFrmLatitude(String frmLatitude) {
        this.frmLatitude = frmLatitude;
    }

    public String getFrmLongitude() {
        return frmLongitude;
    }

    public void setFrmLongitude(String frmLongitude) {
        this.frmLongitude = frmLongitude;
    }

    public String getFrmLastLogin() {
        return frmLastLogin;
    }

    public void setFrmLastLogin(String frmLastLogin) {
        this.frmLastLogin = frmLastLogin;
    }

    public int getApprovedByUserId() {
        return approvedByUserId;
    }

    public void setApprovedByUserId(int approvedByUserId) {
        this.approvedByUserId = approvedByUserId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getFrmStateName() {
        return frmStateName;
    }

    public void setFrmStateName(String frmStateName) {
        this.frmStateName = frmStateName;
    }

    public String getFrmDistrictName() {
        return frmDistrictName;
    }

    public void setFrmDistrictName(String frmDistrictName) {
        this.frmDistrictName = frmDistrictName;
    }

    public String getFrmSubdivisionName() {
        return frmSubdivisionName;
    }

    public void setFrmSubdivisionName(String frmSubdivisionName) {
        this.frmSubdivisionName = frmSubdivisionName;
    }

    public String getFrmBlockName() {
        return frmBlockName;
    }

    public void setFrmBlockName(String frmBlockName) {
        this.frmBlockName = frmBlockName;
    }

    public String getFrmMouzaName() {
        return frmMouzaName;
    }

    public void setFrmMouzaName(String frmMouzaName) {
        this.frmMouzaName = frmMouzaName;
    }

    public String getProfileImageSource() {
        return profileImageSource;
    }

    public void setProfileImageSource(String profileImageSource) {
        this.profileImageSource = profileImageSource;
    }

    public String getFrmFarmerTypeText() {
        return frmFarmerTypeText;
    }

    public void setFrmFarmerTypeText(String frmFarmerTypeText) {
        this.frmFarmerTypeText = frmFarmerTypeText;
    }

}
