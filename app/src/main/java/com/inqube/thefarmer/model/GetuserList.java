package com.inqube.thefarmer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by inqube on 28/12/17.
 */

public class GetuserList {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("frm_father_name")
    @Expose
    private String frmFatherName;
    @SerializedName("frm_gender")
    @Expose
    private String frmGender;
    @SerializedName("frm_primary_number")
    @Expose
    private String frmPrimaryNumber;
    @SerializedName("frm_secondary_number")
    @Expose
    private String frmSecondaryNumber;
    @SerializedName("frm_pincode")
    @Expose
    private String frmPincode;
    @SerializedName("frm_category")
    @Expose
    private int frmCategory;
    @SerializedName("frm_farmer_type")
    @Expose
    private int frmFarmerType;
    @SerializedName("frm_farmer_land_type")
    @Expose
    private int frmFarmerLandType;
    @SerializedName("frm_id_proof")
    @Expose
    private int frmIdProof;
    @SerializedName("frm_id_proof_number")
    @Expose
    private String frmIdProofNumber;
    @SerializedName("frm_kcc_card")
    @Expose
    private int frmKccCard;
    @SerializedName("frm_kcc_card_number")
    @Expose
    private String frmKccCardNumber;
    @SerializedName("frm_bank_account")
    @Expose
    private String frmBankAccount;
    @SerializedName("frm_bank_name")
    @Expose
    private String frmBankName;
    @SerializedName("frm_bank_branch")
    @Expose
    private String frmBankBranch;
    @SerializedName("frm_bank_ifsc")
    @Expose
    private String frmBankIfsc;
    @SerializedName("frm_state")
    @Expose
    private Integer frmState;
    @SerializedName("frm_district")
    @Expose
    private Integer frmDistrict;
    @SerializedName("frm_subdivision")
    @Expose
    private Integer frmSubdivision;
    @SerializedName("frm_block")
    @Expose
    private Integer frmBlock;
    @SerializedName("frm_mouza")
    @Expose
    private Integer frmMouza;
    @SerializedName("frm_total_cultivabe_land")
    @Expose
    private Object frmTotalCultivabeLand;
    @SerializedName("frm_farm_machinery")
    @Expose
    private Object frmFarmMachinery;
    @SerializedName("frm_animal_number")
    @Expose
    private Object frmAnimalNumber;
    @SerializedName("frm_cultivate_fish")
    @Expose
    private Object frmCultivateFish;
    @SerializedName("frm_cultivate_pond_number")
    @Expose
    private Object frmCultivatePondNumber;
    @SerializedName("frm_language_id")
    @Expose
    private Integer frmLanguageId;
    @SerializedName("otp")
    @Expose
    private Object otp;
    @SerializedName("is_active")
    @Expose
    private Integer isActive;
    @SerializedName("frm_latitude")
    @Expose
    private Object frmLatitude;
    @SerializedName("frm_longitude")
    @Expose
    private Object frmLongitude;
    @SerializedName("frm_last_login")
    @Expose
    private Object frmLastLogin;
    @SerializedName("approved_by_user_id")
    @Expose
    private Integer approvedByUserId;
    @SerializedName("created_at")
    @Expose
    private Object createdAt;
    @SerializedName("updated_at")
    @Expose
    private Object updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFrmPrimaryNumber() {
        return frmPrimaryNumber;
    }

    public void setFrmPrimaryNumber(String frmPrimaryNumber) {
        this.frmPrimaryNumber = frmPrimaryNumber;
    }
}
