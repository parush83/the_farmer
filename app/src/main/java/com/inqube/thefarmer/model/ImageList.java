package com.inqube.thefarmer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by inqube on 14/2/18.
 */

public class ImageList {
    @SerializedName("image_source")
    @Expose
    private String imageSource;

    public String getImageSource() {
        return imageSource;
    }

    public void setImageSource(String imageSource) {
        this.imageSource = imageSource;
    }
}
