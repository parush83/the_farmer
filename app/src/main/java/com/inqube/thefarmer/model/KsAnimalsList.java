package com.inqube.thefarmer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sanjib on 24/11/17.
 */

public class KsAnimalsList {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("animal_name")
    @Expose
    private String animalName;
    @SerializedName("animal_language_id")
    @Expose
    private Integer animalLanguageId;
    @SerializedName("animal_id")
    @Expose
    private Integer animalId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private Object updatedAt;
    @SerializedName("language_name")
    @Expose
    private String languageName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAnimalName() {
        return animalName;
    }

    public void setAnimalName(String animalName) {
        this.animalName = animalName;
    }

    public Integer getAnimalLanguageId() {
        return animalLanguageId;
    }

    public void setAnimalLanguageId(Integer animalLanguageId) {
        this.animalLanguageId = animalLanguageId;
    }

    public Integer getAnimalId() {
        return animalId;
    }

    public void setAnimalId(Integer animalId) {
        this.animalId = animalId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Object updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }
}
