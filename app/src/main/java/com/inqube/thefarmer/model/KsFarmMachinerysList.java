package com.inqube.thefarmer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sanjib on 24/11/17.
 */

public class KsFarmMachinerysList {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("farm_machinery_name")
    @Expose
    private String farmMachineryName;
    @SerializedName("farm_machinery_language_id")
    @Expose
    private Integer farmMachineryLanguageId;
    @SerializedName("farm_machinery_id")
    @Expose
    private Integer farmMachineryId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private Object updatedAt;
    @SerializedName("language_name")
    @Expose
    private String languageName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFarmMachineryName() {
        return farmMachineryName;
    }

    public void setFarmMachineryName(String farmMachineryName) {
        this.farmMachineryName = farmMachineryName;
    }

    public Integer getFarmMachineryLanguageId() {
        return farmMachineryLanguageId;
    }

    public void setFarmMachineryLanguageId(Integer farmMachineryLanguageId) {
        this.farmMachineryLanguageId = farmMachineryLanguageId;
    }

    public Integer getFarmMachineryId() {
        return farmMachineryId;
    }

    public void setFarmMachineryId(Integer farmMachineryId) {
        this.farmMachineryId = farmMachineryId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Object updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }
}
