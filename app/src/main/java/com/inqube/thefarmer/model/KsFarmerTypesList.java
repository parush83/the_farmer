package com.inqube.thefarmer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sanjib on 24/11/17.
 */

public class KsFarmerTypesList {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("farmer_type_name")
    @Expose
    private String farmerTypeName;
    @SerializedName("farmer_type_language_id")
    @Expose
    private Integer farmerTypeLanguageId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private Object updatedAt;
    @SerializedName("farmer_type_id")
    @Expose
    private Integer farmerTypeId;
    @SerializedName("language_name")
    @Expose
    private String languageName;

    public Integer getFarmerTypeId() {
        return farmerTypeId;
    }

    public void setFarmerTypeId(Integer farmerTypeId) {
        this.farmerTypeId = farmerTypeId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFarmerTypeName() {
        return farmerTypeName;
    }

    public void setFarmerTypeName(String farmerTypeName) {
        this.farmerTypeName = farmerTypeName;
    }

    public Integer getFarmerTypeLanguageId() {
        return farmerTypeLanguageId;
    }

    public void setFarmerTypeLanguageId(Integer farmerTypeLanguageId) {
        this.farmerTypeLanguageId = farmerTypeLanguageId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Object updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }
}
