package com.inqube.thefarmer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sanjib on 24/11/17.
 */

public class KsProblemTypesList {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("problem_type")
    @Expose
    private String problemType;
    @SerializedName("problem_type_id")
    @Expose
    private Integer problemTypeId;
    @SerializedName("problem_type_language_id")
    @Expose
    private Integer problemTypeLanguageId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private Object updatedAt;
    @SerializedName("language_name")
    @Expose
    private String languageName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProblemType() {
        return problemType;
    }

    public void setProblemType(String problemType) {
        this.problemType = problemType;
    }

    public Integer getProblemTypeId() {
        return problemTypeId;
    }

    public void setProblemTypeId(Integer problemTypeId) {
        this.problemTypeId = problemTypeId;
    }

    public Integer getProblemTypeLanguageId() {
        return problemTypeLanguageId;
    }

    public void setProblemTypeLanguageId(Integer problemTypeLanguageId) {
        this.problemTypeLanguageId = problemTypeLanguageId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Object updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }
}
