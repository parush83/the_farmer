package com.inqube.thefarmer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sanjib on 24/11/17.
 */

public class KsSeasonsList {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("season_name")
    @Expose
    private String seasonName;
    @SerializedName("season_language_id")
    @Expose
    private Integer seasonLanguageId;
    @SerializedName("season_id")
    @Expose
    private Integer seasonId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private Object updatedAt;
    @SerializedName("language_name")
    @Expose
    private String languageName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSeasonName() {
        return seasonName;
    }

    public void setSeasonName(String seasonName) {
        this.seasonName = seasonName;
    }

    public Integer getSeasonLanguageId() {
        return seasonLanguageId;
    }

    public void setSeasonLanguageId(Integer seasonLanguageId) {
        this.seasonLanguageId = seasonLanguageId;
    }

    public Integer getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(Integer seasonId) {
        this.seasonId = seasonId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Object updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }
}
