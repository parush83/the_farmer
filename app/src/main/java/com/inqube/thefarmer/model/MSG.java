package com.inqube.thefarmer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by sanjib on 17/11/17.
 */

public class MSG {

    @SerializedName("userdetails_data")
    @Expose
    private String userdetails_data;
    @SerializedName("otp_data")
    @Expose
    private String otp_data;
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("get_mouza_list")
    @Expose
    private List<GetMouzaList> getMouzaList = null;
    @SerializedName("token_id")
    @Expose
    private TokenId tokenId;
    @SerializedName("img_data")
    @Expose
    private String img_data;
    @SerializedName("ks_animals_list")
    @Expose
    private List<KsAnimalsList> ksAnimalsList = null;
    @SerializedName("ks_farm_machinerys_list")
    @Expose
    private List<KsFarmMachinerysList> ksFarmMachinerysList = null;
    @SerializedName("ks_farmer_types_list")
    @Expose
    private List<KsFarmerTypesList> ksFarmerTypesList = null;
    @SerializedName("ks_seasons_list")
    @Expose
    private List<KsSeasonsList> ksSeasonsList = null;
    @SerializedName("question_answers_data")
    @Expose
    private Integer questionAnswersData;
    @SerializedName("user_details")
    @Expose
    private List<UserDetail> userDetails = null;
    @SerializedName("get_list")
    @Expose
    private List<GetList> getList = null;
    @SerializedName("get_user_details")
    @Expose
    private GetUserDetails getUserDetails;
    @SerializedName("getuser_list")
    @Expose
    private List<GetuserList> getuserList = null;
    @SerializedName("answer_data")
    @Expose
    private List<AnswerDatum> answerData = null;

    public String getOtp_data() {
        return otp_data;
    }

    public void setOtp_data(String otp_data) {
        this.otp_data = otp_data;
    }

    public String getUserdetails_data() {
        return userdetails_data;
    }

    public void setUserdetails_data(String userdetails_data) {
        this.userdetails_data = userdetails_data;
    }

    public String getImg_data() {
        return img_data;
    }

    public void setImg_data(String img_data) {
        this.img_data = img_data;
    }

    public Integer getQuestionAnswersData() {
        return questionAnswersData;
    }

    public void setQuestionAnswersData(Integer questionAnswersData) {
        this.questionAnswersData = questionAnswersData;
    }

    public List<GetuserList> getGetuserList() {
        return getuserList;
    }

    public void setGetuserList(List<GetuserList> getuserList) {
        this.getuserList = getuserList;
    }

    public List<AnswerDatum> getAnswerData() {
        return answerData;
    }

    public void setAnswerData(List<AnswerDatum> answerData) {
        this.answerData = answerData;
    }

    public GetUserDetails getGetUserDetails() {
        return getUserDetails;
    }

    public void setGetUserDetails(GetUserDetails getUserDetails) {
        this.getUserDetails = getUserDetails;
    }

    public List<GetList> getGetList() {
        return getList;
    }

    public void setGetList(List<GetList> getList) {
        this.getList = getList;
    }

    public List<UserDetail> getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(List<UserDetail> userDetails) {
        this.userDetails = userDetails;
    }

    public List<KsAnimalsList> getKsAnimalsList() {
        return ksAnimalsList;
    }

    public void setKsAnimalsList(List<KsAnimalsList> ksAnimalsList) {
        this.ksAnimalsList = ksAnimalsList;
    }

    public List<KsFarmMachinerysList> getKsFarmMachinerysList() {
        return ksFarmMachinerysList;
    }

    public void setKsFarmMachinerysList(List<KsFarmMachinerysList> ksFarmMachinerysList) {
        this.ksFarmMachinerysList = ksFarmMachinerysList;
    }

    public List<KsFarmerTypesList> getKsFarmerTypesList() {
        return ksFarmerTypesList;
    }

    public void setKsFarmerTypesList(List<KsFarmerTypesList> ksFarmerTypesList) {
        this.ksFarmerTypesList = ksFarmerTypesList;
    }

    public List<KsSeasonsList> getKsSeasonsList() {
        return ksSeasonsList;
    }

    public void setKsSeasonsList(List<KsSeasonsList> ksSeasonsList) {
        this.ksSeasonsList = ksSeasonsList;
    }

    public TokenId getTokenId() {
        return tokenId;
    }

    public void setTokenId(TokenId tokenId) {
        this.tokenId = tokenId;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<GetMouzaList> getGetMouzaList() {
        return getMouzaList;
    }

    public void setGetMouzaList(List<GetMouzaList> getMouzaList) {
        this.getMouzaList = getMouzaList;
    }
}
