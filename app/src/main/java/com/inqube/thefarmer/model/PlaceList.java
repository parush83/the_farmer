package com.inqube.thefarmer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by sanjib on 17/11/17.
 */

public class PlaceList {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("state_name")
    @Expose
    private String stateName;
    @SerializedName("state_language_id")
    @Expose
    private Integer stateLanguageId;
    @SerializedName("state_id")
    @Expose
    private Integer stateId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("language_name")
    @Expose
    private String languageName;
    @SerializedName("districts_list")
    @Expose
    private List<DistrictsList> districtsList = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public Integer getStateLanguageId() {
        return stateLanguageId;
    }

    public void setStateLanguageId(Integer stateLanguageId) {
        this.stateLanguageId = stateLanguageId;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }

    public List<DistrictsList> getDistrictsList() {
        return districtsList;
    }

    public void setDistrictsList(List<DistrictsList> districtsList) {
        this.districtsList = districtsList;
    }


}
