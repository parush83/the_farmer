package com.inqube.thefarmer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by inqube on 15/2/18.
 */

public class QuestionDatum {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("question_type_id")
    @Expose
    private Integer questionTypeId;
    @SerializedName("crop_type_id")
    @Expose
    private Integer cropTypeId;
    @SerializedName("crop_id")
    @Expose
    private Integer cropId;
    @SerializedName("growth_stage_id")
    @Expose
    private Integer growthStageId;
    @SerializedName("problem_type_id")
    @Expose
    private Integer problemTypeId;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("question_submit_for")
    @Expose
    private Integer questionSubmitFor;
    @SerializedName("farmer_name")
    @Expose
    private Object farmerName;
    @SerializedName("farmer_mobile_no")
    @Expose
    private Object farmerMobileNo;
    @SerializedName("state_id")
    @Expose
    private Integer stateId;
    @SerializedName("district_id")
    @Expose
    private Integer districtId;
    @SerializedName("subdivision_id")
    @Expose
    private Integer subdivisionId;
    @SerializedName("block_id")
    @Expose
    private Integer blockId;
    @SerializedName("mouza_id")
    @Expose
    private Integer mouzaId;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("lognitude")
    @Expose
    private String lognitude;
    @SerializedName("question_title")
    @Expose
    private String questionTitle;
    @SerializedName("long_description")
    @Expose
    private String longDescription;
    @SerializedName("effective_area")
    @Expose
    private Object effectiveArea;
    @SerializedName("answer_id")
    @Expose
    private Object answerId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("language_name")
    @Expose
    private String languageName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuestionTypeId() {
        return questionTypeId;
    }

    public void setQuestionTypeId(Integer questionTypeId) {
        this.questionTypeId = questionTypeId;
    }

    public Integer getCropTypeId() {
        return cropTypeId;
    }

    public void setCropTypeId(Integer cropTypeId) {
        this.cropTypeId = cropTypeId;
    }

    public Integer getCropId() {
        return cropId;
    }

    public void setCropId(Integer cropId) {
        this.cropId = cropId;
    }

    public Integer getGrowthStageId() {
        return growthStageId;
    }

    public void setGrowthStageId(Integer growthStageId) {
        this.growthStageId = growthStageId;
    }

    public Integer getProblemTypeId() {
        return problemTypeId;
    }

    public void setProblemTypeId(Integer problemTypeId) {
        this.problemTypeId = problemTypeId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getQuestionSubmitFor() {
        return questionSubmitFor;
    }

    public void setQuestionSubmitFor(Integer questionSubmitFor) {
        this.questionSubmitFor = questionSubmitFor;
    }

    public Object getFarmerName() {
        return farmerName;
    }

    public void setFarmerName(Object farmerName) {
        this.farmerName = farmerName;
    }

    public Object getFarmerMobileNo() {
        return farmerMobileNo;
    }

    public void setFarmerMobileNo(Object farmerMobileNo) {
        this.farmerMobileNo = farmerMobileNo;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public Integer getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    public Integer getSubdivisionId() {
        return subdivisionId;
    }

    public void setSubdivisionId(Integer subdivisionId) {
        this.subdivisionId = subdivisionId;
    }

    public Integer getBlockId() {
        return blockId;
    }

    public void setBlockId(Integer blockId) {
        this.blockId = blockId;
    }

    public Integer getMouzaId() {
        return mouzaId;
    }

    public void setMouzaId(Integer mouzaId) {
        this.mouzaId = mouzaId;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLognitude() {
        return lognitude;
    }

    public void setLognitude(String lognitude) {
        this.lognitude = lognitude;
    }

    public String getQuestionTitle() {
        return questionTitle;
    }

    public void setQuestionTitle(String questionTitle) {
        this.questionTitle = questionTitle;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public Object getEffectiveArea() {
        return effectiveArea;
    }

    public void setEffectiveArea(String effectiveArea) {
        this.effectiveArea = effectiveArea;
    }

    public Object getAnswerId() {
        return answerId;
    }

    public void setAnswerId(String answerId) {
        this.answerId = answerId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }
}

