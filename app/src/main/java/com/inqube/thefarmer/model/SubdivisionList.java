package com.inqube.thefarmer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by sanjib on 17/11/17.
 */

public class SubdivisionList {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("sub_division_name")
    @Expose
    private String subDivisionName;
    @SerializedName("district_id")
    @Expose
    private Integer districtId;
    @SerializedName("sub_division_id")
    @Expose
    private Integer subDivisionId;
    @SerializedName("sub_division_language_id")
    @Expose
    private Integer subDivisionLanguageId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("language_name")
    @Expose
    private String languageName;
    @SerializedName("block_list")
    @Expose
    private List<BlockList> blockList = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSubDivisionName() {
        return subDivisionName;
    }

    public void setSubDivisionName(String subDivisionName) {
        this.subDivisionName = subDivisionName;
    }

    public Integer getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    public Integer getSubDivisionId() {
        return subDivisionId;
    }

    public void setSubDivisionId(Integer subDivisionId) {
        this.subDivisionId = subDivisionId;
    }

    public Integer getSubDivisionLanguageId() {
        return subDivisionLanguageId;
    }

    public void setSubDivisionLanguageId(Integer subDivisionLanguageId) {
        this.subDivisionLanguageId = subDivisionLanguageId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }

    public List<BlockList> getBlockList() {
        return blockList;
    }

    public void setBlockList(List<BlockList> blockList) {
        this.blockList = blockList;
    }

}
