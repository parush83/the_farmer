package com.inqube.thefarmer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sanjib on 25/11/17.
 */

public class UserDetail {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("frm_father_name")
    @Expose
    private Object frmFatherName;
    @SerializedName("frm_gender")
    @Expose
    private Object frmGender;
    @SerializedName("frm_primary_number")
    @Expose
    private String frmPrimaryNumber;
    @SerializedName("frm_secondary_number")
    @Expose
    private Object frmSecondaryNumber;
    @SerializedName("frm_pincode")
    @Expose
    private Object frmPincode;
    @SerializedName("frm_category")
    @Expose
    private Object frmCategory;
    @SerializedName("frm_farmer_type")
    @Expose
    private Object frmFarmerType;
    @SerializedName("frm_farmer_land_type")
    @Expose
    private Object frmFarmerLandType;
    @SerializedName("frm_id_proof")
    @Expose
    private Object frmIdProof;
    @SerializedName("frm_kcc_card")
    @Expose
    private Object frmKccCard;
    @SerializedName("frm_kcc_card_number")
    @Expose
    private Object frmKccCardNumber;
    @SerializedName("frm_bank_account")
    @Expose
    private Object frmBankAccount;
    @SerializedName("frm_bank_name")
    @Expose
    private Object frmBankName;
    @SerializedName("frm_bank_branch")
    @Expose
    private Object frmBankBranch;
    @SerializedName("frm_bank_ifsc")
    @Expose
    private Object frmBankIfsc;
    @SerializedName("frm_state")
    @Expose
    private Integer frmState;
    @SerializedName("frm_district")
    @Expose
    private Integer frmDistrict;
    @SerializedName("frm_subdivision")
    @Expose
    private Integer frmSubdivision;
    @SerializedName("frm_block")
    @Expose
    private Integer frmBlock;
    @SerializedName("frm_mouza")
    @Expose
    private Integer frmMouza;
    @SerializedName("frm_total_cultivabe_land")
    @Expose
    private Object frmTotalCultivabeLand;
    @SerializedName("frm_farm_machinery")
    @Expose
    private Object frmFarmMachinery;
    @SerializedName("frm_animal_number")
    @Expose
    private Object frmAnimalNumber;
    @SerializedName("frm_cultivate_fish")
    @Expose
    private Object frmCultivateFish;
    @SerializedName("frm_cultivate_pond_number")
    @Expose
    private Object frmCultivatePondNumber;
    @SerializedName("frm_language_id")
    @Expose
    private Integer frmLanguageId;
    @SerializedName("otp")
    @Expose
    private Object otp;
    @SerializedName("is_active")
    @Expose
    private Integer isActive;
    @SerializedName("frm_last_login")
    @Expose
    private String frmLastLogin;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("profile_image_source")
    @Expose
    private String profileImageSource;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Object getFrmFatherName() {
        return frmFatherName;
    }

    public void setFrmFatherName(Object frmFatherName) {
        this.frmFatherName = frmFatherName;
    }

    public Object getFrmGender() {
        return frmGender;
    }

    public void setFrmGender(Object frmGender) {
        this.frmGender = frmGender;
    }

    public String getFrmPrimaryNumber() {
        return frmPrimaryNumber;
    }

    public void setFrmPrimaryNumber(String frmPrimaryNumber) {
        this.frmPrimaryNumber = frmPrimaryNumber;
    }

    public Object getFrmSecondaryNumber() {
        return frmSecondaryNumber;
    }

    public void setFrmSecondaryNumber(Object frmSecondaryNumber) {
        this.frmSecondaryNumber = frmSecondaryNumber;
    }

    public Object getFrmPincode() {
        return frmPincode;
    }

    public void setFrmPincode(Object frmPincode) {
        this.frmPincode = frmPincode;
    }

    public Object getFrmCategory() {
        return frmCategory;
    }

    public void setFrmCategory(Object frmCategory) {
        this.frmCategory = frmCategory;
    }

    public Object getFrmFarmerType() {
        return frmFarmerType;
    }

    public void setFrmFarmerType(Object frmFarmerType) {
        this.frmFarmerType = frmFarmerType;
    }

    public Object getFrmFarmerLandType() {
        return frmFarmerLandType;
    }

    public void setFrmFarmerLandType(Object frmFarmerLandType) {
        this.frmFarmerLandType = frmFarmerLandType;
    }

    public Object getFrmIdProof() {
        return frmIdProof;
    }

    public void setFrmIdProof(Object frmIdProof) {
        this.frmIdProof = frmIdProof;
    }

    public Object getFrmKccCard() {
        return frmKccCard;
    }

    public void setFrmKccCard(Object frmKccCard) {
        this.frmKccCard = frmKccCard;
    }

    public Object getFrmKccCardNumber() {
        return frmKccCardNumber;
    }

    public void setFrmKccCardNumber(Object frmKccCardNumber) {
        this.frmKccCardNumber = frmKccCardNumber;
    }

    public Object getFrmBankAccount() {
        return frmBankAccount;
    }

    public void setFrmBankAccount(Object frmBankAccount) {
        this.frmBankAccount = frmBankAccount;
    }

    public Object getFrmBankName() {
        return frmBankName;
    }

    public void setFrmBankName(Object frmBankName) {
        this.frmBankName = frmBankName;
    }

    public Object getFrmBankBranch() {
        return frmBankBranch;
    }

    public void setFrmBankBranch(Object frmBankBranch) {
        this.frmBankBranch = frmBankBranch;
    }

    public Object getFrmBankIfsc() {
        return frmBankIfsc;
    }

    public void setFrmBankIfsc(Object frmBankIfsc) {
        this.frmBankIfsc = frmBankIfsc;
    }

    public Integer getFrmState() {
        return frmState;
    }

    public void setFrmState(Integer frmState) {
        this.frmState = frmState;
    }

    public Integer getFrmDistrict() {
        return frmDistrict;
    }

    public void setFrmDistrict(Integer frmDistrict) {
        this.frmDistrict = frmDistrict;
    }

    public Integer getFrmSubdivision() {
        return frmSubdivision;
    }

    public void setFrmSubdivision(Integer frmSubdivision) {
        this.frmSubdivision = frmSubdivision;
    }

    public Integer getFrmBlock() {
        return frmBlock;
    }

    public void setFrmBlock(Integer frmBlock) {
        this.frmBlock = frmBlock;
    }

    public Integer getFrmMouza() {
        return frmMouza;
    }

    public void setFrmMouza(Integer frmMouza) {
        this.frmMouza = frmMouza;
    }

    public Object getFrmTotalCultivabeLand() {
        return frmTotalCultivabeLand;
    }

    public void setFrmTotalCultivabeLand(Object frmTotalCultivabeLand) {
        this.frmTotalCultivabeLand = frmTotalCultivabeLand;
    }

    public Object getFrmFarmMachinery() {
        return frmFarmMachinery;
    }

    public void setFrmFarmMachinery(Object frmFarmMachinery) {
        this.frmFarmMachinery = frmFarmMachinery;
    }

    public Object getFrmAnimalNumber() {
        return frmAnimalNumber;
    }

    public void setFrmAnimalNumber(Object frmAnimalNumber) {
        this.frmAnimalNumber = frmAnimalNumber;
    }

    public Object getFrmCultivateFish() {
        return frmCultivateFish;
    }

    public void setFrmCultivateFish(Object frmCultivateFish) {
        this.frmCultivateFish = frmCultivateFish;
    }

    public Object getFrmCultivatePondNumber() {
        return frmCultivatePondNumber;
    }

    public void setFrmCultivatePondNumber(Object frmCultivatePondNumber) {
        this.frmCultivatePondNumber = frmCultivatePondNumber;
    }

    public Integer getFrmLanguageId() {
        return frmLanguageId;
    }

    public void setFrmLanguageId(Integer frmLanguageId) {
        this.frmLanguageId = frmLanguageId;
    }

    public Object getOtp() {
        return otp;
    }

    public void setOtp(Object otp) {
        this.otp = otp;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public String getFrmLastLogin() {
        return frmLastLogin;
    }

    public void setFrmLastLogin(String frmLastLogin) {
        this.frmLastLogin = frmLastLogin;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getProfileImageSource() {
        return profileImageSource;
    }

    public void setProfileImageSource(String profileImageSource) {
        this.profileImageSource = profileImageSource;
    }
}
