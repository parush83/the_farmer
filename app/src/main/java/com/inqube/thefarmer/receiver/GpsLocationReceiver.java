package com.inqube.thefarmer.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;


import com.inqube.thefarmer.FarmerApplication;

import Utils.ApplicationLifecycleManager;

/**
 * Created by root on 2/16/17.
 */

public class GpsLocationReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
            final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                //do something
            } else {
                if (ApplicationLifecycleManager.isAppInForeground()) {
                    FarmerApplication farmer_base_application = (FarmerApplication) context.getApplicationContext();
                    farmer_base_application.farmer_base_activity.ifGPSNotAvailable();
                }
            }

        }
    }
}